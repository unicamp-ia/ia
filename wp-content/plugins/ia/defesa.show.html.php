<?

$query_var = get_query_var('ia_defesa_id');

Timber::render('twig/defesa/defesa.show.twig',[
    'content' => ia_getDefesa($query_var),
    'lang' => ia_get_lang(),
    'template_directory' => get_bloginfo('template_directory'),
    'topbar' => getNewsflash(),
]);



?>

