<?php
/**
 * Pagina da campanha
 */
function ia_indexCampanha(){
    global $wpdbIntranet;
    $result = [];

    $queryTag = "SELECT * from sintranet.com_tag where type = 3 order by name ASC";


    foreach ($wpdbIntranet->get_results($queryTag) as $item){
        $result[$item->id] = prepareCampanha($item);
    }

    return $result;
}

function prepareCampanha($item){

    $image = WWW4_PUBLIC . '/media/cache/resolve/extensaoCampanhaTopo/extensao/' . $item->document_name;
    return [
        'id' => $item->id,
        'description' => $item->description,
        'name' => $item->name,
        'url' => $item->url,
        'about' => $item->about,
        'image' => $image,
    ];
}

/**
 * Exibe campanha
 * @return array
 */
function ia_getCampanha($campanhaId)
{
    global $wpdbIntranet;
    $result = null;

    // BUsca a categoria
    $queryCampanha = "SELECT * from sintranet.com_tag where url = '" . sanitize_text_field($campanhaId) . "'";
    foreach ($wpdbIntranet->get_results($queryCampanha) as $campanha) {
        $result = prepareCampanha($campanha);
    }

    return $result;
}

/**
 * Exibe campanha
 * @return array
 */
function ia_showCampanha($campanhaSlug){
    global $wpdbIntranet;
    $result = [];

    $queryCampanha = "SELECT * from sintranet.com_tag ct where ct.url  = '" . sanitize_text_field($campanhaSlug) . "'";
    $campanha = $wpdbIntranet->get_row($queryCampanha);


    if(is_null($campanha)) {
        wp_redirect('../../campanha');
        exit;
    }

    // busca as categorias
    $queryCategoria = "select ct.*, ctf.category_name from sintranet.com_tag_file ctf 
join sintranet.com_tag ct on ct.id = ctf.tag_child_id 
where ctf.tag_parent_id =" . $campanha->id;

    foreach ($wpdbIntranet->get_results($queryCategoria) as $category){
        $image = WWW4_PUBLIC .'/media/cache/resolve/extensaoCampanhaCategory/extensao/'  . $category->category_name;

        $result['categories'][$category->id] = [
            'id' => $category->id,
            'url' => $category->url,
            'name' => $category->name,
            'image' => $image,
        ];

        // procurar copnteudo com aquela categoria
        $queryContent = "select DISTINCT cc.* from sintranet.com_content_tag cct
join sintranet.com_content cc on cc.id  = cct.content_id 
where cct.tag_id in (".$category->id.",".$category->id.")
and cc.status = 1
order by cc.title";

        foreach ($wpdbIntranet->get_results($queryContent) as $content){
            $row = prepareContent($content);

            // Associa o conteudo a sua categoria
            $queryContentTag = "SELECT * from sintranet.com_content_tag where content_id = " . $content->id;
            foreach ($wpdbIntranet->get_results($queryContentTag) as $contentTag){
                if(isset($result['categories'][$contentTag->tag_id])) {
                    $result['categories'][$contentTag->tag_id]['rows'][] = $row;
                }
            }
        }

    }

    return $result;
}

function prepareContent($content) {
    $image = null;
    if($content->slideshow_name) $image = WWW4_PUBLIC .'/media/cache/resolve/contentShow/slideshow/'  . $content->slideshow_name;
    if($content->evento_name) $image = WWW4_PUBLIC .'/media/cache/resolve/contentShow/evento/'  . $content->evento_name;

    return [
        'id' => $content->id,
        'title' => $content->title,
        'youtubeId' => $content->you_tube_id,
        'image' => $image,
        'typeLabel' => ia_getContentLabel($content->type),

    ];
}
/**
 * Pagina de filtro de campanha
 */
function ia_campanha_search(){
    global $wpdbIntranet;

    if(!isset($_GET['s'])) return null;
    $text = $_GET['s'];

    $query = " select * from sintranet.com_content cc where status = 1 and type = 5" .
                " and (cc.title  like '%".sanitize_text_field($text)."%'" .
                " or cc.body  like '%".sanitize_text_field($text)."%' or cc.evento_local like '%".sanitize_text_field($text)."%')";

    $result = array();
    foreach ($wpdbIntranet->get_results($query) as $content) {
        $result[] = prepareContent($content);
    }


    return $result;
}
