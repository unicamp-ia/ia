<?php
/**
 * Plugin Name: IA
 * Plugin URI: https://www.iar.unicamp.br
 * Description: Plugin de customizacoes do IA
 * Version: 1.0
 * Author: Newton Silva
 */
require_once 'ia-campanha.php';

// Write a new permalink entry on code activation
register_activation_hook( __FILE__, 'ia_content_activation' );
function ia_content_activation() {
    ia_content_custom_output();
    flush_rewrite_rules(); // Update the permalink entries in the database, so the permalink structure needn't be redone every page load
}

// If the plugin is deactivated, clean the permalink structure
register_deactivation_hook( __FILE__, 'ia_content_deactivation' );
function ia_content_deactivation() {
    flush_rewrite_rules();
}


// And now, the code that do the magic!!!
// This code create a new permalink entry
add_action('init', 'ia_content_custom_output');
function ia_content_custom_output() {
    add_rewrite_tag( '%ia_content_id%', '([^/]+)' );
    add_permastruct( 'ia_content_id', '/content/%ia_content_id%' );

    // defesas
    add_permastruct( 'ia_defesas', '/defesas' );
    add_permastruct( 'ia_content', '/content' );

    add_rewrite_tag( '%ia_defesa_id%', '([^/]+)' );
    add_permastruct( 'id_defesa_id', '/defesas/%ia_defesa_id%' );

    // extensao
    add_permastruct( 'ia_extensao', '/cursosExtensao' );
    add_rewrite_tag( '%ia_extensao_id%', '([^/]+)' );
    add_permastruct( 'id_extensao_id', '/cursosExtensao/%ia_extensao_id%' );

    add_permastruct( 'ia_extensao_campanha', '/campanha' );

    add_rewrite_tag( '%ia_campanha_id%', '([^/]+)' );
    add_permastruct( 'id_campanha_id', '/extensao/%ia_campanha_id%' );
    add_permastruct( 'campanha_sobre', '/extensao/%ia_campanha_id%/sobre' );



}

// The following controls the output content
add_action('template_include', 'ia_content_display');
function ia_content_display($original_template) {
    global $wp;
    if (get_query_var('ia_content_id')) {
        include(__DIR__ . '/content.show.html.php');
    } elseif (get_query_var('ia_defesa_id')) {
        include(__DIR__ . '/defesa.show.html.php');
    } elseif (get_query_var('ia_extensao_id')) {
        include(__DIR__ . '/extensao.show.html.php');
    } elseif (add_query_arg( array(), $wp->request) == 'defesas') {
        include(__DIR__ . '/defesa.index.html.php');
    } elseif (add_query_arg( array(), $wp->request) == 'cursosExtensao') {
        include(__DIR__ . '/extensao.index.html.php');
    } elseif (add_query_arg( array(), $wp->request) == 'content') {
        include(__DIR__ . '/content.index.html.php');
    } elseif (add_query_arg( array(), $wp->request) == 'campanha') {
        include(__DIR__ . '/extensao.campanha.index.html.php');

    } elseif (get_query_var('ia_campanha_id')) {

        if($wp->matched_rule == 'extensao/([^/]+)/sobre/?$'){
            include(__DIR__ . '/extensao.campanha.sobre.html.php');
        } else {
            include(__DIR__ . '/extensao.campanha.show.html.php');
        }
    } else {
        return $original_template;
    }
}


/**
 * @param $id
 * @return array
 * @throws Exception
 */
function ia_getContent($id){
    global $wpdbIntranet;
    $content = $wpdbIntranet->get_results('SELECT * from sintranet.com_content where id = ' . $id);

    date_default_timezone_set('America/Sao_Paulo');

    foreach ($content as $item){
        $now = new DateTime();

        if($item->evento_inicio) {
            $inicio = new DateTime($item->evento_inicio);
            // correcao fuso :(
            $horario_inicio = $inicio->format('H');
        }
        else {
            $inicio = null;
            $horario_inicio = null;
        }

        if($item->evento_fim) {
            $fim = new DateTime($item->evento_fim);
            $horario_fim = $fim->format('H');
        } else {
            $fim = null;
            $horario_fim = null;
        }
        
        $created = new DateTime($item->created);

        if($item->redirect){
            if(substr($item->redirect,0,3) == 'www'){
                $item->redirect = 'https://' . $item->redirect;
            }

            header("Location: " . $item->redirect);
            exit();
        }

        $image = null;

        if($item->publish_instagram and  !$item->created_instagram){
            postInstagram($id);
        }

        if($item->slideshow_name) $image = WWW4_PUBLIC .'/media/cache/resolve/contentShow/slideshow/'  . $item->slideshow_name;
        if($item->evento_name) $image = WWW4_PUBLIC .'/media/cache/resolve/contentShow/evento/'  . $item->evento_name;

        $result['content'] = [
            'title' => $item->title,
            'body' => $item->body,
            'typeLabel' => ia_getContentLabel($item->type),
            'type' => $item->type,
            'created' => $created->format('d/m/Y'),
            'chamada' => $item->evento_chamada,
            'inicio' => $inicio,
            'horario_inicio' => $horario_inicio,
            'fim' => $fim,
            'carouselConfigXML' => ia_getContent_carousel($item),
            'horario_fim' => $horario_fim,
            'youtubeId' => $item->you_tube_id,
            'dataProducao' => $item->data_producao,
            'credito' => $item->evento_credito,
            'mail' => $item->mail,
            'open' => $item->open_to,
            'local' => $item->evento_local,
            'image' => $image,
            'id' => $item->id,
        ];

        // Campanha
        $queryCampanha = "select ct.* from sintranet.com_content_tag cct " .
                         " left join sintranet.com_tag  ct on ct.id  = cct.tag_id ".
                        " where ct.type = 3 and cct.content_id = " . $item->id;
        foreach ($wpdbIntranet->get_results($queryCampanha) as $tag){
            $result['campanha'] = prepareCampanha($tag);

            // sobrescreve caso possua topo personalizado para aquela categoria
            $queryTopoCategoria = 'select ctf.document_name from sintranet.com_tag_file ctf ' .
                                ' left join sintranet.com_content_tag cct on ctf.tag_child_id = cct.tag_id '.
                                ' where cct.content_id = ' . $item->id;
            $imagemTopoCategoria = $wpdbIntranet->get_row($queryTopoCategoria);
            if($imagemTopoCategoria) {
                $result['campanha']['image'] = WWW4_PUBLIC . '/media/cache/resolve/extensaoCampanhaTopo/extensao/'
                    . $imagemTopoCategoria->document_name;
            }

        }
    }

    return $result;
}

/**
 * Busca imagens de carousel de um conteudo
 * @param $item
 */
function ia_getContent_carousel($content){
    global $wpdbIntranet;

    $contentFilesQuery = $wpdbIntranet->get_results('select * from sintranet.com_content_file ccf where slideshow = true and content_id = ' . $content->id);

    if($contentFilesQuery) {
        return WWW4_PUBLIC . '/public/comunicacao/content/' . $content->id . '/carousel';
    } else {
        return null;
    }
}



function postInstagram($id){
    global $wpdbIntranet;

    /** @var Woo_IGP $woo */
    $woo = new Woo_IGP(array());

    $content = $wpdbIntranet->get_results('SELECT * from sintranet.com_content where id = ' . $id);

    foreach ($content as $item){

        $getInstagramLogin = $woo->wigp_get_inst();

        if($getInstagramLogin['success']){

            $options = get_option( 'wooigp_settings' );

            if (!isset( $options['wooigp_number_show_words']))
                $options['wooigp_number_show_words'] = 2000;

            $post_desc = strip_tags(wp_trim_words($item->body, 100) );

            $message = $item->title;
            //$message.= metadata_exists('post', $post_id, '_wigp_msg') ? " - ".get_post_meta( $post_id, '_wigp_msg', true ) : '';


            if ( $options['wooigp_number_show_words'] > 0 ) {
                $message.=  "\n" . $post_desc;
            }


            $image = null;
            echo 4;
            $message = strip_shortcodes(html_entity_decode($message, ENT_COMPAT, "UTF-8"));
            if($item->slideshow_name) $image = '/site/sintranet/web/files/slideshow/'  . $item->slideshow_name;
            if($item->evento_name) $image = '/site/sintranet/web/files/evento/'  . $item->evento_name;
            if($item->instagram_name) $image = '/site/sintranet/web/files/instagram/'  . $item->instagram_name;



            if ($image){
                echo 5;
                $photo = new \InstagramAPI\Media\Photo\InstagramPhoto($image);
                $woo->igp->timeline->uploadPhoto($photo->getFile(), ['caption' => $message]);
                $wpdbIntranet->query('UPDATE sintranet.com_content set created_instagram = CURRENT_TIMESTAMP where id ='.$id);
                return true;
            }


        } else return;
    }
}

function ia_getDefesa($id){
    global $wpdbIntranet;
    $today = new DateTime();
    $today->setTime(0,0,0);

    $result = array();

    // PROX DEFESAS
    $defesas = $wpdbIntranet->get_results("select a.id, a.link_gravacao, a.link_gravacao2, a.data_prevista_defesa, a.orientador, a.local_exame, 
        b.nome, a.titulo_tese, a.resumo_tese, a.hora_defesa, c.desc_nivel, c.nome as nomeNivel, a.link_videoconferencia
        from sintranet.cpg_orientacao a join sintranet.user_user b on a.aluno_id = b.id join sintranet.cpg_curso c on c.id = a.curso_id
        where a.id = ".$id);

    foreach ($defesas as $defesa){

        $dia = new DateTime($defesa->data_prevista_defesa,new DateTimeZone('America/Sao_Paulo'));
        // gravacoes ficarao disponiveis por ate 4 dias apos a defesa


        $result = [
            'link_gravacao' => $defesa->link_gravacao,
            'link_gravacao2' => $defesa->link_gravacao2,
            'resumo' => $defesa->resumo_tese,
            'data' => $dia->modify('+3 hours'),
            'descNivel' => $defesa->desc_nivel . ' ' . $defesa->nomeNivel,
            'hora' => substr($defesa->hora_defesa,0,5),
            'titulo' => $defesa->titulo_tese,
            'orientador' => $defesa->orientador,
            'linkVideoconferencia' => $defesa->link_videoconferencia,
            'local' => $defesa->local_exame,
            'nome' => $defesa->nome
        ];
    }

    return $result;
}




function ia_getContentLabel($type){
    switch ($type){
        case 1: return 'Evento';
        case 2: return 'Notícia';
        case 3: return 'Comunicado';
    }
}

