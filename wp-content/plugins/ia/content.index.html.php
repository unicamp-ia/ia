<?

Timber::render('twig/content/content.index.twig',[
    'lang' => ia_get_lang(),
    'get' => $_GET,
    'rows' => ia_content_page(),
    'WWW4_PATH' => WWW4P_PATH,
    'template_directory' => get_bloginfo('template_directory'),
    'topbar' => getNewsflash(),
]);
