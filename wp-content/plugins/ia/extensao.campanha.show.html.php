<?

Timber::render('twig/extensao/campanha.show.twig',[
    'lang' => ia_get_lang(),
    'get' => $_GET,
    'data' => ia_showCampanha(get_query_var('ia_campanha_id')),
    'campanha' => ia_getCampanha(get_query_var('ia_campanha_id')),
    'search' => ia_campanha_search(),
    'WWW4_PATH' => WWW4P_PATH,
    'template_directory' => get_bloginfo('template_directory'),
    'topbar' => getNewsflash(),
    'page_name' => 'campanha_show',
    'main_menu' => 'pesquisa-extensao',

]);