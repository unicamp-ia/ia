<?

$query_var = get_query_var('ia_content_id');
$render = ia_getContent($query_var);
$content = $render['content'];

Timber::render('twig/content/content.show.twig',[
    'content' => $content,
    'lang' => ia_get_lang(),
    'page_title' => $content['title'],
    'template_directory' => get_bloginfo('template_directory'),
    'topbar' => getNewsflash(),
    'campanha' => isset($render['campanha']) ? $render['campanha'] : null,
]);

?>

