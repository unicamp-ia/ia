<?

Timber::render('twig/extensao/campanha.sobre.twig',[
    'lang' => ia_get_lang(),
    'data' => ia_showCampanha(get_query_var('ia_campanha_id')),
    'campanha' => ia_getCampanha(get_query_var('ia_campanha_id')),
    'WWW4_PATH' => WWW4P_PATH,
    'page_name' => 'campanha_sobre',
    'template_directory' => get_bloginfo('template_directory'),
    'topbar' => getNewsflash(),
    'main_menu' => 'pesquisa-extensao',

]);