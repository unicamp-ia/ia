<style>
#mask {
background-color: #666;
background: url(wordpress/wp-content/themes/principal/img/bg_pop.png) repeat;
height: 100%;
left: 0;
position: fixed;
top: 0;
width: 100%;
z-index: 9000;
}
#popup {
color: #000066;
height: 500px;
display: none;
position:absolute; 
top:20%; 
left:50%; 
width: 500px;
margin-left:-300px;  
margin-top:-100px;  
z-index: 9999;
}
#fechar {
position: relative;
}
#fechar a {
position: absolute;
right: -100px;
top: 0;
width: 52px;
height: 24px;
overflow: hidden;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
color: #FFFFFF;
}
#fechar a:hover {
opacity: 0.7;
filter: alpha(opacity = 70);
}
</style>
<div id="mask" onclick="javascript: fechar();"></div>
<div id="popup" class="popup">
<div id="fechar"><a href="javascript: fechar();"><font size="2">Fechar</font></a></div>
<div id="img_pop"><a href="http://www.iar.unicamp.br/evento/mostraunificada1" target="_blank"><img src="wordpress/wp-content/themes/principal/img/mostra-ia-2015.png"  height="600" width="600" /></a></div>
</div>
<script language="javascript">
// Função que fecha o pop-up ao clicar no botao fechar
	function fechar(){
		document.getElementById('popup').style.display = 'none';
		document.getElementById('mask').style.display = 'none';
	}
	// Aqui definimos o tempo para fechar o pop-up automaticamente em milissengundos
	function abrir(){
		document.getElementById('popup').style.display = 'block';
		setTimeout ("fechar()", 10000);
	}
	function trocaordem(){
		document.getElementById("popup").style.zIndex="1";
	}
	abrir();
</script>

<?php
	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/nivo-slider.css"),
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/home.css"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/jquery.nivo.slider.pack.js"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/home.js")
	);
	
	get_header();
?>

<?php

	$imagens = array(
		array("src" => "home/1.jpg" , "title" => "Foto: Celso Palermo" ,        "alt" => ""),
		array("src" => "home/2.jpg" , "title" => "Foto: Milena Travassos" ,     "alt" => ""),
		array("src" => "home/3.jpg" , "title" => "Foto: Celso Palermo" ,        "alt" => ""),
		array("src" => "home/4.jpg" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => ""),
		array("src" => "home/5.jpg" , "title" => "Foto: Bruna Scorsatto" ,      "alt" => "")
	);
	
	slider($imagens);

?>
<div class="row">
    <div class="col-md-12">
        <nav id="areas">
            <ul>
                <?php
                    
                    $menu = array(
                                "Artes Visuais" => array(97,99),
                                "Comunicação"   => array(105,155),
                                "Dança"         => array(68,88),
                                "Música"        => array(107,109),
                                "Teatro"        => array(80,88)
                    );
                    
                    foreach ($menu as $titulo => $paginas) { ?>
                        <li>
                            <span><?php echo($titulo) ?></span>
                            <ul>
                                <?php foreach ($paginas as $id) { $pagina = get_page($id); ?>
                                    <li><a href="<?php echo($pagina->post_name); ?>/" title="<?php echo($pagina->post_title); ?>"><?php echo($pagina->post_title); ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                <?php } ?>
            </ul>
        </nav>
    </div> <!-- div.col-md-12 -->
</div> <!-- div.row -->
    
<div class="row">
    <div class="col-md-3">
        <div class="destaque">
            <a class="title" href="<?php echo(get_page_link(1271)); ?>">Eventos</a>
            
            <?php
            $the_query = new WP_Query( array("post_type" => "tribe_events" , "tax_query" => array(array("taxonomy" => "tribe_events_cat","terms" => 135,"operator" => "NOT IN")) , "posts_per_page" => 6)); // "eventDisplay" => "upcoming"
            
            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <section<?php if ($the_query->current_post == 0) { echo(" class=\"first\""); } ?>>
                    <?php if (has_post_thumbnail()) {?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php if ($the_query->current_post == 0) { the_post_thumbnail("home-first"); } else { the_post_thumbnail("home"); } ?></a>
                    <?php } ?>
                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    <p class="data"><?php echo(tribe_get_start_date()); ?></p>
                    <p><?php echo excerpt(15); ?></p>
                </section>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>

        
    <div class="col-md-3">
        <div class="destaque">
            <a class="title" href="<?php echo(get_category_link(1)); ?>">Notícias</a>
        
            <?php
            $the_query = new WP_Query( array("cat" => "66,-15,-3" , "posts_per_page" => 6));
            
            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <section<?php if ($the_query->current_post == 0) { echo(" class=\"first\""); } ?>>
                    <?php if (has_post_thumbnail()) {?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php if ($the_query->current_post == 0) { the_post_thumbnail("home-first"); } else { the_post_thumbnail("home"); } ?></a>
                    <?php } ?>
                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    <p><?php echo excerpt(15); ?></p>
                </section>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
	</div>
    
    <div class="col-md-3">
		<?php
            $the_query = new WP_Query( array("post_type" => "tribe_events" , "tax_query" => array(array("taxonomy" => "tribe_events_cat","terms" => 135)) , "posts_per_page" => 2));
            if ($the_query->found_posts > 0) {
        ?>
            <div class="destaque">
                <a class="title" href="<?php echo(get_term_link(135,"tribe_events_cat")); ?>">Galeria de Arte - GAIA</a>
                
                <?php
                
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <section<?php if ($the_query->current_post == 0) { echo(" class=\"first\""); } ?>>
                        <?php if (has_post_thumbnail()) {?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php if ($the_query->current_post == 0) { the_post_thumbnail("home-first"); } else { the_post_thumbnail("home"); } ?></a>
                        <?php } ?>
                        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                        <p><?php echo excerpt(15); ?></p>
                    </section>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
            
            <hr />
        <?php } ?>
                
        <div class="panel panel-default destaque">
            <div class="panel-heading">
    	        <h3 class="panel-title">Destaques</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><h2><a href="<?php echo(get_page_link(282)); ?>">Defesas</a></h2></li>
                    <li><h2><a href="<?php echo(get_category_link(32)); ?>">Processos Seletivos</a></h2></li>
                    <li><h2><a href="<?php echo(get_page_link(4165)); ?>">Concursos</a></h2></li>
                    <li><h2><a href="<?php echo(get_category_link(57)); ?>">Editais</a></h2></li>
                </ul>
            </div>
        </div>
    </div>      
      
    <div class="col-md-3">
        <div class="destaque">
            <div class="title"><a href="<?php echo(get_category_link(15)); ?>">Graduação</a> / <br /><a href="<?php echo(get_category_link(3)); ?>">Pós-graduação</a></div>
            <?php
            $the_query = new WP_Query( array("cat" => "15,3" , "posts_per_page" => 5));
            
            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <section<?php if ($the_query->current_post == 0) { echo(" class=\"first\""); } ?>>
                    <?php if (has_post_thumbnail()) {?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php if ($the_query->current_post == 0) { the_post_thumbnail("home-first"); } else { the_post_thumbnail("home"); } ?></a>
                    <?php } ?>
                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    <p><?php echo excerpt(15); ?></p>
                </section>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>
    

</div> <!-- div.row -->

<?php get_footer(); ?>
