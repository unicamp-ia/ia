<?php

remove_action("wp_head", "wp_generator"); // Remove a versão do Wordpress por questões de segurança
define("WP_DEBUG", false); // Desabilita o degub


add_action("admin_head", function  () {
   if (!current_user_can("manage_options")) {
	   echo '<style type="text/css">
			   td.mce_h1,
			   td.mce_h4,
			   td.mce_h5,
			   td.mce_h6,
			   
			   
			   li#wp-admin-bar-comments,
			   li#wp-admin-bar-new-media,
			   li#menu-media,
			   li#menu-comments,
			   li#menu-tools { display:none; }
			 </style>
			 ';
   }
});


function my_admin_footer_function() {
    global $pagenow;
    if (($pagenow == "post.php") && (!current_user_can("manage_options"))) {
		echo '
			<script src="'.get_bloginfo("template_directory").'/js/jquery-1.10.1.min.js"></script>
			<script type="text/javascript">
				$(document).ready(function () {
					//$("select#page_template").attr("disabled","disabled");
					$("p:contains(\'Modelo\')").hide();
					$("select#page_template").hide();
				});
			 </script>';
    }
}
//add_action('admin_footer', 'my_admin_footer_function');

function htaccess_notice () {
    global $pagenow;
    if ( $pagenow == "options-permalink.php" ) {
         echo '<div class="updated">
             <p><strong>ATENÇÃO!</strong> O arquivo <strong>.htaccess</strong> foi modificado manualmente. Qualquer alteração nesta página pode sobrescrever as configurações.</p>
         </div>';
    }
}
add_action("admin_notices", "htaccess_notice");

function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).' &#91;...&#93;';
	}
	else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

if ( function_exists("add_theme_support") ) {
	add_theme_support("post-thumbnails");
}

if ( function_exists("add_image_size") ) { 
	add_image_size("home-first", 306, 204, true );
	add_image_size("home", 63, 42, true );
}

function the_breadcrumb () {
	
	$lista_negra = array(
		112,  // O Instituto
		168,  // Pós-graduação
		216,  // Resultados Pós-graduação Artes Visuais
		1133, // Resultados Pós-graduação Multimeios
		1181, // Resultados Pós-graduação Artes da Cena
		1239, // Resultados Pós-graduação Música
		366,  // Congregação
		987,  // Graduação
		201,  // Viver em Baraão Geraldo
		280,  // Dissertações e Teses
		231,  // Vida Acadêmica
		825,  // Pesquisa
		833,  // Projetos em Andamento
		586,  // Calendário Bolsas
		286,  // Seleção Estudante Especial
		180   // CAPES e CNpQ
	);
	
    $separator = "&raquo;";
	global $post;
	
    echo "<ul id=\"breadcrumbs\" class=\"list-inline\">";
	
	echo "<li><a href=\"" . get_option("home") . "\">Home</a></li><li class=\"separator\">$separator</li>";
	
	if (is_category()) {
		global $wp_query;
		$category = $wp_query->get_queried_object();
		echo "<li>" . $category->name . "</li>";
	}
	elseif (is_single()) {
		$categories = get_the_category();
		
		foreach ($categories as $index => $category) {
			if ($category->cat_ID != 66) { // Home
				$aux[] = array("nome" => $category->name , "link" => get_category_link($category->cat_ID));
			}
		}
		
		$categories = $aux;
		
		$tamanho = sizeof($categories);
		if ($tamanho > 0) {
			echo("<li>");
			foreach ($categories as $index => $category) {
				echo("<a href=\"" . $category["link"] . "\" title=\"" . $category["nome"] . "\">" . $category["nome"]  . "</a>");
				if ($index < $tamanho-1) { echo(", "); }
			}
			echo("</li><li class=\"separator\">$separator</li><li>".get_the_title()."</li>");			
		}
	}
	elseif (is_page()) {
		if($post->post_parent){
			$anc = array_reverse(get_post_ancestors($post->ID));
			$title = get_the_title();
			foreach ( $anc as $ancestor ) {
				if (in_array($ancestor,$lista_negra)) {
					$output = $output . "<li>".get_the_title($ancestor)."</li>";						
				}
				else {
					$output = $output . "<li><a href=\"".get_permalink($ancestor)."\" title=\"".get_the_title($ancestor)."\">".get_the_title($ancestor)."</a></li>";
				}
				$output = $output . "<li class=\"separator\">$separator</li>";						
			}
			echo $output;
			echo "<li><strong title=\"$title\">$title</strong></li>";
		} else {
			echo "<li><strong>".get_the_title()."</strong></li>";
		}
	}
	/*
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
	*/
    echo "</ul>";
}

function the_pagination () {
	
	global $wp_query;
	
	$big = 999999999; // need an unlikely integer
	
	$paginas = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'type' => 'array'
	) );
	
	if(isset($paginas)) {
		echo("<nav><ul class=\"pagination\">");
		foreach($paginas as $pagina) {
			if (strpos($pagina, "current") === false) {
				echo("<li>$pagina</li>");
			} else {
				echo("<li class=\"active\">$pagina</li>");
			}
		}
		echo("</ul></nav>");
	}
}

function get_page_parent ($id) {
	$page = get_page($id);
	
	return $page->post_parent;
}

function get_page_parent_high_level($id) {
	$page = get_page($id);
	
	if ($page->post_parent == 0) {
		return $page->ID;
	}
	else {
		return get_page_parent_high_level($page->post_parent);
	}
}

function paginas_especiais () {
	$tops = array(
				97,  // Graduação em Artes Visuais
				99,  // Pós-graduação em Artes Visuais
				692, // Departamento de Artes Plásticas
				
				105, // Graduação em Midialogia
				155, // Pós-graduação em Multimeios
				//99,  // Pós-graduação em Artes Visuais
				129, // Departamento de Cinema
				695, // Departamento de Multimeios, Mídia e Comunicação
				
				68,  // Graduação em Dança
				88,  // Pós-graduação em Artes da Cena
				119, // Departamento de Artes Corporais
				
				107, // Graduação em Música
				109, // Pós-graduação em Música
				698, // Departamento de Música
				
				80,  // Graduação em Artes Cênicas
				//92,  // Pós-graduação em Artes da Cena
				700  // Departamento de Artes Cênicas
	);
	
	return $tops;
}

function pagina_especial_filha ($id) {
	
	$objeto = get_post($id);
	
	if ($objeto->post_parent != 0) {
		if (in_array($objeto->post_parent,paginas_especiais())) {
			return true;
		}
		else {
			return pagina_especial_filha($objeto->post_parent);
		}	
	}
	
	return false;
	
}

function pagina_graduacao_geral ($id) {
	$objeto = get_post($id);
	
	if ($objeto->post_parent != 0) {
		if ($objeto->post_parent == 987) {
			return true;
		}
		else {
			return pagina_graduacao_geral($objeto->post_parent);
		}	
	}
	
	return false;
}

function paginas_graduacao_especificas () {
	
	$paginas = array(
				80,  // Graduação em Artes Cênicas
				97,  // Graduação em Artes Visuais
				68,  // Graduação em Dança
				105, // Graduação em Midialogia
				107  // Graduação em Música
	);
	
	return $paginas;
	
}

function pagina_graduacao_especifica_filha ($id) {
	
	$objeto = get_post($id);
	
	if ($objeto->post_parent != 0) {
		if (in_array($objeto->post_parent,paginas_graduacao_especificas())) {
			return true;
		}
		else {
			return pagina_graduacao_especifica_filha($objeto->post_parent);
		}	
	}
	
	return false;
}

function is_graduacao($id) {
	if ((in_array($id,paginas_graduacao_especificas())) || (pagina_graduacao_especifica_filha($id)) || (pagina_graduacao_geral($id))) {
		return true;		
	}
	return false;
}

function pagina_pos_graduacao_geral ($id) {
	$objeto = get_post($id);
	
	if ($objeto->post_parent != 0) {
		if ($objeto->post_parent == 168) {
			return true;
		}
		else {
			return pagina_pos_graduacao_geral($objeto->post_parent);
		}	
	}
	
	return false;
}

function paginas_pos_graduacao_especificas () {
	
	$paginas = array(
				99,  // Pós-graduação em Artes Visuais
				155, // Pós-graduação em Multimeios
				88,  // Pós-graduação em Artes da Cena
				109  // Pós-graduação em Música
	);
	
	return $paginas;
	
}

function pagina_pos_graduacao_especifica_filha ($id) {
	
	$objeto = get_post($id);
	
	if ($objeto->post_parent != 0) {
		if (in_array($objeto->post_parent,paginas_pos_graduacao_especificas())) {
			return true;
		}
		else {
			return pagina_pos_graduacao_especifica_filha($objeto->post_parent);
		}	
	}
	
	return false;
}

function is_pos_graduacao ($id) {
	if ((in_array($id,paginas_pos_graduacao_especificas())) || (pagina_pos_graduacao_especifica_filha($id)) || (pagina_pos_graduacao_geral($id))) {
		return true;
	}
	
	return false;
}

function paginas_departamento () {
	
	$paginas = array(
				700,  // Departamento de Artes Cênicas
				119,  // Departamento de Artes Corporais
				692,  // Departamento de Artes Plásticas
				129,  // Departamento de Cinema
				695,  // Departamento de Multimeios, Mídia e Comunicação
				698,  // Departamento de Música

	);
	
	return $paginas;
	
}

function menu_lateral ($str,$id = "") {
	
	if ((in_array($id,paginas_pos_graduacao_especificas())) || (pagina_pos_graduacao_especifica_filha($id))) {
		return "pos-graduacao";
	}
	
	if ((in_array($id,paginas_graduacao_especificas())) || (pagina_graduacao_especifica_filha($id))) {
		return "graduacao";
	}
	
	if ((in_array($id,paginas_departamento())) || (in_array(get_page_parent($id),paginas_departamento()))) {
		return "departamento";
	}
}

add_filter( "the_content", "filtro_email_spam", 20 );
function filtro_email_spam( $content ) {
	
	$pattern="/(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";
	preg_match_all($pattern, $content, $matches);
	
	$matches = array_unique($matches[0]);
	
	foreach ($matches as $email) {
		$email_filtrado = str_replace("@", " (arroba) ", $email);
		$email_filtrado = str_replace(".", " (ponto) ", $email_filtrado);
		
		$content = str_replace($email,$email_filtrado,$content);
	}

    return $content;
}

add_theme_support("menus");
/*
function fb_show_admin_bar() {
	if ( current_user_can( 'manage_options' ) ) {
		return true;
	}
	else {
		return false;
	}
	return false;
}
add_filter("show_admin_bar", "fb_show_admin_bar" );
*/


function paginas_area () {
	
	foreach (unserialize(AREAS) as $area => $codigo) {
		$filhas[$area] = get_children(array("post_type" => "page" , "post_parent" => $codigo),ARRAY_A);
	}
	
	foreach ($filhas as $area => $paginas) {
		foreach ($paginas as $pagina) {
			//$aux[$area][] = $pagina["ID"];
			$aux[] = $pagina["ID"];
		}	
	}
	
	return $aux;
}

function paginas_principais_curso () {
	$filhas = get_children(array("post_type" => "page" , "post_parent" => 63),ARRAY_A);
	
	foreach ($filhas as $key => $value) {
		$paginas[] = $key;
	}
	
	return $paginas;
}

function qual_curso($id) {
	$objeto = get_post($id);
	
	if ($objeto->post_parent == 63) {
		return $objeto->ID;
	}
	else {
		return qual_curso($objeto->post_parent);
	}
}

function get_area_name ($id) {
	switch (get_page_parent_high_level($id)) {
		case 97  : return "artes-visuais";
		case 99  : return "artes-visuais";
		case 692 : return "artes-visuais";
				
		case 105 : return "comunicacao";
		case 155 : return "comunicacao";
		case 129 : return "comunicacao";
		case 695 : return "comunicacao";
				
		case 68  : return "danca";
		case 88  : return "danca";
		case 119 : return "danca";
				
		case 107 : return "musica";
		case 109 : return "musica";
		case 698 : return "musica";
				
		case 80  : return "teatro";
		case 700 : return "teatro";
	}
	
}




function get_the_slug($tipo = "this") {
    if (is_home()) {
		return "home";
	}
	else {
		switch ($tipo) {
			case "this"	: {
				$post_data = get_post($post->ID);
				return $post_data->post_name;
				break;
			}
			
			case "curso" : {
				$post_data = get_post(qual_curso($post->ID));
				return $post_data->post_name;
				break;
			}
		}
	}
}

function body_slug () {
    if (is_home()) {
		return "home";
	}
	elseif ((is_page(paginas_especiais())) || pagina_especial_filha(get_the_ID())) {
		return "pagina-especial-com-slide " . get_area_name(get_the_ID());
	}
	elseif (pagina_pos_graduacao_geral(get_the_ID())) {
		return "pagina-especial-sem-slide";
	}
}

function get_the_css () {
	if (is_home()) { $css = "home"; }
	
	elseif (is_category()) { $css = "categoria"; }
	
	elseif (is_single()) { $css = "post"; }
	
	elseif ((in_array(get_the_ID(),paginas_especiais())) || pagina_especial_filha(get_the_ID())) { $css = "pagina-especial-com-slide"; }
	
	elseif ((pagina_pos_graduacao_geral(get_the_ID()) || (pagina_graduacao_geral(get_the_ID()))) || (get_the_ID() == 168)) { $css = "pagina-especial-sem-slide"; }
	
	elseif (is_page()) { $css = "page"; }
	
	return get_bloginfo("template_url") . "/css/" . $css . ".css";
}

function get_the_script () {
	if (is_home()) { $script = "home"; }
	
	elseif ((is_page(paginas_especiais())) || pagina_especial_filha(get_the_ID())) { $script = "pagina-especial-com-slide"; }
	
	elseif ((pagina_pos_graduacao_geral(get_the_ID()) || (pagina_graduacao_geral(get_the_ID()))) || (get_the_ID() == 168)) { $script = "pagina-especial-sem-slide"; }
	
	elseif (is_page()) { $script = "page"; }
	
	return get_bloginfo("template_url") . "/js/" . $script . ".js";
}



function menu_func($atts) {
	extract( shortcode_atts( array(
		"nome" => "nome"
	), $atts ) );

	return wp_nav_menu( array("menu" => $nome , "echo" => false , "container" => false));
}
add_shortcode("menu","menu_func");

function include_shortcode( $atts, $content = null ) {
	include("/usr/local/www/data/".$content);
}
add_shortcode("include","include_shortcode");

function php_shortcode( $atts, $content = null ) {
	//eval(htmlentities(html_entity_decode($content)));
	//echo(html_entity_decode($content,ENT_QUOTES));
	//echo(html_entity_decode($content, ENT_QUOTES, "Windows-1252"));
	//echo(html_entity_decode("&amp;"));
	
    $search = array("&#8217;","&#8220;","&#8221;","&gt;","&lt;","&#8211;"); 
    $replace = array("'","\"","\"",">","<","--"); 
 
    return eval(html_entity_decode(str_replace($search, $replace, $content)));
	//echo(html_entity_decode(str_replace($search, $replace, $content)));
	
}
add_shortcode("php","php_shortcode");

/*
function get_curso_nivel ($id) {
	$post_data = get_post($post->ID);
}*/

function stop_echo() {
	ob_start();
}

function print_echo() {
	$saida = ob_get_contents();
	ob_end_clean();
	echo(utf8_encode($saida));
}








/*
add_action( 'load-post.php', 'post_listing_page' );
function post_listing_page($post) {
	global $post;
	print_r($post);
}
*/


function get_permissoes ($grupo,$post_type) {
	$grupos["ceprod"]["post"] =  array();
	//$grupos["ceprod"]["page"] =  array(828,875,845,848);
	$grupos["ceprod"]["page"] =  array(828,875,845,848,4675,4680,4682,4684,4688);
	
	return $grupos[$grupo][$post_type];
}


add_filter('pre_get_posts', function ($query) {
	
	if (($query->is_admin) && (!current_user_can("manage_options"))) {
		global $post_type;
		$current_user = wp_get_current_user();
		$grupo = $current_user->roles[0];
		$query->set("post__in",get_permissoes($grupo,$post_type));
	}
	
	return $query;
});

add_action( 'admin_head-post.php', function () {

	//global $wp_roles;
	
	//print_r($wp_roles->role_names);
	
	//print_r($wp_roles->roles["ceprod"]["capabilities"]);
	
	/*
	$editor = get_role("editor");
	$ceprod = get_role("ceprod");
	
	foreach ($editor->capabilities as $key => $value) {
		$ceprod->add_cap($key);
	}
	
	print_r($ceprod->capabilities);
	*/
	
	if (!current_user_can("manage_options")) {
	
		$current_user = wp_get_current_user();
		$grupo = $current_user->roles[0];
	
		global $post;
	
		$permissoes = get_permissoes($grupo,$post->post_type);
		
		if (!empty($permissoes)) {
			if (!in_array($post->ID,$permissoes)) {
			
				switch ($post->post_type) {
					case "post" : { $titulo = "post"; $mensagem = "este post"; break; }
					case "page" : { $titulo = "página"; $mensagem = "esta página"; break; }
				}
			
				echo('<style>div#wpbody-content , div#wp-fullscreen-body {display:none;}  </style>');
				echo('<script>jQuery(document).ready(function () {
					jQuery("div#wp-fullscreen-body").html("");
					jQuery("div#wpbody-content").html("<div class=\"wrap\"><h2>Editar ' . $titulo . '</h2> <div class=\"error\"><p>Você não está autorizado a editar ' . $mensagem . '.</p></div></div>");
					jQuery("div#wpbody-content").show();
				});</script>');
			
			}	
		}
	}
});

function slider ($imagens) {
	$path = get_bloginfo("template_url") . "/img/slide/";
	echo("<div id=\"slider-wrapper\"><div id=\"slider\">");
	foreach ($imagens as $imagem) {			
		echo("<a href=\"javascript:void(0);\"><img src=\"" . $path . $imagem["src"] . "\" title=\"" . $imagem["title"] . "\" alt=\"" . $imagem["alt"] . "\" /></a>");
	}
	echo("</div></div>");
}

function menu_externo ($menus) {
	
	foreach ($menus as $menu) {
		if (strpos($menu," em ") !== false) {
			$container_id = "menu-especifico";
		}
		else {
			$container_id = "menu-comum";
		}
		wp_nav_menu( array("menu" => $menu , "container" => "nav" , "container_id" => $container_id));
	}
}

/**
* class Bootstrap_Walker_Nav_Menu()
*
* Extending Walker_Nav_Menu to modify class assigned to submenu ul element
*
* @author Rachel Baker
**/
class Bootstrapwp_Walker_Nav_Menu extends Walker_Nav_Menu {
	function __construct() {}
	function start_lvl($output, $depth) {

	$indent = str_repeat("\t", $depth);
	$output .= "\n$indent&lt;ul class=\"dropdown-menu\"&gt;\n";
	}
}

?>