<?php
	$tags = array(
		array("tag" => "meta" , "name" => "robots" , "content" => "noindex, nofollow")
	);

	get_header();
?>
<div class="row">
    <div class="col-md-9 listagem">
        <h3>Você buscou por: <em><?php echo($_GET["s"]); ?></em></h3>

        <?php if ( have_posts() ) :?>
            <h1>Resultados da busca</h1>
            <?php while ( have_posts() ) : the_post(); ?>
                <section<?php if ($wp_query->current_post == 0) { echo(" class=\"first\""); } ?>>
                    <?php if (has_post_thumbnail()) {?>
                        <a class="imagem" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail("home"); ?></a>
                    <?php } ?>
                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    <p><?php echo excerpt(80); ?></p>
                </section>
        <?php endwhile;
        
        the_pagination();
        
        else: ?>
            <p>Nada foi encontrado</p>
        <?php endif; ?>
    </div>
    
    <aside class="col-md-3">
        <?php get_sidebar(); ?>
    </aside>
</div>
<?php get_footer(); ?>