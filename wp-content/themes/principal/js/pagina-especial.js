$(document).ready(function () {
	$("div#menu-lateral nav ul").parent().children("a").addClass("submenu");

	$("div#menu-lateral nav ul a.submenu").each(function(index, element) {
        $(this).html("<b>+</b> " + $(this).html());
		$(this).attr("href","javascript:void(0);");
    });

	$("div#menu-lateral nav ul a.submenu").click(function(e) {
        $(this).parent().children("ul").slideToggle();
    });
	
	$("div#menu-lateral li.current-menu-item").parents("ul").show();
});