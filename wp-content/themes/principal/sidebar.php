<?php
	if (is_graduacao(get_the_ID())) {
		$categoria = 15;
	}
	elseif (is_pos_graduacao(get_the_ID())) {
		$categoria = 3;
	}
	else {
		$categoria = 1;
	}
?>

<h2>Notícias Relacionadas</h2>

<?php
$the_query = new WP_Query( array("cat" => $categoria , "posts_per_page" => 3));

while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<section<?php if ($the_query->current_post == 0) { echo(" class=\"first\""); } ?>>
		<?php /*if (has_post_thumbnail()) {?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail("home"); ?></a>
		<?php }*/ ?>
		<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
	</section>
<?php endwhile; wp_reset_postdata(); ?>