<?php
	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/page.css")
	);

	get_header();
?>

<div class="row">
    <div class="col-md-9">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <h1><?php the_title(); ?></h1>
            <?php the_breadcrumb(); ?>
            <?php the_content(); ?>
        <?php endwhile; else: ?>
            <p>Página não encontrada</p>
        <?php endif; ?>
    </div>
    
    <aside class="col-md-3">
        <?php get_sidebar(); ?>
    </aside>
</div>

<?php get_footer(); ?>