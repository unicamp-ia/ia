<?php
		
	$imagens = array(
		/*
		"artes-visuais" => array(
			array("src" => "artes-visuais/1.jpg" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => ""),
			array("src" => "artes-visuais/2.jpg" , "title" => "Foto: Geraldo Porto"        , "alt" => ""),
			array("src" => "artes-visuais/3.jpg" , "title" => "Foto: Priscila Guerra"      , "alt" => "")
		),
		*/
		
		"artes-visuais" => array(
			array("src" => "artes-visuais/_CAP5319.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
			array("src" => "artes-visuais/_CAP5336.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
			array("src" => "artes-visuais/DSC_0414.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
			array("src" => "artes-visuais/DSC_0426.jpg" , "title" => "Foto: Celso Palermo" , "alt" => "")
		),
		
		"comunicacao" => array(
			array("src" => "comunicacao/1.png" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => ""),
			array("src" => "comunicacao/2.png" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => ""),
			array("src" => "comunicacao/3.png" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => "")
		),
		
		"danca" => array(
			array("src" => "danca/1.jpg" , "title" => "Foto: Roberto Berton"  , "alt" => ""),
			array("src" => "danca/2.jpg" , "title" => "Foto: Roberto Berton"  , "alt" => ""),
			array("src" => "danca/3.jpg" , "title" => "Foto: Carlos Penteado" , "alt" => "")
		),
		
		"musica" => array(
			//array("src" => "musica/1.png" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => "")
			array("src" => "musica/2014_04_29_aulapiano_12187.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
			array("src" => "musica/edumusical-25.jpg" , "title" => "Foto: Celso Palermo" , "alt" => "")
		),
		
		"teatro" => array(
			array("src" => "teatro/2014_02_18_OdeDentroOdeFora_0002.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
			array("src" => "teatro/2014_02_18_OdeDentroOdeFora_0003.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
			array("src" => "teatro/27062013o_curtico_no_gramado47.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
			array("src" => "teatro/2013_06_12_Ossetegatinhos_0001.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
			array("src" => "teatro/2.jpg" , "title" => "Foto: Celso Palermo" , "alt" => "")
		)
	);
		
?>