<?php
/*
Template Name: Eventos
*/
?>
<?php
	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/eventos.css"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/eventos.js")
	);

	get_header();
?>
<div class="tabbable tabs-left">
    <ul class="nav nav-tabs">
        <?php
			if (is_single()) {
				$categorias = wp_get_post_terms($wp_query->post->ID, "tribe_events_cat");
				$categorias = $categorias[0];
				$categoria_atual = $categorias->name;
			}
			else {
				$categoria_atual = tribe_meta_event_category_name();
			}
		
            $categorias = get_terms(array("tribe_events_cat"),array("hide_empty" => 0));
            foreach ($categorias as $categoria) {
            	echo("<li");
				if ($categoria_atual == $categoria->name) { echo(" class=\"active\""); }
				echo("><a href=\"" . get_term_link($categoria) . "\">" . $categoria->name . "</a></li>\n");
            }
        ?>
    </ul>
    <div class="tab-content">
    <div class="tab-pane active">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        <?php endwhile; else: ?>
            <p>Página não encontrada</p>
        <?php endif; ?>
    </div>
    </div>
</div>

<?php get_footer(); ?>