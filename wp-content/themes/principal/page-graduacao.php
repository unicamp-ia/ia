<?php
/*
Template Name: Graduação
*/
?>
<?php
	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/pagina-especial-sem-slide.css"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/pagina-especial.js"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/pagina-especial-sem-slide.js")
	);
	
	get_header();
?>
<div class="row">
    <div id="menu-lateral" class="col-md-3">
        <?php include("lateral-graduacao.php"); ?>
    </div>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
        <div class="col-md-6">
            <h1><?php the_title(); ?></h1>
        
            <?php the_breadcrumb(); ?>
            
            <?php the_content(); ?>
        </div>
    
    <?php endwhile; endif; ?>
    
    <aside class="col-md-3">
        <?php get_sidebar(); ?>
    </aside>
</div>
<?php get_footer(); ?>