<!doctype html>
<!--[if lt IE 7]> <html class="ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->

<head>
<meta charset="<?php bloginfo("charset"); ?>" />
<title><?php bloginfo("name");?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" id="viewport" <?php /*content="width=1000"*/?>/>

<?php if (is_search()) { ?><meta name="robots" content="noindex, nofollow" /><?php } ?>

<?php
	$template_directory = get_bloginfo('template_directory');
	$stylesheet_url = get_bloginfo('stylesheet_url');
	
	if ($_SERVER['HTTPS'] == 'on') {
		$template_directory = str_replace('http://', 'https://', $template_directory);
		$stylesheet_url = str_replace('http://', 'https://', $stylesheet_url);
	}
?>

<link rel="apple-touch-icon" href="<?php echo($template_directory); ?>/img/icons/apple57.png"/>  
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo($template_directory); ?>/img/icons/apple72.png"/>  
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo($template_directory); ?>/img/icons/apple114.png"/> 
<link rel="shortcut icon" href="<?php echo($template_directory); ?>/img/icons/favicon.ico" />

<link rel="stylesheet" type="text/css" href="<?php echo($template_directory); ?>/css/bootstrap.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo($stylesheet_url); ?>" media="screen" />
<link rel="pingback" href="<?php bloginfo("pingback_url"); ?>" />

<!--[if lt IE 9]><script type="text/javascript" src="<?php echo($template_directory); ?>/js/ie8.js"></script><![endif]-->

<script type="text/javascript" src="<?php echo($template_directory); ?>/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo($template_directory); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo($template_directory); ?>/js/script.js"></script>

<?php
	global $tags;
	if (isset($tags)) {
		foreach ($tags as $tag) {
			switch ($tag["tag"]) {
				case "link"   : { echo("<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $tag["href"] . "\" media=\"" . (isset($tag["media"]) ? $tag["media"] : "all") . "\" />\n"); break;}
				case "script" : { echo("<script type=\"text/javascript\" src=\"" . $tag["src"] . "\"></script>\n"); break;}
				case "meta"   : { echo("<meta name=\"" . $tag["name"] . "\" content=\"" . $tag["content"] . "\" />\n"); break;}
			}
		}
	}
?>

<?php wp_head(); ?>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-3176901-1', 'unicamp.br');
	ga('send', 'pageview');
        
        function versaoDesktop() {
            var $viewport = $('head meta[name="viewport"]');    
            $viewport.attr('content', 'width=1000');
        }
        
        jQuery(function($){
            $('.menu-btn').click(function(){
               $('.responsive-menu').toggleClass('expand');
            })
            
            $(window).resize(function() {
                alteraMenu();
            });
        })
        
        function alteraMenu() {
          
            if ($('#desktop').css('display') === 'none' ) {
                //$('.menu-item-has-children').append('<div id="caretmenu" class="caret"></div>');
                $('.menu-item-has-children').addClass('dropdown');
                $('.sub-menu').addClass('dropdown-menu');
                //$('.menu').addClass('nav');
               // $('.menu').addClass('navbar-nav');
                
                //$('.nav .open > a, .nav .open > a:hover, .nav .open > a:focus').css("background-color", "none");
                //$('.nav .open > a, .nav .open > a:hover, .nav .open > a:focus').css("border-color", "none");
                
                $('.menu-item').addClass('ajustamenu-mobile');
                $('.menu-item').addClass('corsite');
                
                

                $('.menu-item-has-children').mouseover(function(){

                    $('.menu-item-has-children').addClass('open');
                }).mouseout(function(){

                    $('.menu-item-has-children').removeClass('open');
                });

                //jQuery(function($){
                //    $( '.menu-item-has-children' ).click(function(){
                        //$('.caret').toggleClass('expand')
                //    })
                //})
            } else {
                $('.menu-item-has-children').removeClass('dropdown');
                $('.sub-menu').removeClass('dropdown-menu');
                $('.menu-item-has-children').removeClass('open');
                $('.menu-item').removeClass('corsite');
                $('.menu-item').addClass('corsite');
                //$('.menu').removeClass('nav');
                //$('.menu').removeClass('navbar-nav');
                $('.menu-item').removeClass('ajustamenu-mobile');
                //$('.menu-item-has-children').remove('#caretmenu');
            }
        }
                
        jQuery(document).ready(function($) {
            alteraMenu();            
        });
    
        
</script>

                
</head>

<body <?php body_class(body_slug()); ?>>

<header>
    <div id="desktop">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if (is_home()) { ?><h1 id="logo">Instituto de Artes</h1><?php }
                    else { ?><a id="logo" href="<?php bloginfo("url"); ?>">Instituto de Artes</a><?php } ?>

                    <?php wp_nav_menu( array("menu" => "Principal" , "container" => "nav" , "container_id" => "nav-main")); ?>

                    <div id="unicamp"><a href="http://www.unicamp.br" target="_blank"><img src="<?php bloginfo("template_url"); ?>/img/unicamp.png" /></a></div>
    
                    <form id="pesquisar" action="<?php bloginfo("url"); ?>" method="get" class="col-md-3">
                        <div class="input-group">
                                                <?php
                                                $url = "http://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                                                $urls = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];			
                                                if (($url !== pll_home_url()) && ($urls !== pll_home_url())) { //Para n�o exibir na homepage
                                                ?>
                                                        <div id="linguagem">
                                                                <?php pll_the_languages(array('show_flags'=>1,'show_names'=>1, 'hide_if_no_translation'=>1, 'hide_current'=>1, 'force_home'=>0));?>   
                                                        </div>
                                                <?php } ?>
                            <input type="text" class="form-control" placeholder="Pesquisar" name="s" id="s" />
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default" aria-label="Pesquisar" title="Pesquisar">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </button>
                            </span>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <?php
            global $menu;
            if (isset($menu)) {
                wp_nav_menu( array("menu" => $menu , "container" => "nav" , "container_id" => "nav-area" , "menu_class" => "container"));
            }

            ?>
    </div>
    <div id="mobile">
        <div class="container" >
            <div class="row config">
                <div class="col-md-12">
                    <div><a href="<?php bloginfo("url");?>"><img id="logo-ia-mobile" src="<?php bloginfo("template_url"); ?>/img/logo_mobile.png"/><img id="titulo-mobile" src="<?php bloginfo("template_url"); ?>/img/descricao_ia_mobile.png"/></a></div>
                      
                        <?php
                        $url = "http://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                        $urls = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];			
                        if (($url !== pll_home_url()) && ($urls !== pll_home_url())) { //Para n�o exibir na homepage
                        ?>
                            <div id="linguagem-mobile">
                                    <?php pll_the_languages(array('show_flags'=>1,'show_names'=>0, 'hide_if_no_translation'=>1, 'hide_current'=>1, 'force_home'=>0));?>   
                            </div>
                        <?php } ?>
                    
                    <div id="unicamp-mobile"><a href="http://www.unicamp.br" target="_blank"><img src="<?php bloginfo("template_url"); ?>/img/unicamp-mobile.png" /></a></div>
                </div>
            </div>
            
            <div id="divpesq" class="row">
                <div class="col-md-12">
                    <div class="mobile-nav">
                        <div class="menu-btn" id="menu-btn">
                            <button id="btn-menu-principal-mobile">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    </div>
                    <form action="<?php bloginfo("url"); ?>" method="get" class="col-md-4" id="form-pesq">
                        <div class="input-group pesquisar-mobile">
                            <input type="text" class="form-control" placeholder="Pesquisar" name="s" id="s" />
                        </div>
                    </form> 
                </div>
                <div id="divsobpesq" class="row"></div>
                <div class="responsive-menu container-fluid">
                    <nav>
                        <?php wp_nav_menu(array("menu" => "Principal" , 'container' => 'div', "container_class" => "nav" ) ); ?>
                    </nav>
                </div>    
            </div>
        </div>
        
    </div>    
</header>

<div class="container">
	<div id="content">