<div class="row">
	<div class="col-xs-12 col-md-1 mb-4" id="brand-menu">
		<div class="row">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-gaia.png" width="100%"/>
		</div>
	</div>
	<div class="col-xs-12 col-md-11 pr-0 mb-4">
		<nav class="navbar navbar-gaia">
			<div class="navbar-header">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main1">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				</a>
			</div>
			<div class="navbar-collapse collapse" id="navbar-main1">
				<div class="row">
					<?php 
					wp_nav_menu( array(
						//Adicionar GAIA no wp-admin/nav-menus.php
						'menu'            => 'GAIA',
						'depth'	          => 2, // 1 = no dropdowns, 2 = with dropdowns.
						'menu_class'      => 'nav nav-justified',
						'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
						'walker'          => new WP_Bootstrap_Navwalker(),
					) );
					?>
				</div>
			</div>
		</nav>
	</div>
</div>

<?php if(false){ //somente para debugar ?>
<nav class="navbar navbar-gaia">
	<ul class="nav nav-justified">
		<li class="active"><a href="<?php bloginfo("wpurl") ?>">Home</a></li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">Galeria<span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">A Galeria</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">História</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Exposições Anteriores</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Artistas</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Como expor</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Conselho de Arte</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Equipe Técnica</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">A Galeria na Mídia - Artigos</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">A Galeria na Mídia - Vídeos</a></li>
			</ul>
		</li>
		<li><a href="<?php bloginfo("wpurl") ?>/gaia-virtual">Tour Virtual</a></li>	
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">Eventos<span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Palestras</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Dança</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Música</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Visita Monitorada</a></li>
				<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Outros Eventos</a></li>
			</ul>
		</li>										
		<li><a href="<?php bloginfo("wpurl") ?>/conteudo">Links</a></li>
		<li><a href="<?php bloginfo("wpurl") ?>/contato">Contato</a></li>
	</ul>
</nav>
<?php } ?>