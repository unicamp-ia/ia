﻿<?php
/**
 * Template Name: GAIA-Conteúdo
 * The template para o GAIA
 */
 
	$tags = array(		
		//PARA GAIA
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/fontawesome-free-5.4.1-web/css/all.css"),
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/gaia-style.css?v2")
	);	
	get_header();	
	include('_menu.php');
?>
		<div class="col-xs-12 col-md-12"  id="conteudo-pagina">
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="span6">
				<?php the_breadcrumb(); ?>	
				<h1><?php the_title(); ?></h1>		
				<?php the_content(); ?>
			</div>
		<?php endwhile; else: ?>
			<p>Página não encontrada.</p>
		<?php endif; ?>
		
		</div>					
	
<?php get_footer(); ?>
</body>
</html>