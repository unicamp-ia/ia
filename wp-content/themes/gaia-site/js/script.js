$(document).ready(function () {
	if ("ontouchstart" in document.documentElement) {
		document.documentElement.className += " touch";
	}
	else {
		document.documentElement.className += " no-touch";
	}
	
	$("header ul li ul li ul").each(function(index, element) {
        $(this).css("left",$(this).parent().parent().width()+"px");
    });
	
	$("ul#menu-principal li.menu-item-has-children > a").click(function(e) {
		var li = $(this).parent();
		var ul = li.children("ul");
		
		if (ul.hasClass("show")) {
			ul.removeClass("show");
		}
		else {
			$("ul.show").each(function(index, element) {
				if ($("li#"+$(this).parent("li").attr("id")+" li#"+li.attr("id")).length == 0) {
					$(this).removeClass("show");
				}
			});
			
			ul.addClass("show");
		}
		
    });
	
	$(document).on('touchstart', function (e) {	
		var target = $(e.target);    
		if (!target.parents("ul#menu-principal").length) {
			$("ul.show").removeClass("show");
		}
    });

	
	$("nav#areas ul li ul").append("<li class='seta'></li>");
	
	if (!$.isEmptyObject($.find('#content'))) {
		
		content = $("div#content").html();
		
		if (content.indexOf("(arroba)") != -1) {
			$("div#content").html($("div#content").html().replace(/ \(arroba\) /g,"@").replace(/ \(ponto\) /g,"."));	
		}	
	}
	

	$('#myTab a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});
	
	$("body.page-id-29 a[rel|='popover']").popover({
		"content" : "kkkkkk",
		"placement" : "left",
		"trigger" : "focus"
	});
	
	$("div.fb-like-box").attr("data-width",$("div.col-md-3").width());
});