$(document).ready(function($){
	var icontop = $('#ancoratopo');	
	icontop.hide();	
	$(window).scroll(function(){		
		if ($(this).scrollTop() > 350){
			icontop.fadeIn('slow');
		}else{
			icontop.fadeOut('slow');
		};		
	});		
});

//Bootstrap ToolTip
$(function(){
  $('[data-toggle="tooltip"]').tooltip()
});

//Colorbox
$(document).ready(function(){
	$(".group").colorbox({rel:'group', width:"50%", transition:"fade", slideshow:true});;
	$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
});

//Mask
var SPMaskBehavior = function (val) {
	return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
	onKeyPress: function(val, e, field, options) {
		field.mask(SPMaskBehavior.apply({}, arguments), options);
	}
};
$('#txt_tel').mask(SPMaskBehavior, spOptions);	


//fixed menu
$(document).ready(function(){
	var header 	= $('#header-global').height()+86; 	
	var menu 	= $('.navbar');
	var logoBranco 	= $('.logoBranco');
	
	logoBranco.hide();
	
	$(window).scroll(function(){
		if ($(this).scrollTop() > header){								
			menu.attr('style', "opacity: 0.9; position: fixed; top: 0; z-index: 5");
			logoBranco.fadeIn('slow');
		}else{
			menu.attr('style', "opacity: 1;");
			logoBranco.fadeOut('slow');
		}	
	});
});

$(function(){
	$('.deslizar').on('click', function(e) {
		event.preventDefault();
		var idElemento = $(this).attr("href");
		var deslocamento = $(idElemento).offset().top;
		$('html, body').animate({ scrollTop: deslocamento }, 'slow');
	});
});