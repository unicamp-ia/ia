﻿<?php
/**
* Template Name: GAIA-Virtual
* The template para o GAIA
*/
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: 0');

function my_theme_custom_mimes( $existing_mimes ) {
	$existing_mimes['json'] = 'application/json';
	$existing_mimes['unityweb'] = 'application/octet-stream';
	$existing_mimes['data'] = 'application/octet-stream';
	$existing_mimes['woff2'] = 'application/x-font-woff2';
	return $existing_mimes;
}
add_filter('mime_types', 'my_theme_custom_mimes');

	$tags = array(
		//PARA GAIA
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/fontawesome-free-5.4.1-web/css/all.css"),
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/gaia-style.css?v2"),
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/gaia-virtual-style.css")
	);
	get_header();
	include('_menu.php');
?>
	<?php
		function isMobile() {
			return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
		}
		if(isMobile()){
	?>

		<div class="row">
			<div class="content-page text-center">
				<div class="p-4 m-5" style="color: white; font-size: 300%;">
					<big>O GAIA Virtual possui uma melhor performace e te proporciona uma melhor experiência quando acessado pelo Computador de Mesa. Então pedimos que acesse este link através desse dispositivo e nos vemos lá!</big>
				</div>
			</div>
		</div>

	<?php
		}else{
	?>
	<div class="row">

		<div class="col-xs-12 col-md-12">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
			the_breadcrumb();
			the_content();
			endwhile; else: ?>
			<p>Conteúdo não disponível.</p>
			<?php endif; ?>
		</div>
		<br clear="both"/>

		<div class="content-page text-center">
			<div class="p-4 mb-4" style="color: white; font-size: 110%;">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/018_-_Expand-512.png" class="hint-icon" width="60" align="left"/>
				<big>Após carregar, aperte [F] para ativar ou desativar o modo de tela cheia.<br/>Para exibir o cursor do mouse, pressione [ESC].</big>
			</div>
		</div>


		<div class="col-xs-12 col-md-12 content-page px-0">

			<div class="loader pt-5 px-1">
				<div id="logo-loading-comporta" class="my-5">
					<div class="container">
						<div class="row">
							<div id="loading-total-logo" class="col-xs-12 col-md-6 text-center p-5">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-gaia-virtual.png" class="logo-gaia"/>
							</div>
							<div id="loading-total" class="col-xs-12 col-md-6 text-center">
								<div class="loader-icon"></div>
								<span class="text">Carregando sua experiência...</span>
								<br/>
								<div id="dicasCarousel" class="carousel fadeIn slide text-center p-2 my-4" data-interval="5000" data-ride="carousel">
									<i class="fas fa-lightbulb"></i> <strong>DICA de atalho</strong>
									<div class="carousel-inner" role="listbox">
										<div class="item active">
											Para dar zoom e ver os detalhes da obra, aperte e mantenha o botão direito do seu mouse pressionado.
										</div>
										<div class="item">
											Para habilitar <i class="fas fa-volume"></i> e desabilitar o som em qualquer momento <i class="fas fa-volume-mute"></i><br/> aperte o [M] de seu teclado <i class="fas fa-keyboard"></i>.
										</div>
										<div class="item">
											Para habilitar e desabilitar a tela inteira em qualquer momento<br/> aperte o [F] de seu teclado <i class="fas fa-keyboard"></i>.
										</div>
										<div class="item">
											Para andar pelo espaço <i class="fas fa-shoe-prints"></i><br/> use as teclas de direção do seu teclado <i class="fas fa-keyboard"></i>.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 text-right p-2 mt-5">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-unicamp.png" width="200"/>
				</div>
			</div>

			<div class="webgl-content">
				<div id="gameContainer" style="width: auto; height: 100%"></div>
			</div>


		</div>
	</div>


	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/webgl/Build/UnityLoader.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/_main.js?v2"></script>
	<script>var gameInstance = UnityLoader.instantiate("gameContainer", "<?php echo esc_url( get_template_directory_uri() ); ?>/webgl/Build/webGL.json", {onProgress: UnityProgress});</script>

	<?php
		}
	?>

<?php get_footer(); ?>

	<script type="text/javascript">

		<?php //Código necessário para solucionar bug de versão de Jquery com a versão do WebGL Unity ?>
		jQuery.browser = {};
		(function () {
			jQuery.browser.msie = false;
			jQuery.browser.version = 0;
			if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
				jQuery.browser.msie = true;
				jQuery.browser.version = RegExp.$1;
			}
		})();
	</script>

</body>
</html>