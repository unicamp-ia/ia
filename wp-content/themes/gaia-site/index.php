﻿<?php
/**
 * Template Name: GAIA-Index
 * The template para o GAIA
 */
 ?>
<?php
	$tags = array(
		//array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/nivo-slider.css"),
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/home.css"),
		//array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/jquery.nivo.slider.pack.js"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/home.js"),
		
		//PARA GAIA
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/fontawesome-free-5.4.1-web/css/all.css"),
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/gaia-style.css?v2")
	);	
	get_header();	
	include('_menu.php');
?>

	<div class="row" id="conteudo-pagina">
		<div class="col-xs-12 col-md-12 px-0">
			
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="item active"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/001.jpg" width="100%"/></div>
					<div class="item"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/002.jpg" width="100%"/></div>
					<div class="item"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/003.jpg" width="100%"/></div>
				</div>
				
				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<i class="fas fa-angle-left" style="font-size: 30px; position: absolute; top: 50%;"></i>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<i class="fas fa-angle-right" style="font-size: 30px; position: absolute; top: 50%;"></i>
					<span class="sr-only">Next</span>
				</a>
			</div>
			
		</div>
		<br clear="both"/>
		<br clear="both"/>
		<div class="col-xs-12 col-md-3">							
			<div id="bem-vindo" class="panel panel-default">
				<?php 
					//Localhost
					//$box1 = 'page_id=81';
					//$box2 = 'page_id=82';
					//$box3 = 'page_id=83';
					
					//IA
					$box1 = 'page_id=40';
					$box2 = 'page_id=42';
					$box3 = 'page_id=44';
				?>
				
				
				<?php $the_query = new WP_Query( $box1 ); ?>
				<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
				<div class="panel-heading">
					<h3 class="panel-title"><?php the_title(); ?></h3>
				</div>
				<div class="panel-body" style="min-height: 230px;">
					<?php the_content(); ?>
				</div>
				<?php endwhile;?>
			</div>							
		</div>
		<div class="col-xs-12 col-md-3">							
			<div id="bem-vindo" class="panel panel-default">
				<?php $the_query = new WP_Query( $box2 ); ?>
				<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
				<div class="panel-heading">
					<h3 class="panel-title"><?php the_title(); ?></h3>
				</div>
				<div class="panel-body" style="min-height: 230px;">
					<?php the_content(); ?>
				</div>
				<?php endwhile;?>
			</div>							
		</div>
		<div class="col-xs-12 col-md-3">							
			<div id="bem-vindo" class="panel panel-default">
				<?php $the_query = new WP_Query( $box3 ); ?>
				<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
				<div class="panel-heading">
					<h3 class="panel-title"><?php the_title(); ?></h3>
				</div>
				<div class="panel-body" style="min-height: 230px;">
					<?php the_content(); ?>
				</div>
				<?php endwhile;?>
			</div>							
		</div>
		<div class="col-xs-12 col-md-3">							
			<div id="bem-vindo" class="panel panel-default">
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
				<header>
				<div class="panel-heading">
					<h3 class="panel-title">Siga-nos no Facebook</h3>
				</div>
				<div class="panel-body" style="background: white;">								
				<div class="row">								
					<div class="fb-like-box" data-href="https://www.facebook.com/Galeria-do-Instituto-de-Artes-GAIA-Unicamp-471702572856101" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
				</div>
				</div>
			</div>							
		</div>
	</div>
	<div class="row gaia-map" id="conteudo-pagina">
		<div class="col-xs-12 col-md-4">
			<h3><strong>Galeria de Arte Unicamp/IA</strong></h3>
			<p>							
			Rua Sérgio Buarque de Holanda, s/no.<br/>
			Prédio da Biblioteca Central - Térreo<br/>
			CEP 13083-859<br/>
			Cidade Universitária, Campinas - SP</p>

			<p><i class="fas fa-phone-square"></i> +55 (19) 3521-7453 ou 3521-6561</p>
			<p><i class="fas fa-envelope-square"></i> galeria@iar.unicamp.br</p>
			
			Seg - Sex | 9:00 às 17:00
		</div>
		<div class="col-xs-12 col-md-8">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7355.229921420527!2d-47.07247767503271!3d-22.81672694023072!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c8c14cc13bca81%3A0xe138fe0fdba36f13!2sGaleria+de+Arte+Unicamp!5e0!3m2!1sen!2sbr!4v1539115400408" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>					

    
<?php get_footer(); ?>
</body>
</html>