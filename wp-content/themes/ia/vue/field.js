Vue.component('ia-field', {
    props: {
        label: String,
        value: String,
    },

    template: `
  <div class="form-group" v-if="value">
      <p class="mb-0 bold">{{label}}</p>
      <div v-html="value"></div>
    </div>
  `
});
