Vue.component('ia-equipamentos', {
    props: {
        intranet_path: String,
    },
    data: function () {
        return {
            nome: '',
            responsavel: '',
            allRows: [],
        }
    },
    created() {

        axios.get(this.intranet_path + '/api/extensao/equipamento')
            .then( resp => {
                this.allRows = resp.data
            });
    },
    computed: {
        filteredRows: function () {
            return this.allRows.filter(item => {
                var isValid = true

                if (item.tipo.toLowerCase().includes(this.nome.toLowerCase()) === false) {
                    isValid = false
                }

                if (item.responsavel
                    && item.responsavel.nome.toLowerCase().includes(this.responsavel.toLowerCase()) === false) {
                    isValid = false
                }

                if (isValid) {
                    return item
                }

            });
        },
    },
    template: `
  <v-container fluid data-app>
    <v-form>
      <v-row class="row">
        <v-col cols="12" sm="4">
          <v-text-field
            v-model="nome"
            label="Título"
          ></v-text-field>
        </v-col>
        <v-col cols="12" sm="4">
          <v-text-field
            v-model="responsavel"
            label="Responsável"
          ></v-text-field>
        </v-col>       
      </v-row>
    </v-form>
    
    <v-row justify="center">
        <v-expansion-panels accordion>
            <v-expansion-panel v-if="filteredRows.length == 0">
              <v-expansion-panel-header class="d-flex justify-space-between p-3">
                Nenhum registro encontrado
              </v-expansion-panel-header>
            </v-expansion-panel>
            <v-expansion-panel
                v-for='item in filteredRows'
                :key="item.id" 
            >
              <v-expansion-panel-header class="d-flex justify-space-between p-3">
                  <div class="text-left">
                    <strong>{{item.tipo}}</strong>
                  </div>
                  <div class="d-flex justify-content-end">
                    <v-chip small v-if="item.departamento" outlined>
                      {{item.departamento.descricao}}
                    </v-chip>
                  </div>
              </v-expansion-panel-header>
              
              <v-expansion-panel-content class="p-0">
                                
                <div class="form-group" v-if="item.caracteristicas">
                  <p class="mb-0 bold">Características</p>
                  <div v-html="item.caracteristicas"></div>
                </div>

                <div class="form-group" v-if="item.dataAquisicao">
                  <p class="mb-0 bold">Data de aquisição</p>
                  <div v-html="item.dataAquisicao"></div>
                </div>
                
                <div class="form-group" v-if="item.localizacao">
                  <p class="mb-0 bold">Localização</p>
                  <div v-html="item.localizacao"></div>
                </div>
                
                <div class="form-group" v-if="item.marca">
                  <p class="mb-0 bold">Marca</p>
                  <div v-html="item.marca"></div>
                </div>
                
                <div class="form-group" v-if="item.modelo">
                  <p class="mb-0 bold">Modelo</p>
                  <div v-html="item.modelo"></div>
                </div>
                
                <div class="form-group" v-if="item.natureza">
                  <p class="mb-0 bold">Natureza</p>
                  <div v-html="item.natureza"></div>
                </div>
                
                <div class="form-group" v-if="item.pi">
                  <p class="mb-0 bold">PI</p>
                  <div v-html="item.pi"></div>
                </div>
                
                <div class="form-group" v-if="item.responsavel">
                  <p class="mb-0 bold">Responsável</p>
                  <div v-html="item.responsavel.nome"></div>
                </div>
                
                <div class="form-group" v-if="item.valor">
                  <p class="mb-0 bold">Valor</p>
                  <div v-html="item.valor"></div>
                </div>
                
                
                
                <!-- More Info -->                
                <div class="form-group" v-if="item.contato">
                  <div v-html="item.contato"></div>
                </div>
                
                      
              </v-expansion-panel-content>
                
            </v-expansion-panel>
        </v-expansion-panels>
    </v-row>
    
  </v-container>
  `
});
