Vue.component('ia-projetos', {
    props: {
        intranet_path: String,
    },
    data: function () {
        return {
            nome: '',
            responsavel: '',
            allRows: [],
        }
    },
    created() {
        axios.get(this.intranet_path + '/api/extensao/projeto')
            .then( resp => this.allRows = resp.data);
    },
    computed: {
        filteredRows: function () {
            return this.allRows.filter(item => {
                var isValid = true

                if (item.titulo.toLowerCase().includes(this.nome.toLowerCase()) === false) {
                    isValid = false
                }

                if (item.pesquisador
                    && item.pesquisador.nome.toLowerCase().includes(this.responsavel.toLowerCase()) === false) {
                    isValid = false
                }

                if (isValid) {
                    return item
                }

            });
        },
    },
    template: `
  <v-container fluid data-app>
    <v-form>
      <v-row class="row">
        <v-col cols="12" sm="4">
          <v-text-field
            v-model="nome"
            label="Título"
          ></v-text-field>
        </v-col>
        <v-col cols="12" sm="4">
          <v-text-field
            v-model="responsavel"
            label="Pesquisador"
          ></v-text-field>
        </v-col>       
      </v-row>
    </v-form>
    
    <v-row justify="center">
        <v-expansion-panels accordion>
            <v-expansion-panel v-if="filteredRows.length == 0">
              <v-expansion-panel-header class="d-flex justify-space-between p-3">
                Nenhum registro encontrado
              </v-expansion-panel-header>
            </v-expansion-panel>
            <v-expansion-panel
                v-for='item in filteredRows'
                :key="item.id" 
            >
              <v-expansion-panel-header class="d-flex justify-space-between p-3">
                  <div class="text-left">
                    <strong>{{item.titulo}}</strong>
                  </div>
              </v-expansion-panel-header>
              
              <v-expansion-panel-content class="p-0">
                <ia-field label="Situação" :value="item.situacao"></ia-field>
              
                <ia-field label="Pesquisador responsável" :value="item.pesquisador.nome"></ia-field>
              
                <ia-field label="Instituições" :value="item.instituicoes"></ia-field>
                
                <ia-field label="Grupo de Pesquisa" :value="item.grupo_pesquisa.nome"></ia-field>
                
                <ia-field label="Programa" :value="item.programa.nome"></ia-field>
                
                <ia-field label="Natureza" :value="item.natureza"></ia-field>
                
                <ia-field label="Início" :value="item.grupo_pesquisa.inicio"></ia-field>
                
                <ia-field label="Término" :value="item.grupo_pesquisa.inicio"></ia-field>
                
                <ia-field label="Resumo" :value="item.resumo"></ia-field>
                
                
                      
              </v-expansion-panel-content>
                
            </v-expansion-panel>
        </v-expansion-panels>
    </v-row>
    
  </v-container>
  `
});
