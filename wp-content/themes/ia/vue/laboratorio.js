Vue.component('ia-laboratorio', {
    props: {
        intranet_path: String,
    },
    data: function () {
        return {
            nome: '',
            coordenador: '',
            departamento: '',
            allRows: [],
            optionsDepartamento: [
                'Todos',
            ],
        }
    },
    created() {
        axios.get(this.intranet_path + '/api/extensao/laboratorio')
            .then( resp => this.allRows = resp.data);

        axios.get(this.intranet_path + '/api/extensao/laboratorio/departamento')
            .then( resp => {
                const deptos = resp.data.map( item => item.descricao)
                this.optionsDepartamento = this.optionsDepartamento.concat(deptos);
            });
    },
    computed: {
        filteredRows: function () {
            return this.allRows.filter(item => {
                var isValid = true

                if (item.nome.toLowerCase().includes(this.nome.toLowerCase()) === false) {
                    isValid = false
                }

                if (item.coordenador
                    && item.coordenador.nome.toLowerCase().includes(this.coordenador.toLowerCase()) === false) {
                    isValid = false
                }

                if (this.departamento !== 'Todos'
                    && item.departamento.descricao.toLowerCase().includes(this.departamento.toLowerCase()) === false) {
                    isValid = false
                }

                if (isValid) {
                    return item
                }

            });
        },
    },
    template: `
  <v-container fluid data-app>
    <v-form>
    <strong>Filtro</strong>
      <v-row class="row">
        <v-col cols="12" sm="4">
          <v-text-field
            v-model="nome"
            label="Título"
          ></v-text-field>
        </v-col>
        <v-col cols="12" sm="4">
          <v-text-field
            v-model="coordenador"
            label="Coordenador"
          ></v-text-field>
        </v-col>
        <v-col cols="12" sm="4">
          <v-select v-model="departamento" 
          :items="optionsDepartamento" 
          label="Departamento"
          >
          </v-select>
        </v-col>
      </v-row>
    </v-form>
    
    <v-row justify="center">
        <v-expansion-panels accordion>
            <v-expansion-panel v-if="filteredRows.length == 0">
              <v-expansion-panel-header class="d-flex justify-space-between p-3">
                Nenhum registro encontrado
              </v-expansion-panel-header>
            </v-expansion-panel>
            <v-expansion-panel
                v-for='item in filteredRows'
                :key="item.id" 
            >
              <v-expansion-panel-header class="d-flex justify-space-between p-3">
                  <div class="text-left">
                    <strong>{{item.nome}}</strong>
                  </div>
                  <div class="d-flex justify-content-end">
                    <v-chip small v-if="item.departamento" outlined>
                      {{item.departamento.descricao}}
                    </v-chip>
                  </div>
              </v-expansion-panel-header>
              
              <v-expansion-panel-content class="p-0">
                <!-- descricao-->
                <div v-if="item.descricao" v-html="item.descricao"></div>

                <!-- Coordenador-->                
                <div class="form-group">
                  <p class="mb-0 bold">Coordenador</p>
                  <p class="mt-0 mb-1" v-if="item.coordenador">
                    {{ item.coordenador.nome }}
                    <a v-if="item.coordenador.linkLattes" v-bind:href="item.coordenador.linkLattes" target="_blank">
                      <span data-tooltip="Currículo Lattes" data-position="right center">
                          <img style="max-height: 16px" src="https://sobral.ufc.br/wp-content/uploads/2020/04/lattes.svg">
                      </span>
                    </a>
                  </p>
                </div>
                <div class="form-group">
                  <p class="mt-0 mb-1" v-if="item.laboratorioTecnicos">
                    <p class="mb-0 bold">Técnico(s)</p>
                    <div v-for='tecnico in item.laboratorioTecnicos' :key="tecnico.id">
                        {{tecnico.user.nome}}
                        <a v-if="tecnico.user.linkLattes" v-bind:href="tecnico.user.linkLattes" target="_blank">
                          <span data-tooltip="Currículo Lattes" data-position="right center">
                              <img style="max-height: 16px" src="https://sobral.ufc.br/wp-content/uploads/2020/04/lattes.svg">
                          </span>
                        </a>
                    </div>
                  </p>
                </div>
                
                <!-- Departamento -->                
                <div class="form-group" v-if="item.departamento">
                  <div class="bold" v-html="item.departamento.descricao"></div>
                </div>
                
                <!-- Equipamentos -->                
                <div class="form-group" v-if="item.equipamentos">
                  <p class="mb-0 bold">Equipamentos</p>
                  <div v-html="item.equipamentos"></div>
                </div>
                
                <!-- Documentos -->                
                <div class="form-group" v-if="item.documentos">
                  <p class="mb-0 bold">Documentos</p>
                  <div v-html="item.documentos"></div>
                </div>
                
                <!-- More Info -->                
                <div class="form-group" v-if="item.contato">
                  <div v-html="item.contato"></div>
                </div>
                
                      
              </v-expansion-panel-content>
                
            </v-expansion-panel>
        </v-expansion-panels>
    </v-row>
    
  </v-container>
  `
});
