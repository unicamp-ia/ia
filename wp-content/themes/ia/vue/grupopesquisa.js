Vue.component('ia-grupopesquisa', {
  props: {
    filterprograma: String,
    intranet_path: String,
  },
  data: function () {
    return {
      nome: '',
      lider: '',
      programa: this.filterprograma ? this.filterprograma : 'Todos',
      allRows: [],
      departamento: '',
      optionProgramas: [
          'Todos',
          'Artes da Cena',
          'Artes Visuais',
          'Multimeios',
          'Música'
      ],
      optionsDepartamento: [
        'Todos',
      ],
      total: 0,
      page: 1,
      perPage: 10,
    }
  },
  created() {
    axios.get(this.intranet_path + '/api/grupopesquisa/')
        .then( resp => {this.allRows = resp.data});

    axios.get(this.intranet_path + '/api/extensao/laboratorio/departamento')
        .then( resp => {
          const deptos = resp.data.map( item => item.descricao)
          this.optionsDepartamento = this.optionsDepartamento.concat(deptos);
        });
        
  },
  computed: {
    filteredRows: function () {
      const items = this.allRows.filter(item => {
        var isValid = true

        console.log(item)

        if (this.programa !== 'Todos' && item.programa.nome.toLowerCase().includes(this.programa.toLowerCase()) === false) {
          isValid = false
        }

        if (item.nome.toLowerCase().includes(this.nome.toLowerCase()) === false) {
          isValid = false
        }

        if (this.departamento !== 'Todos' && item.orgao
            && item.orgao.descricao.toLowerCase().includes(this.departamento.toLowerCase()) === false) {
          isValid = false
        }

        if (item.lider.nome.toLowerCase().includes(this.lider.toLowerCase()) === false) {
          isValid = false
        }

        if (isValid) {
          return item
        }
      });
      
      this.total = items.length
      
      return items.slice((this.page - 1)* this.perPage, this.page* this.perPage)
    },
    calculatePages: function () {
      return Math.ceil(this.total/this.perPage)
    },
  },
  
  template: `
  <v-container fluid data-app>
    <v-form>
      <strong>Filtro:</strong>
      <v-row class="row">
        <v-col cols="12" sm="3">
          <v-text-field
            v-model="nome"
            label="Nome do grupo"
          ></v-text-field>
        </v-col>
        <v-col cols="12" sm="3">
          <v-select v-model="departamento" 
          :items="optionsDepartamento" 
          label="Departamento"
          >
          </v-select>
        </v-col>
        <v-col cols="12" sm="3">
          <v-select v-model="programa" 
          :items="optionProgramas" 
          label="Programa"
          >
          </v-select>
        </v-col>
        <v-col cols="12" sm="3">
          <v-text-field
            v-model="lider"
            label="Líder"
          ></v-text-field>
        </v-col>
        
        
      </v-row>
    </v-form>
    
    <v-row justify="center">
        <v-expansion-panels accordion>
            <v-expansion-panel
            v-for='grupoPesquisa in filteredRows'
            :key="grupoPesquisa.id" 
            >
              <v-expansion-panel-header class="d-flex justify-space-between p-3">
                <div class="text-left">
                  <strong>{{grupoPesquisa.nome}}</strong>
                </div>
                <div class="text-right">
                  <v-chip small>
                      {{grupoPesquisa.programa.nome}}
                  </v-chip>
                </div>
              </v-expansion-panel-header>
              <v-expansion-panel-content class="p-0">
                <div v-html="grupoPesquisa.descricao"></div>

                <div class="form-group">
                  <a v-bind:href="grupoPesquisa.linkDgp" target="_blank">
                    <i class="fas fa-link"></i>
                    {{ grupoPesquisa.linkDgp }}
                  </a>
                </div>

                <div class="form-group">
                  <p class="mb-1 bold">Líder(es) do grupo</p>
        
                  <p class="mt-0 mb-1" v-if="grupoPesquisa.lider">
                    {{ grupoPesquisa.lider.nome }}
                    <a v-if="grupoPesquisa.lider.linkLattes" v-bind:href="grupoPesquisa.lider.linkLattes" target="_blank">
                      <span data-tooltip="Currículo Lattes" data-position="right center">
                          <img style="max-height: 16px" src="https://sobral.ufc.br/wp-content/uploads/2020/04/lattes.svg">
                      </span>
                    </a>
                  </p>
        
                  <p class="mt-0 mb-1" v-if="grupoPesquisa.viceLider">
                    {{ grupoPesquisa.viceLider.nome }}
                    <a v-if="grupoPesquisa.viceLider.linkLattes" v-bind:href="grupoPesquisa.viceLider.linkLattes" target="_blank">
                      <span data-tooltip="Currículo Lattes" data-position="right center">
                          <img style="max-height: 16px" src="https://sobral.ufc.br/wp-content/uploads/2020/04/lattes.svg">
                      </span>
                    </a>
                  </p>
                </div>

                <div class="form-group" v-if="grupoPesquisa.orgao">
                  <p class="mb-0 bold">
                    {{ grupoPesquisa.orgao.descricao }}
                  </p>
                </div>
        
                <div class="form-group" v-if="grupoPesquisa.programa">
                  <p class="mb-0 bold">Programa de Pós-graduação</p>
                  {{ grupoPesquisa.programa.nome }}
                </div>
                
               
                <div class="form-group" v-if="grupoPesquisa.outraLinha">
                  <p class="mb-0 bold">Linhas de pesquisa</p>
                  <div v-html="grupoPesquisa.outraLinha"></div>
                </div>

                <div class="form-group" v-if="grupoPesquisa.contatos">
                  <span v-html="grupoPesquisa.contatos"></span>
                </div>
              </v-expansion-panel-content>  
            </v-expansion-panel>
        </v-expansion-panels>
        <div class="text-center w-100 mt-2">
          <v-pagination v-model="page" :length="calculatePages">
          </v-pagination>
        </div>
        
    </v-row>
    
  </v-container>
  `,
});