Vue.component('ia-defesa', {
    props: {
        intranet_path: String,
        filterprograma: String,
        filtertipo: String,
    },
    data: function () {
        return {
            titulo: '',
            status: 'Todos',
            tipo: this.filtertipo ? this.filtertipo : 'Todos',
            programa: this.filterprograma ? this.filterprograma : 'Todos',
            allRows: [],
            upload_path: this.intranet_path,
            optionsTipo: [
                'Todos',
                'Processo Seletivo',
                'PRINT',
                'Seleçao Interna',
                'Aluno Especial',
                'Residentes no Exterior'
            ],
            optionProgramas: [
                'Todos',
                'Artes da Cena',
                'Artes Visuais',
                'Multimeios',
                'Música'
            ],
            optionsStatus: ['Todos','Em Andamento', 'Encerrado'],
        }
    },
    created() {
        axios.get(this.intranet_path + '/api/cpg/processo')
            .then( resp => this.allRows = resp.data);
    },
    computed: {
        filteredRows: function () {
            return this.allRows.filter(item => {
                var isValid = true

                if (item.titulo.toLowerCase().includes(this.titulo.toLowerCase()) === false) {
                    isValid = false
                }

                if (this.status !== 'Todos'
                    && item.status.toLowerCase().includes(this.status.toLowerCase()) === false) {
                    isValid = false
                }

                if (this.tipo !== 'Todos'
                    && item.tipo.toLowerCase().includes(this.tipo.toLowerCase()) === false) {
                    isValid = false
                }

                // filtro por programa e programa definido
                if (this.programa !== 'Todos' && item.programa
                    && item.programa.nome.toLowerCase().includes(this.programa.toLowerCase()) === false) {
                    isValid = false
                }

                if (this.programa !== 'Todos' && item.programa
                    && item.programa.nome.toLowerCase().includes(this.programa.toLowerCase()) === false) {
                    isValid = false
                }

                if (this.programa !== 'Todos' && !item.programa){
                    isValid = false
                }

                if (isValid) {
                    return item
                }

            });
        },
    },
    template: `
  <v-container fluid data-app>
    <v-form>
    <strong>Filtro</strong>
      <v-row class="row">
        <v-col cols="12" sm="3">
          <v-text-field
            v-model="titulo"
            label="Título"
          ></v-text-field>
        </v-col>
        <v-col cols="12" sm="3">
          <v-select v-model="tipo" 
          :items="optionsTipo" 
          label="Tipo de edital"
          >
          </v-select>
        </v-col>
        <v-col cols="12" sm="3">
          <v-select v-model="programa" 
          :items="optionProgramas" 
          label="Programa"
          >
          </v-select>
        </v-col>
        <v-col cols="12" sm="3">
          <v-select v-model="status" 
          :items="optionsStatus" 
          label="Status"
          >
          </v-select>
        </v-col>
      </v-row>
    </v-form>
    
    <v-row justify="center">
        <v-expansion-panels accordion>
            <v-expansion-panel v-if="filteredRows.length == 0">
              <v-expansion-panel-header class="d-flex justify-space-between p-3">
                Nenhum registro encontrado
              </v-expansion-panel-header>
            </v-expansion-panel>
            <v-expansion-panel
                v-for='item in filteredRows'
                :key="item.id" 
            >
              <v-expansion-panel-header class="d-flex justify-space-between p-3">
                  <div class="text-left">
                    <strong>
                        {{item.titulo}}
                    </strong>
                  </div>
                  <div class="d-flex justify-content-end">
                    <v-chip-group>
                      <v-chip small v-if="item.programa" outlined>
                          {{item.programa.nome}}
                      </v-chip>
                      <v-chip small outlined>
                          {{item.tipo}}
                      </v-chip>
                      <v-chip small outlined 
                        v-if="item.status == 'Em Andamento'"
                        class="b-green"
                      >
                        <i class="far fa-clock pr-2"></i>
                        {{item.status}}
                      </v-chip>
                      <v-chip small outlined 
                        v-if="item.status != 'Em Andamento'"
                      >
                        <i class="far fa-check pr-2"></i>
                        {{item.status}}
                      </v-chip>
                    </v-chip-group>
                  </div>
              </v-expansion-panel-header>
              <v-expansion-panel-content class="p-0">
                <div v-if="item.descricao" v-html="item.descricao"></div>
                <div class="d-flex flex-column mb-2" v-for='processoDoc in item.processoDocs' :key="processoDoc.id">
                  <a v-bind:href="upload_path + '/edital/' + processoDoc.document" target="_blank">
                    <i class="fas fa-file-pdf"></i> {{processoDoc.titulo}}
                  </a>
                  <div v-if="processoDoc.descricao" v-html="processoDoc.descricao"></div>
                </div>
               
                <div v-if="!item.processoDocs.length">
                    Nao há arquivos
                </div>        
              </v-expansion-panel-content>
                
            </v-expansion-panel>
        </v-expansion-panels>
    </v-row>
    
  </v-container>
  `
});
