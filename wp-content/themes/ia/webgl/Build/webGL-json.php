<?php
header('Content-Type: application/json');
$wevGLjson = array (
	'companyName' => 'Unicamp',
	'productName' => 'Unity - Gaia',
	'dataUrl' => 'webGL.data.unityweb',
	'wasmCodeUrl' => 'webGL.wasm.code.unityweb',
	'wasmFrameworkUrl' => 'webGL.wasm.framework.unityweb',
	'TOTAL_MEMORY' => 268435456,
	'graphicsAPI' => 
	array (
		0 => 'WebGL 2.0',
		1 => 'WebGL 1.0',
	),
	'webglContextAttributes' => 
	array (
		'preserveDrawingBuffer' => false,
	),
	'splashScreenStyle' => 'Dark',
	'backgroundColor' => '#231F20',
	'cacheControl' => 
	array (
		'default' => 'must-revalidate',
	),
);
echo json_encode($wevGLjson, JSON_PRETTY_PRINT);
?>