<?php
/*
Template Name: Pagina Tabs
*/

$lang = ia_get_lang();

Timber::render('twig/page-tabs.twig',[
    'template_name' => 'page',
    'main_menu' => getMainMenuName(get_the_ID(), $lang),
    'content' => the_post(),
]);