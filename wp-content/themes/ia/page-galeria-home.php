<?php
/*
Template Name: GaleriaHome
*/


$lang = ia_get_lang();

$tags = [
    array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/nivo-slider.css"),
    array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/pagina-especial-com-slide.css"),
];

$tagsFooter = array(
    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/jquery.nivo.slider.pack.js"),
    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/pagina-especial.js"),
    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/pagina-especial-com-slide.js")
);

$main_menu = getMainMenuName(get_the_ID(), $lang);

$avisos = get_field("avisos", get_the_ID());
$bem_vindo = get_field("bem_vindo", get_the_ID());
$gaia_virtual = get_field("gaia_virtual", get_the_ID());
$exposicoes = get_field("exposicoes", get_the_ID());

$imagem_topo = get_field("imagem_topo", get_page_parent_high_level(get_the_ID()));
$post_data = get_post($post->ID);
$content = the_post();

Timber::render('twig/page-galeria-home.twig',[
    'template_name' => 'pos-especial',
    'content' => $content,
    'main_menu' => $main_menu,
    'avisos' => $avisos,
    'bem_vindo' => $bem_vindo,
    'gaia_virtual' => $gaia_virtual,
    'exposicoes' => $exposicoes,
    'contato' => filtro_email_spam(get_field("contato", get_the_ID())),
    'imagem_topo' => $imagem_topo['url'],
    'topbar' => getNewsflash(),
]);


