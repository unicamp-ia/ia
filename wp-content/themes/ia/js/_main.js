﻿function UnityProgress(gameInstance, progress) {
	$(".webgl-content").hide();
	if (!gameInstance.progress) {    
		$(".text").html("Carregando sua experiência... " + Math.floor(100 * progress) + "%");
	}
	
	if (progress == 1){
		$(".text").html("Sua experiência foi carregada!");
		$("#loading-total").delay(1000*1).fadeOut("slow", function () {
		$("#loading-total-logo").attr("class", "col-md-12 text-center").fadeIn("slow", 
		function () {
			$(".loader").fadeOut('slow', 
			function () {
					$(".webgl-content").fadeIn('slow', 
						function () {
						//
						});
			});
		});
	});	
	}
}