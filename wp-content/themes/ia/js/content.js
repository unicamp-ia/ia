$(document).ready(function () {
    $("div#menu-lateral nav ul").parent().children("a").addClass("submenu");

    $("div#menu-lateral nav ul a.submenu").each(function(index, element) {
        $(this).html('<b>+</b> ' + $(this).html());
    });

    // oculta todos
    $("div#menu-lateral nav ul.ancestor-tree ul ul.children").each(function(index, element) {
        $(this).attr("style","display:none;");
    });

    // Exibe itens ativos
    $('#menu-lateral .current_page_item > ul').each(function(index, element) {
        $(this).attr("style","");
    });

    // exibe primeiro item
    $("div#menu-lateral nav ul a.submenu").click(function(e) {

    });

    $("div#menu-lateral li.current_page_ancestor").each(function(index, element) {
        console.log(element)
        $(this).children("ul").attr("style","");
    });

});