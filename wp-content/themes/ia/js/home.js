$(document).ready(function () {
	$('.slick').slick({
		dots: true,
		centerMode: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 2,
		variableWidth: true,
		prevArrow: '<a class="slick-prev"> <i class="fas fa-angle-left" aria-hidden="true"></i></a>',
		nextArrow: '<a class="slick-next"> <i class="fas fa-angle-right" aria-hidden="true"></i></a>',
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 769,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 426,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]

	});
	$('#slider').nivoSlider({
		pauseOnHover: true,
		pauseTime: 10000,
		randomStart: true,
		effect: 'fade',
		animSpeed: 0,
		prevText: '<i class="fas fa-angle-left"></i>',
		nextText: '<i class="fas fa-angle-right"></i>',
	});
});