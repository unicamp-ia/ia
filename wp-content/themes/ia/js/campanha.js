$('.mosaic').Mosaic({
    refitOnResize: true,
    maxRows: 3,
    showTailWhenNotEnoughItemsForEvenOneRow: false,
    maxRowHeightPolicy: 'oversize',
});
