<?php

	$menu = "Departamento de ";
	switch (get_page_parent_high_level(get_the_ID())) {
		case 700 : $menu = $menu . "Artes Cênicas";                   break;
		case 119 : $menu = $menu . "Artes Corporais";                 break;
		case 692 : $menu = $menu . "Artes Plásticas";                 break;
		case 129 : $menu = $menu . "Cinema";                          break;
		case 695 : $menu = $menu . "Multimeios, Mídia e Comunicação"; break;
		case 698 : $menu = $menu . "Música";                          break;
	}
	
	wp_nav_menu( array("menu" => $menu , "container" => "nav" , "container_id" => "menu-especifico"));

?>

<div class="contato">
    <?php echo(filtro_email_spam(get_field("contato",get_page_parent_high_level(get_the_ID())))); ?>
</div>