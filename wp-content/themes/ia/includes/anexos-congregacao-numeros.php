<?php if ($_SERVER["REQUEST_METHOD"] === "POST") {

	$pauta = $_POST["pauta"];
	$ano   = $_POST["ano"];
	$tipo  = $_POST["tipo"];
	
	$arquivos = array_diff(scandir("/usr/local/www/data/congregacao/$ano/$tipo$pauta"), array(".",".."));
	
	$letras = array();
	
	foreach ($arquivos as $arquivo) {
		$letra = $arquivo[0].$arquivo[1];
		$nome = substr($arquivo, 2, strlen($arquivo)-6);
		$numero = substr($nome,strlen($nome)-4,strlen($nome));
		$nome = str_replace($numero,"",$nome);
		$letras[$letra][$numero] = $nome;
	}
	
	echo("<textarea style=\"width:100%; height:500px;; font-family:'Courier New';\" readonly=\"readonly\">");
	
	$i = 1;
	foreach ($letras as $letra => $arquivos) {
		echo("$i)\n---\n<td>\n");
		
		$tamanho = sizeof($arquivos);
		$j = 1;
		foreach ($arquivos as $index => $nome) {
			echo("<a href=\"/congregacao/$ano/$tipo$pauta/".$letra.$nome.sprintf("%04d",$index).".jpg\">$j</a>");
			
			if ($j < $tamanho) {
				echo(" - ");
			}
			
			$j++;
		}
		
		echo("\n</td>\n\n\n");
		
		$i++;
	}
	
	echo("</textarea>");

} else { ?>
<br />
<form class="form-horizontal" action="" method="post">
    <div class="form-group">
        <label for="pauta" class="col-sm-2 control-label">Pauta:</label>
        <div class="col-sm-2">
        	<input type="text" class="form-control" id="pauta" name="pauta" />
        </div>
    </div>
    <div class="form-group">
        <label for="ano" class="col-sm-2 control-label">Ano:</label>
        <div class="col-sm-2">
			<input type="text" class="form-control" id="ano" name="ano" value="<?php $ano = getdate(); echo($ano["year"]); ?>" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tipo:</label>
        <div class="col-sm-10">
            <label class="radio-inline">
              <input type="radio" name="tipo" id="ordinaria" value="anexo" checked="checked"> Ordinária
            </label>
            <label class="radio-inline">
              <input type="radio" name="tipo" id="extraordinaria" value="extra"> Extraordinária
            </label>
            <label class="radio-inline">
              <input type="radio" name="tipo" id="complementar" value="complementar"> Complementar
            </label>
        </div>
    </div>
    <div class="form-group">
    	<hr class="col-sm-offset-2 col-sm-2" />
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
        	<button type="submit" class="btn btn-default">Gerar</button>
        </div>
    </div>
</form>
<?php } ?>