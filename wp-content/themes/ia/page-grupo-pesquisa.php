<?php
/*
Template Name: PG - Grupo de Pesquisa
*/

$lang = ia_get_lang();

$post_data = get_post($post->ID);



echo is_user_logged_in();

switch (get_page_parent_high_level(get_the_ID())){
    case 88: $field_name = 'cenicas'; break;
    case 99: $field_name = 'plasticas'; break;
    case 155: $field_name = 'multimeios'; break;
    case 109: $field_name = 'musica'; break;
}

// Busca o grupo do programa na página de grupo de pesquisa
$grupo_pesquisa = get_field($field_name, 828);

Timber::render('twig/page.twig',[
    'template_name' => 'page',
    'main_menu' => getMainMenuName(get_the_ID(), $lang),
    'contato' => filtro_email_spam(get_field("contato", get_page_parent_high_level(get_the_ID()))),
    'before_content' => $grupo_pesquisa,
    'content' => the_post(),
]);

