<?php
/*
Template Name: Especial
*/


$lang = ia_get_lang();

$tags = [
    array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/nivo-slider.css"),
    array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/pagina-especial-com-slide.css"),
];

$tagsFooter = array(
    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/jquery.nivo.slider.pack.js"),
    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/pagina-especial.js"),
    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/pagina-especial-com-slide.js")
);


$main_menu = getMainMenuName(get_the_ID(), $lang);

$avisos = get_field("avisos", get_the_ID());
$imagem_topo = get_field("imagem_topo", get_page_parent_high_level(get_the_ID()));
$post_data = get_post($post->ID);

Timber::render('twig/page-especial.twig',[
    'template_name' => 'pos-especial',
    'content' => the_post(),
    'main_menu' => $main_menu,
    'avisos' => $avisos,
    'coordenador_geral' =>  filtro_email_spam(get_field("coordenador-geral", get_the_ID())),
    'coordenador_area' => filtro_email_spam(get_field("coordenador-da-area", get_page_parent_high_level(get_the_ID()))),
    'secretaria' => filtro_email_spam(get_field("secretaria", get_the_ID())),
    'contato' => filtro_email_spam(get_field("contato", get_the_ID())),
    'imagem_topo' => $imagem_topo['url'],
    'telefones' =>  filtro_email_spam(get_field("telefones", get_the_ID())),
    'topbar' => getNewsflash(),
    'custom_html' => get_post_meta(get_the_ID(), 'html', TRUE),
]);


