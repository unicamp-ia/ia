<?php

	if ((is_page(paginas_especiais())) || pagina_especial_filha(get_the_ID())) {
		$menu = "Graduação em ";
		switch (get_page_parent_high_level(get_the_ID())) {
			case 80  : $menu = $menu . "Artes Cênicas"; break;
			case 97  : $menu = $menu . "Artes Visuais"; break;
			case 68  : $menu = $menu . "Dança";         break;
			case 105 : $menu = $menu . "Midialogia";    break;
			case 107 : $menu = $menu . "Música";        break;
		}
		echo("<small>Menu Específico do Programa</small>");
		wp_nav_menu( array("menu" => $menu , "container" => "nav" , "container_id" => "menu-especifico"));
	}
	echo("<small>Menu Comum à Graduação</small>");
	wp_nav_menu( array("menu" => "Graduação" , "container" => "nav" , "container_id" => "menu-comum"));
	wp_nav_menu( array("menu" => "Viver em Barão Geraldo" , "container" => "nav" , "container_id" => "menu-comum"));

?>

<div class="contato">
	<p><strong>Coordenador Geral</strong></p>
	<?php echo(filtro_email_spam(get_field("coordenador-geral",987))); ?>
</div>
<?php if ((in_array(get_the_ID(),paginas_graduacao_especificas())) || (pagina_graduacao_especifica_filha(get_the_ID()))) { ?>
<div class="contato">
	<p><strong>Coordenador da Área</strong></p>
	<?php echo(filtro_email_spam(get_field("coordenador-da-area",get_page_parent_high_level(get_the_ID())))); ?>
</div>
<?php } ?>
<div class="contato">
	<p><strong>Secret&aacute;ria de Gradua&ccedil;&atilde;o</strong></p>
	<?php echo(filtro_email_spam(get_field("secretaria",987))); ?>
</div>

<div class="contato">
	<p><strong>Telefones</strong></p>
	<?php echo(filtro_email_spam(get_field("telefones",987))); ?>
</div>
