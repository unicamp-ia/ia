<?php
/*
Template Name: Iframe
*/

$lang = ia_get_lang();

$main_menu = getMainMenuName(get_the_ID(), $lang);

Timber::render('twig/page-iframe.twig',[
    'template_name' => 'pos-especial',
    'content' => the_post(),
    'main_menu' => $main_menu,
]);


