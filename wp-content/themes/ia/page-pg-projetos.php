<?php
/*
Template Name: PG - Projetos
*/

$lang = ia_get_lang();

$post_data = get_post($post->ID);



echo is_user_logged_in();

switch (get_page_parent_high_level(get_the_ID())){
    case 88:
        $docentes = 'artes-da-cena';
        $ic = 'ic-t';
        $m = 'm-ac';
        $d = 'd-ac';
        break;
    case 99:
        $docentes = 'visuais';
        $ic = 'ic-av';
        $m = 'm-av';
        $d = 'd-av';
        break;
        break;
    case 155:
        $docentes = 'multimeios';
        $ic = 'ic-c';
        $m = 'm-mult';
        $d = 'd-mult';
        break;
        break;
    case 109:
        $docentes = 'musica';
        $ic = 'ic-m';
        $m = 'm-mus';
        $d = 'd-mus';
        break;

}

// Busca o grupo do programa na página de grupo de pesquisa
$ic = get_field($ic, 854);
$m = get_field($m, 854);
$d = get_field($d, 854);
$docentes = get_field($docentes, 2607);

Timber::render('twig/page-pg-projetos.twig',[
    'template_name' => 'page',
    'main_menu' => getMainMenuName(get_the_ID(), $lang),
    'contato' => filtro_email_spam(get_field("contato", get_page_parent_high_level(get_the_ID()))),
    'ic' => $ic,
    'm' => $m,
    'd' => $d,
    'docentes' => $docentes,
    'content' => the_post(),
]);

