<?php
/*
Template Name: Pós-graduação - Home
*/

$lang = ia_get_lang();

$tags = [
    array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/nivo-slider.css"),
    array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/pagina-especial-com-slide.css"),
];

$tagsFooter = array(
);


$main_menu = getMainMenuName(get_the_ID(), $lang);

$avisos = get_field("avisos", get_the_ID());
$imagem_topo = get_field("imagem_topo", get_page_parent_high_level(get_the_ID()));
$post_data = get_post($post->ID);

Timber::render('twig/cpg/page-home.twig',[
    'template_name' => 'pos-home-cpg',
    'contents' => [
        'defesas' => getNextDefesas(),
        'noticias' => getNews(3,5),
    ],
    'content' => the_post(),
    'main_menu' => $main_menu,
    'avisos' => $avisos,
    'imagem_topo' => $imagem_topo['url'],

    'topbar' => getNewsflash(),
]);
