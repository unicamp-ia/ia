<?php
/*
Template Name: Eventos
*/
$lang = ia_get_lang();

$post_data = get_post($post->ID);

Timber::render('twig/page-evento-legacy.twig',[
    'template_name' => 'page',
    'content' => the_post(),
]);

