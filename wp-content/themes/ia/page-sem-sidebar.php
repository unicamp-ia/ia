<?php
/*
Template Name: Sem Sidebar
*/
$lang = ia_get_lang();

$post_data = get_post($post->ID);

Timber::render('twig/page-nosidebar.twig',[
    'template_name' => 'page',
    'main_menu' => getMainMenuName(get_the_ID(), $lang),
    'content' => the_post(),
]);

