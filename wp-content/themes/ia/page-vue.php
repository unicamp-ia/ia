<?php
/*
Template Name: Vue
*/

$lang = ia_get_lang();

$main_menu = getMainMenuName(get_the_ID(), $lang);

Timber::render('twig/page-vue.twig',[
    'template_name' => 'vue',
    'content' => the_post(),
    'intranet_path' => WWW4_PUBLIC,
    'main_menu' => $main_menu,
    'custom_html' => get_post_meta(get_the_ID(), 'html', TRUE),
]);


