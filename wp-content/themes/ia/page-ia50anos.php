<?php
/*
Template Name: IA 50 anos
*/
$lang = ia_get_lang();

$post_data = get_post($post->ID);



Timber::render('twig/page-ia50anos.twig',[
    'template_name' => 'page',
    'main_menu' => getMainMenuName(get_the_ID(), $lang),
    'content' => the_post(),
]);

