<?php

global $wpdbIntranet;
$wpdbIntranet = new WPDB( 'sintranet', 'er6zJ3tTWtS23eyU', 'sintranet', 'db');

add_action("admin_head", function  () {
    if (!current_user_can("manage_options")) {
        echo '<style type="text/css">
			   td.mce_h1,
			   td.mce_h4,
			   td.mce_h5,
			   td.mce_h6,
			   
			   
			   li#wp-admin-bar-comments,
			   li#wp-admin-bar-new-media,
			   li#menu-media,
			   li#menu-comments,
			   li#menu-tools { display:none; }
			 </style>
			 ';
    }
});


function my_admin_footer_function() {
    global $pagenow;
    if (($pagenow == "post.php") && (!current_user_can("manage_options"))) {
        echo '
			<script src="'.get_bloginfo("template_directory").'/js/jquery-1.10.1.min.js"></script>
			<script type="text/javascript">
				$(document).ready(function () {
					//$("select#page_template").attr("disabled","disabled");
					$("p:contains(\'Modelo\')").hide();
					$("select#page_template").hide();
				});
			 </script>';
    }
}
//add_action('admin_footer', 'my_admin_footer_function');

function htaccess_notice () {
    global $pagenow;
    if ( $pagenow == "options-permalink.php" ) {
        echo '<div class="updated">
             <p><strong>ATENÇÃO!</strong> O arquivo <strong>.htaccess</strong> foi modificado manualmente. Qualquer alteração nesta página pode sobrescrever as configurações.</p>
         </div>';
    }
}
add_action("admin_notices", "htaccess_notice");

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).' &#91;...&#93;';
    }
    else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}

if ( function_exists("add_theme_support") ) {
    add_theme_support("post-thumbnails");
}

if ( function_exists("add_image_size") ) {
    add_image_size("home-first", 306, 204, true );
    add_image_size("home", 63, 42, true );
}

function the_breadcrumb () {
    $output = '';
    $lista_negra = array(
        112,  // O Instituto
        168,  // Pós-graduação
        216,  // Resultados Pós-graduação Artes Visuais
        1133, // Resultados Pós-graduação Multimeios
        1181, // Resultados Pós-graduação Artes da Cena
        1239, // Resultados Pós-graduação Música
        366,  // Congregação
        987,  // Graduação
        201,  // Viver em Baraão Geraldo
        280,  // Dissertações e Teses
        231,  // Vida Acadêmica
        825,  // Pesquisa
        833,  // Projetos em Andamento
        586,  // Calendário Bolsas
        286,  // Seleção Estudante Especial
        180   // CAPES e CNpQ
    );

    $separator = "&raquo;";
    global $post;

    echo "<ul id=\"breadcrumbs\" class=\"list-inline\">";

    echo "<li><a href=\"" . get_option("home") . "\">Home</a></li><li class=\"separator\">$separator</li>";

    if (is_category()) {
        global $wp_query;
        $category = $wp_query->get_queried_object();
        echo "<li>" . $category->name . "</li>";
    }
    elseif (is_single()) {
        $categories = get_the_category();

        foreach ($categories as $index => $category) {
            if ($category->cat_ID != 66) { // Home
                $aux[] = array("nome" => $category->name , "link" => get_category_link($category->cat_ID));
            }
        }

        $categories = $aux;

        $tamanho = sizeof($categories);
        if ($tamanho > 0) {
            echo("<li>");
            foreach ($categories as $index => $category) {
                echo("<a href=\"" . $category["link"] . "\" title=\"" . $category["nome"] . "\">" . $category["nome"]  . "</a>");
                if ($index < $tamanho-1) { echo(", "); }
            }
            echo("</li><li class=\"separator\">$separator</li><li>".get_the_title()."</li>");
        }
    }
    elseif (is_page()) {
        if($post->post_parent){
            $anc = array_reverse(get_post_ancestors($post->ID));
            $title = get_the_title();
            foreach ( $anc as $ancestor ) {
                if (in_array($ancestor,$lista_negra)) {
                    $output = $output . "<li>".get_the_title($ancestor)."</li>";
                }
                else {
                    $output = $output . "<li><a href=\"".get_permalink($ancestor)."\" title=\"".get_the_title($ancestor)."\">".get_the_title($ancestor)."</a></li>";
                }
                $output = $output . "<li class=\"separator\">$separator</li>";
            }
            echo $output;
            echo "<li><strong title=\"$title\">$title</strong></li>";
        } else {
            echo "<li><strong>".get_the_title()."</strong></li>";
        }
    }
    echo "</ul>";
}

function the_pagination () {

    global $wp_query;

    $big = 999999999; // need an unlikely integer

    $paginas = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type' => 'array'
    ) );

    if(isset($paginas)) {
        echo("<nav><ul class=\"pagination\">");
        foreach($paginas as $pagina) {
            if (strpos($pagina, "current") === false) {
                echo("<li>$pagina</li>");
            } else {
                echo("<li class=\"active\">$pagina</li>");
            }
        }
        echo("</ul></nav>");
    }
}

function get_page_parent ($id) {
    $page = get_page($id);

    return $page->post_parent;
}

function get_page_parent_high_level($id) {
    $page = get_post($id);

    if(!$page) return null;

    if ($page->post_parent == 0) {
        return $page->ID;
    }
    else {
        return get_page_parent_high_level($page->post_parent);
    }
}

function paginas_especiais () {
    $tops = array(
        97,  // Graduação em Artes Visuais
        99,  // Pós-graduação em Artes Visuais
        692, // Departamento de Artes Plásticas

        105, // Graduação em Midialogia
        155, // Pós-graduação em Multimeios
        //99,  // Pós-graduação em Artes Visuais
        129, // Departamento de Cinema
        695, // Departamento de Multimeios, Mídia e Comunicação

        68,  // Graduação em Dança
        88,  // Pós-graduação em Artes da Cena
        119, // Departamento de Artes Corporais

        107, // Graduação em Música
        109, // Pós-graduação em Música
        698, // Departamento de Música

        80,  // Graduação em Artes Cênicas
        //92,  // Pós-graduação em Artes da Cena
        700  // Departamento de Artes Cênicas
    );

    return $tops;
}


function pagina_especial_filha ($id) {

    $objeto = get_post($id);
    if(!$objeto) return false;

    if ($objeto->post_parent != 0) {
        if (in_array($objeto->post_parent,paginas_especiais())) {
            return true;
        }
        else {
            return pagina_especial_filha($objeto->post_parent);
        }
    }

    return false;

}

function pagina_graduacao_geral ($id) {
    $objeto = get_post($id);
    if(!$objeto) return false;

    if ($objeto->post_parent != 0) {
        if ($objeto->post_parent == 987) {
            return true;
        }
        else {
            return pagina_graduacao_geral($objeto->post_parent);
        }
    }

    return false;
}

function paginas_graduacao_especificas () {

    $paginas = array(
        80,  // Graduação em Artes Cênicas
        97,  // Graduação em Artes Visuais
        68,  // Graduação em Dança
        105, // Graduação em Midialogia
        107  // Graduação em Música
    );

    return $paginas;

}

function pagina_graduacao_especifica_filha ($id) {

    $objeto = get_post($id);
    if(!$objeto) return false;
    if ($objeto->post_parent != 0) {
        if (in_array($objeto->post_parent,paginas_graduacao_especificas())) {
            return true;
        }
        else {
            return pagina_graduacao_especifica_filha($objeto->post_parent);
        }
    }

    return false;
}

function is_graduacao($id) {
    if ((in_array($id,paginas_graduacao_especificas())) || (pagina_graduacao_especifica_filha($id)) || (pagina_graduacao_geral($id))) {
        return true;
    }
    return false;
}

function pagina_pos_graduacao_geral ($id) {
    $objeto = get_post($id);
    if(!$objeto) return false;
    if ($objeto->post_parent != 0) {
        if ($objeto->post_parent == 168) {
            return true;
        }
        else {
            return pagina_pos_graduacao_geral($objeto->post_parent);
        }
    }

    return false;
}

function paginas_pos_graduacao_especificas () {

    $paginas = array(
        99,  // Pós-graduação em Artes Visuais
        155, // Pós-graduação em Multimeios
        88,  // Pós-graduação em Artes da Cena
        109  // Pós-graduação em Música
    );

    return $paginas;

}

function pagina_pos_graduacao_especifica_filha ($id) {

    $objeto = get_post($id);
    if(!$objeto) return false;

    if ($objeto->post_parent != 0) {
        if (in_array($objeto->post_parent,paginas_pos_graduacao_especificas())) {
            return true;
        }
        else {
            return pagina_pos_graduacao_especifica_filha($objeto->post_parent);
        }
    }

    return false;
}

function is_pos_graduacao ($id) {
    if ((in_array($id,paginas_pos_graduacao_especificas())) || (pagina_pos_graduacao_especifica_filha($id)) || (pagina_pos_graduacao_geral($id))) {
        return true;
    }

    return false;
}

function paginas_departamento () {

    $paginas = array(
        700,  // Departamento de Artes Cênicas
        119,  // Departamento de Artes Corporais
        692,  // Departamento de Artes Plásticas
        129,  // Departamento de Cinema
        695,  // Departamento de Multimeios, Mídia e Comunicação
        698,  // Departamento de Música

    );

    return $paginas;

}

function menu_lateral ($str,$id = "") {

    if ((in_array($id,paginas_pos_graduacao_especificas())) || (pagina_pos_graduacao_especifica_filha($id))) {
        return "pos-graduacao";
    }

    if ((in_array($id,paginas_graduacao_especificas())) || (pagina_graduacao_especifica_filha($id))) {
        return "graduacao";
    }

    if ((in_array($id,paginas_departamento())) || (in_array(get_page_parent($id),paginas_departamento()))) {
        return "departamento";
    }
}

add_filter( "the_content", "filtro_email_spam", 20 );
function filtro_email_spam( $content ) {

    $pattern="/(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";
    preg_match_all($pattern, $content, $matches);

    $matches = array_unique($matches[0]);

    foreach ($matches as $email) {
        $email_filtrado = str_replace("@", " (arroba) ", $email);
        $email_filtrado = str_replace(".", " (ponto) ", $email_filtrado);

        $content = str_replace($email,$email_filtrado,$content);
    }

    return $content;
}

add_theme_support("menus");

function paginas_area () {

    foreach (unserialize(AREAS) as $area => $codigo) {
        $filhas[$area] = get_children(array("post_type" => "page" , "post_parent" => $codigo),ARRAY_A);
    }

    foreach ($filhas as $area => $paginas) {
        foreach ($paginas as $pagina) {
            //$aux[$area][] = $pagina["ID"];
            $aux[] = $pagina["ID"];
        }
    }

    return $aux;
}

function paginas_principais_curso () {
    $filhas = get_children(array("post_type" => "page" , "post_parent" => 63),ARRAY_A);

    foreach ($filhas as $key => $value) {
        $paginas[] = $key;
    }

    return $paginas;
}

function qual_curso($id) {
    $objeto = get_post($id);

    if ($objeto->post_parent == 63) {
        return $objeto->ID;
    }
    else {
        return qual_curso($objeto->post_parent);
    }
}

function get_area_name ($id) {
    switch (get_page_parent_high_level($id)) {
        case 97  : return "artes-visuais";
        case 99  : return "artes-visuais";
        case 692 : return "artes-visuais";

        case 105 : return "comunicacao";
        case 155 : return "comunicacao";
        case 129 : return "comunicacao";
        case 695 : return "comunicacao";

        case 68  : return "danca";
        case 88  : return "danca";
        case 119 : return "danca";

        case 107 : return "musica";
        case 109 : return "musica";
        case 698 : return "musica";

        case 80  : return "teatro";
        case 700 : return "teatro";
    }

}

/**
 * Define o menu principal
 * @param $id
 * @param $lang
 * @return string
 */
function getMainMenuName($id, $lang) {
    switch (get_page_parent_high_level($id)) {
        case 80:
        case 97:
        case 105:
        case 68:
        case 107:
        case 987:
            $mainMenuName = 'Graduação';
            break;
        case 88:
        case 99:
        case 155:
        case 168:
        case 109:
        case 700:
            $mainMenuName = 'Pós-graduação';
            break;
        case 18489:
            $mainMenuName = 'pesquisa-extensao';
            break;
        case 21543:
            // 21543 na web e 17720 na webnew
            $mainMenuName = 'biblioteca';
            break;
        case 22295:
            // 17812 na webnew 22295 na web
            $mainMenuName = 'galeria';
            break;
        case 21119:
            $mainMenuName = 'ia50anos';
            break;
        default:
            $mainMenuName = 'Principal';
            break;
    }

    if($lang != 'pt'){
        $mainMenuName = $mainMenuName.'-'.$lang;
    }

    return $mainMenuName;
}

function getParentName(){
    $parent = get_page_parent_high_level(get_the_ID());
    return get_the_title($parent);
}

/**
 * Procura topo na pagina pai
 * @return |null
 */
function getTopo() {
    $field = get_field("imagem_topo", get_page_parent_high_level(get_the_ID()));
    if(isset($field['url'])){
        return $field['url'];
    }
    return null;
}


function get_the_slug($tipo = "this") {
    if (is_home()) {
        return "home";
    }
    else {
        switch ($tipo) {
            case "this"	: {
                $post_data = get_post($post->ID);
                return $post_data->post_name;
                break;
            }

            case "curso" : {
                $post_data = get_post(qual_curso($post->ID));
                return $post_data->post_name;
                break;
            }
        }
    }
}

function body_slug () {
    if (is_home()) {
        return "home";
    }
    elseif ((is_page(paginas_especiais())) || pagina_especial_filha(get_the_ID())) {
        return "pagina-especial-com-slide " . get_area_name(get_the_ID());
    }
    elseif (pagina_pos_graduacao_geral(get_the_ID())) {
        return "pagina-especial-sem-slide";
    }
}

function get_the_css () {
    if (is_home()) { $css = "home"; }

    elseif (is_category()) { $css = "categoria"; }

    elseif (is_single()) { $css = "post"; }

    elseif ((in_array(get_the_ID(),paginas_especiais())) || pagina_especial_filha(get_the_ID())) { $css = "pagina-especial-com-slide"; }

    elseif ((pagina_pos_graduacao_geral(get_the_ID()) || (pagina_graduacao_geral(get_the_ID()))) || (get_the_ID() == 168)) { $css = "pagina-especial-sem-slide"; }

    elseif (is_page()) { $css = "page"; }

    return get_bloginfo("template_url") . "/css/" . $css . ".css";
}

function get_the_script () {
    if (is_home()) { $script = "home"; }

    elseif ((is_page(paginas_especiais())) || pagina_especial_filha(get_the_ID())) { $script = "pagina-especial-com-slide"; }

    elseif ((pagina_pos_graduacao_geral(get_the_ID()) || (pagina_graduacao_geral(get_the_ID()))) || (get_the_ID() == 168)) { $script = "pagina-especial-sem-slide"; }

    elseif (is_page()) { $script = "page"; }

    return get_bloginfo("template_url") . "/js/" . $script . ".js";
}



function menu_func($atts) {
    extract( shortcode_atts( array(
        "nome" => "nome"
    ), $atts ) );

    return wp_nav_menu( array("menu" => $nome , "echo" => false , "container" => false));
}
add_shortcode("menu","menu_func");



function php_shortcode( $atts, $content = null ) {
    //eval(htmlentities(html_entity_decode($content)));
    //echo(html_entity_decode($content,ENT_QUOTES));
    //echo(html_entity_decode($content, ENT_QUOTES, "Windows-1252"));
    //echo(html_entity_decode("&amp;"));

    $search = array("&#8217;","&#8220;","&#8221;","&gt;","&lt;","&#8211;");
    $replace = array("'","\"","\"",">","<","--");

    return eval(html_entity_decode(str_replace($search, $replace, $content)));
    //echo(html_entity_decode(str_replace($search, $replace, $content)));

}
add_shortcode("php","php_shortcode");

/*
function get_curso_nivel ($id) {
	$post_data = get_post($post->ID);
}*/

function stop_echo() {
    ob_start();
}

function print_echo() {
    $saida = ob_get_contents();
    ob_end_clean();
    echo(utf8_encode($saida));
}

function get_permissoes ($grupo,$post_type) {
    $grupos["ceprod"]["post"] =  array();
    //$grupos["ceprod"]["page"] =  array(828,875,845,848);
    $grupos["ceprod"]["page"] =  array(828,875,845,848,4675,4680,4682,4684,4688);

    return $grupos[$grupo][$post_type];
}


add_filter('pre_get_posts', function ($query) {

    if (($query->is_admin) && (!current_user_can("manage_options"))) {
        global $post_type;
        $current_user = wp_get_current_user();
        $grupo = $current_user->roles[0];
        $query->set("post__in",get_permissoes($grupo,$post_type));
    }

    return $query;
});

add_action( 'admin_head-post.php', function () {

    if (!current_user_can("manage_options")) {

        $current_user = wp_get_current_user();
        $grupo = $current_user->roles[0];

        global $post;

        $permissoes = get_permissoes($grupo,$post->post_type);

        if (!empty($permissoes)) {
            if (!in_array($post->ID,$permissoes)) {

                switch ($post->post_type) {
                    case "post" : { $titulo = "post"; $mensagem = "este post"; break; }
                    case "page" : { $titulo = "página"; $mensagem = "esta página"; break; }
                }

                echo('<style>div#wpbody-content , div#wp-fullscreen-body {display:none;}  </style>');
                echo('<script>jQuery(document).ready(function () {
					jQuery("div#wp-fullscreen-body").html("");
					jQuery("div#wpbody-content").html("<div class=\"wrap\"><h2>Editar ' . $titulo . '</h2> <div class=\"error\"><p>Você não está autorizado a editar ' . $mensagem . '.</p></div></div>");
					jQuery("div#wpbody-content").show();
				});</script>');

            }
        }
    }
});

function menu_externo ($menus) {

    foreach ($menus as $menu) {
        if (strpos($menu," em ") !== false) {
            $container_id = "menu-especifico";
        }
        else {
            $container_id = "menu-comum";
        }
        wp_nav_menu( array("menu" => $menu , "container" => "nav" , "container_id" => $container_id));
    }
}

/**
 * class Bootstrap_Walker_Nav_Menu()
 *
 * Extending Walker_Nav_Menu to modify class assigned to submenu ul element
 *
 * @author Rachel Baker
 **/
class Bootstrapwp_Walker_Nav_Menu extends Walker_Nav_Menu {
    function __construct() {}
    function start_lvl(&$output, $depth = 0, $args = array()) {

        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent&lt;ul class=\"dropdown-menu\"&gt;\n";
    }
}


function slider($imagens) {
    $result = '';
    $path = get_bloginfo("template_url") . "/img/slide/";
    $result .= "<div id=\"slider-wrapper\" class=\"hidden-sm hidden-xs\"><div id=\"slider\">";

    foreach ($imagens as $imagem) {
        $result .= "<a href=\"javascript:void(0);\"><img src=\"" . $path . $imagem["src"] . "\" title=\"" . $imagem["title"] . "\" alt=\"" . $imagem["alt"] . "\" /></a>";
    }

    $result .= "</div></div>";
    return $result;
}

function get_WWW4_PUBLIC(){
    return WWW4_PUBLIC;
}

/**
 * Obtem conteudos que vai para a home
 * @param int $maxEvento
 * @param int $maxNoticias
 * @return array
 * @throws Exception
 */
function getContent($maxEvento = 10, $maxNoticias = 9) {
    global $wpdbIntranet;
    $today = new DateTime();
    $today->setTime(0,0,0);
    $result = ['noticias' => []];
    $lang = ia_get_lang();

    $query = 'SELECT * from sintranet.com_content where status = 1 ' .
        " AND language = '".$lang."' " .
        " AND slideshow_inicio <= '".$today->format('Y-m-d 00:00:00')."'" .
        " AND slideshow_termino >'".$today->format('Y-m-d 00:00:00') ."' order by id DESC LIMIT 5 ";

    $slideshows = $wpdbIntranet->get_results($query);

    foreach ($slideshows as $item){
        $result['slideshow'][$item->id] = [
            'title' => $item->title,
            'redirect' => $item->redirect ? $item->redirect : null,
            'hasContent' => $item->body ? true : false,
            'credito' => $item->slideshow_credito,
            'image' => WWW4_PUBLIC .'/media/cache/resolve/homeSlideshow/slideshow/'  . $item->slideshow_name,
            'id' => $item->id,
        ];
    }

    $eventos_query = 'SELECT * from sintranet.com_content where type = 1'.
        " AND language = '".$lang."'" .
        " AND status = 1 AND home = 1 " .
        " AND evento_fim >= '".$today->format('Y-m-d 00:00:00')."' " .
        //" AND evento_inicio <= '".$today->format('Y-m-d 00:00:00')."'" .
        ' order by evento_inicio ASC LIMIT 20';

    $eventos = $wpdbIntranet->get_results($eventos_query);

    $i = 0;
    foreach ($eventos as $item){
        //if(isset($result['slideshow'][$item->id])) continue;
        if($i >= $maxEvento) continue;

        $inicio = new DateTime($item->evento_inicio);
        $fim = new DateTime($item->evento_fim);

        $result['eventos'][$item->id] = [
            'title' => $item->title,
            'chamada' => $item->evento_chamada,
            'day' => $inicio->format('d'),
            'month' => convertMonth($inicio),
            'local' => $item->evento_local,
            'image' => $item->evento_name ? WWW4_PUBLIC .'/media/cache/resolve/homeEvento/evento/'  . $item->evento_name : null,
            'id' => $item->id,
        ];
        $i++;
    }

    // NOTICIAS
    $noticias = $wpdbIntranet->get_results('SELECT * from sintranet.com_content where type = 2 AND home = 1 AND status = 1' .
        " AND language = '".$lang."'" .
        " ORDER BY id DESC LIMIT 20");

    $i = 0;
    if(count($noticias) < 3) $maxNoticias = 0;
    if(count($noticias) >= 6) $maxNoticias = 6;
    if(count($noticias) >= 9) $maxNoticias = 9;

    foreach ($noticias as $item){

        $row = [
            'title' => $item->title,
            'id' => $item->id,
            'chamada' => $item->evento_chamada,
            'image' => $item->evento_name ? WWW4_PUBLIC .'/media/cache/resolve/homeNoticia/evento/'  . $item->evento_name : null,
            'slideshow' => $item->slideshow_name ? WWW4_PUBLIC .'/media/cache/resolve/homeNoticia/slideshow/'  . $item->slideshow_name : null,
        ];


        if(count($result['noticias']) < $maxNoticias){
            $result['noticias'][] = $row;
        }

        $i++;
    }

    // Extensao
    $extensaos = $wpdbIntranet->get_results("SELECT * from sintranet.com_curso_extensao where destacado = 1 ORDER BY id DESC LIMIT 4");


    foreach ($extensaos as $item){
        $result['extensao'][] = [
            'id' => $item->id,
            'nome' => $item->nome,
            'tipo' => $item->tipo,
            'image' => $item->slideshow_name ? WWW4_PUBLIC .'/media/cache/resolve/homeExtensao/slideshow/'  . $item->slideshow_name : null,
        ];
    }

    // noticias RIGHT
    $result['noticias2'][] = ['rows' => getNews(15,2),'label' => 'Graduação','name' => 'graduacao'];
    $result['noticias2'][] = ['rows' => getNews(3,2),'label' => 'Pós-Graduação','name' => 'pos-graduacao'];
    $result['noticias2'][] = ['rows' => getNews(57,1),'label' => 'Editais','name' => 'editais'];

    // PROX DEFESAS
    $result['defesas'] = getNextDefesas();
    $result['defesas_anteriores'] = getDefesasAnteriores();

    return $result;
}


/**
 * Get Newsflash
 * @return array|null
 */
function getNewsflash(){
    global $wpdbIntranet;
    date_default_timezone_set("America/Sao_Paulo");

    $start = new DateTime();
    $end = new DateTime();

    $result = null;

    $query = 'SELECT * from sintranet.com_content where status = 1 ' .
        " AND type = 4 " .
        " AND news_flash_inicio <= '".$start->format('Y-m-d H:i:00')."'" .
        " AND news_flash_termino >='".$end->format('Y-m-d H:i:00') .
        "' order by ID DESC LIMIT 1 ";

    $newsflash = $wpdbIntranet->get_results($query);

    foreach ($newsflash as $item){
        if($item->redirect) {
            $link = $item->redirect;
        } else {
            $link = get_site_url() . '/content/' . $item->id;
        }

        $result = ["title" => $item->title,'link' => $link];
    }
    return $result;
}

function convertMonth(DateTime $date){
    switch ($date->format('M')){
        case 'Feb': return 'Fev'; break;
        case 'Apr': return 'Abr'; break;
        case 'May': return 'Mai'; break;
        case 'Aug': return 'Ago'; break;
        case 'Sep': return 'Set'; break;
        case 'Dec': return 'Dez'; break;
        case 'Oct': return 'Out'; break;
        default: return $date->format('M');
    }
}



/**
 * @param int $max
 * @return array
 * @throws Exception
 */
function getNextDefesas($max = 7) {
    $result = array();
    $today = new DateTime();
    $counter = 0;
    global $wpdbIntranet;

    // PROX DEFESAS
    $defesas = $wpdbIntranet->get_results("select a.id, a.data_prevista_defesa, a.orientador, a.local_exame, b.nome, a.titulo_tese, a.cod_nivel_curso
        from sintranet.cpg_orientacao a
        join sintranet.user_user b on a.aluno_id = b.id
        where a.data_prevista_defesa >= '". $today->format('Y-m-d 00:00:00'). "' " .
        ' and a.status = 2 ORDER BY a.data_prevista_defesa ');

    foreach ($defesas as $defesa){
        if(strlen($defesa->titulo_tese) > 100){
            $title = substr(wp_strip_all_tags($defesa->titulo_tese),0,100).'...';
        } else {
            $title = $defesa->titulo_tese;
        }

        $date = new DateTime($defesa->data_prevista_defesa);
        $result[$date->format('Y-m-d')][] = [
            'link' => 'defesas/' . $defesa->id,
            'data' => $defesa->data_prevista_defesa,
            'titulo' => $title,
            'orientador' => $defesa->orientador,
            'cod_nivel_curso' => $defesa->cod_nivel_curso == 'D' ? 'Doutorado' : 'Mestrado',
            'local' => $defesa->local_exame,
            'nome' => substr($defesa->nome,0,200)
        ];
    }

    foreach ($result as $key => $row){
        if($counter >= $max){
            unset($result[$key]);
        } else {
            $counter = $counter + count($result[$key]);
        }

    }

    $result2 = array();
    foreach ($result as $row){
        foreach ($row as $item){
            $result2[] = $item;
        }
    }

    return $result2;
}

/**
 * @param int $max
 * @return array
 * @throws Exception
 */
function getDefesasAnteriores($max = 7) {
    $result = array();
    $today = new DateTime();
    $counter = 0;
    global $wpdbIntranet;

    // PROX DEFESAS
    $defesas = $wpdbIntranet->get_results("select a.id, a.data_prevista_defesa, a.orientador, a.local_exame, b.nome, a.titulo_tese, a.cod_nivel_curso
        from sintranet.cpg_orientacao a
        join sintranet.user_user b on a.aluno_id = b.id
        where  a.link_gravacao != '' " .
        ' and a.status = 2 ORDER BY a.data_prevista_defesa DESC');

    foreach ($defesas as $defesa){
        if(strlen($defesa->titulo_tese) > 100){
            $title = substr(wp_strip_all_tags($defesa->titulo_tese),0,100).'...';
        } else {
            $title = $defesa->titulo_tese;
        }

        $date = new DateTime($defesa->data_prevista_defesa);
        $result[$date->format('Y-m-d')][] = [
            'link' => 'defesas/' . $defesa->id,
            'data' => $defesa->data_prevista_defesa,
            'titulo' => $title,
            'orientador' => $defesa->orientador,
            'cod_nivel_curso' => $defesa->cod_nivel_curso == 'D' ? 'Doutorado' : 'Mestrado',
            'local' => $defesa->local_exame,
            'nome' => substr($defesa->nome,0,200)
        ];
    }

    foreach ($result as $key => $row){
        if($counter >= $max){
            unset($result[$key]);
        } else {
            $counter = $counter + count($result[$key]);
        }

    }

    $result2 = array();
    foreach ($result as $row){
        foreach ($row as $item){
            $result2[] = $item;
        }
    }

    return $result2;
}



/**
 * Obtem posts de categorias
 * @param $cat
 * @param int $limit
 */
function getNews($cat, $limit = 2){

    $the_query = new WP_Query(array("cat" => $cat, "posts_per_page" => $limit));

    $result = null;

    while($the_query->have_posts()) {
        $the_query->the_post();
        global $post;
        $result[] = [
            'title' => substr($post->post_title,0,80).'...',
            'link' => get_permalink($post->ID),
        ];
    };


    wp_reset_postdata();

    return $result;
}


/**
 * Obtem post de uma lingua
 * @param $postId
 * @return false|string
 */
function ia_get_post($postId)
{
    $lang = icl_get_current_language();

    $lang_post_id =  icl_object_id($postId, 'any', true,$lang);

    return get_post($lang_post_id);

    $url = "";
    if($lang_post_id != 0) {
        $url = get_permalink($lang_post_id);
    }else {
        // No page found, it's most likely the homepage
        global $sitepress;
        $url = $sitepress->language_url($lang);
    }

    return $url;
}

/**
 * @return string
 */
function ia_get_lang(){
    return icl_get_current_language();
}

function ia_get_post_url($postId)
{
    $lang = icl_get_current_language();

    $lang_post_id =  icl_object_id($postId, 'any', true,$lang);

    $url = "";
    if($lang_post_id != 0) {
        $url = get_permalink($lang_post_id);
    }else {
        $url = get_permalink($postId);
    }

    echo $url;
    return $url;
}

// Add not-home to body class
add_filter('body_class','add_not_home_body_class');
function add_not_home_body_class($classes) {
    global $wp;


    if(!is_front_page()) $classes['home'] = 'not-home';

    if(count($wp->query_vars) > 0) $classes['home'] = 'not-home';
    else $classes['home'] = 'home';

    if($wp->request == 'en') $classes['home'] = 'home';

    return $classes;
}

/**
 * recupera e exibe na página de rosto da Gaia os três
 * posts da categoria página galeria
 */
function ia_gaia_get_posts() {
    // SlideShow
    $args = array('category_name' => 'slideshow-galeria','posts_per_page' => 3);
    $args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    $wp_query = new WP_Query( $args );
    $slideshow = array();
    if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
        $slideshow[] = array("link" => get_the_post_thumbnail_url());
    endwhile; endif;
    wp_reset_postdata();
    return $slideshow;
}

/**
 * recupera e exibe na página de rosto da Biblioteca os quatro
 * posts mais recentes da categoria últimas aquisições
 */
function ia_biblio_ultimasaquisicoes($num_aquisicoes) {
    $args = array('category_name' => 'ultimas-aquisicoes', 'posts_per_page' => $num_aquisicoes);
    query_posts($args);
    $ultimasaquisicoes = array();
    if ( have_posts() ) : while ( have_posts() ) : the_post();
        $ultimasaquisicoes[] = array("link"       => get_the_post_thumbnail_url(),
            "titulo"     => get_the_title(),
            "numchamada" => get_field("numero-de-chamada"));
    endwhile; endif;
    wp_reset_postdata();
    return $ultimasaquisicoes;
}

/**
 * recupera e exibe na página de últimas aquisições da
 * Biblioteca todos os posts cat últimas aquisições
 */
function ia_biblio_getaquisicoes() {
    global $wp_query;
    $todasaquisicoes = array();
    $args = array('category_name' => 'ultimas-aquisicoes','posts_per_page' => 12);
    $args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    $custom_query = new WP_Query( $args );
    $temp_query = $wp_query;
    $wp_query = NULL; $wp_query = $custom_query;
    if ( $custom_query->have_posts() ) : while ( $custom_query->have_posts() ) : $custom_query->the_post();
        $todasaquisicoes[] = array("link"       => get_the_post_thumbnail_url(),
            "titulo"     => get_the_title(),
            "numchamada" => get_field("numero-de-chamada"));
    endwhile; endif;
    wp_reset_postdata();
    the_pagination();
    $wp_query = NULL;
    $wp_query = $temp_query;
    return $todasaquisicoes;
}


/**
 * Obtem menu a partir da pagina
 * @return bool|string
 */
function ia_getmenu() {
    // Bail if this is not a page.
    if ( ! is_page() ) {
        return false;
    }

    // Get the current post.
    $post = get_post();

    /**
     * Get array of post ancestor IDs.
     * Note: The direct parent is returned as the first value in the array.
     * The highest level ancestor is returned as the last value in the array.
     * See https://codex.wordpress.org/Function_Reference/get_post_ancestors
     */
    $ancestors = get_post_ancestors( $post->ID );

    // If there are ancestors, get the top level parent.
    // Otherwise use the current post's ID.
    $parent = ( ! empty( $ancestors ) ) ? array_pop( $ancestors ) : $post->ID;

    // Get all pages that are a child of $parent.
    $pages = get_pages( [
        'child_of' => $parent,
    ] );

    // Bail if there are no results.
    if ( ! $pages ) {
        return false;
    }

    // Store array of page IDs to include latere on.
    $page_ids = array();
    foreach ( $pages as $page ) {
        $page_ids[] = $page->ID;
    }

    // Add parent page to beginning of $page_ids array.
    array_unshift( $page_ids, $parent );

    // Get the output and return results if they exist.
    $output = wp_list_pages( [
        'include'  => $page_ids,
        'title_li' => false,
        'echo'     => false,
    ] );

    if ( ! $output ) {
        return false;
    } else {
        return '<nav><ul class="page-menu ancestor-tree">' . PHP_EOL .
            $output . PHP_EOL .
            '</ul></nav>' . PHP_EOL;
    }
}

/**
 * Pagina de filtro do conteudo
 */
function ia_extensao_page(){
    global $wpdbIntranet;
    $query = "SELECT * from sintranet.com_curso_extensao order by id DESC";

    foreach ($wpdbIntranet->get_results($query) as $item){
        $rows[$item->tipo]['tipo'] = $item->tipo;
        $rows[$item->tipo]['rows'][$item->id] = ia_bindExtensao($item);
    }

    return $rows;
}



function ia_showExtensao($id){
    global $wpdbIntranet;
    $result = array();
    $content = $wpdbIntranet->get_results('SELECT a.*,b.nome as responsavel from sintranet.com_curso_extensao a join sintranet.user_user b on b.id = a.responsavel_id ' .
        ' where a.id = ' . $id);

    foreach ($content as $item){
        $result = ia_bindExtensao($item);
    }

    return $result;
}

/**
 * @param $row
 * @return array
 */
function ia_bindExtensao($item){
    $image = 'https://www.iar.unicamp.br/wp-content/themes/ia/img/logo-normal.png';
    if($item->slideshow_name) $image = WWW4_PUBLIC .'/media/cache/resolve/contentShow/slideshow/'  . $item->slideshow_name;

    $ini_inscricao = new DateTime($item->ini_inscricao);
    $fim_inscricao = new DateTime($item->fim_inscricao);
    $ini_oferecimento = new DateTime($item->ini_oferecimento);
    $fim_oferecimento = new DateTime($item->fim_oferecimento);

    return [
        'nome' => $item->nome,
        'sigla' => $item->sigla,
        'id' => $item->id,
        'tipo' => $item->tipo,
        'imagem' => $image,
        'ini_inscricao' => $item->ini_inscricao ? new DateTime($item->ini_inscricao) : null,
        'data_ini_inscricao' => $ini_inscricao->format('d/m/Y'),
        'fim_inscricao' => $item->fim_inscricao ? new DateTime($item->fim_inscricao) : null,
        'data_fim_inscricao' => $fim_inscricao->format('d/m/Y'),
        'ini_oferecimento' => $item->ini_oferecimento ? new DateTime($item->ini_oferecimento) : null,
        'data_ini_oferecimento' => $ini_oferecimento->format('d/m/Y'),
        'fim_oferecimento' => $item->fim_oferecimento ? new DateTime($item->fim_oferecimento) : null,
        'data_fim_oferecimento' => $fim_oferecimento->format('d/m/Y'),
        'link' => $item->link,
        'body' => $item->body,
        'responsavel' => isset($item->responsavel) ? $item->responsavel : null,
    ];
}

/**
 * Pagina de filtro do conteudo
 */
function ia_content_page(){
    global $wpdbIntranet;
    $rows = array();
    $clear = true;

    $query = "SELECT * from sintranet.com_content where status = 1 AND type in (1,2) ";

    if(isset($_GET['titulo']) and $titulo = $_GET['titulo']){
        $clear = false;
        $query .= " AND title like '%". sanitize_text_field($titulo)."%' ";
    }

    if(isset($_GET['titulo']) and $tipo = $_GET['tipo']){
        $clear = false;
        $query .= " AND type = " . sanitize_text_field($tipo);
    }


    if(isset($_GET['created_ini']) and $data = DateTime::createFromFormat('d/m/Y',$_GET['created_ini'])){
        $clear = false;
        $query .= " AND created >= '".$data->format('Y-m-d 00:00:00')."'";
    }

    if(isset($_GET['created_fim']) and $datafim = DateTime::createFromFormat('d/m/Y',$_GET['created_fim'])){
        $clear = false;
        $query .= " AND created <= '".$datafim->format('Y-m-d 23:59:59')."'";
    }

    if(isset($_GET['evento_ini']) and $evento = DateTime::createFromFormat('d/m/Y',$_GET['evento_ini'])){
        $clear = false;
        $query .= " AND evento_inicio >= '".$evento->format('Y-m-d 00:00:00')."'";
    }


    if(isset($_GET['evento_fim']) and $eventoFim = DateTime::createFromFormat('d/m/Y',$_GET['evento_fim'])){
        $clear = false;
        $query .= " AND evento_inicio <= '".$eventoFim->format('Y-m-d 23:59:59')."'";
    }


    if($clear){
        $query .= " ORDER BY id DESC LIMIT 50";
    } else {
        $query .= " ORDER BY id DESC LIMIT 50";
        ia_content_page_wp($rows);
    }

    ia_content_page_query($query,$rows);
    ksort($rows);

    return $rows;
}

/**
 * Realiza a pesquisa em eventos anteriores a migracao
 * @param $rows
 */
function ia_content_page_wp(&$rows){
    global $wpdbIntranet;

    $query = "select a.post_title title,1 as type, a.post_date created, inicio.meta_value as data, a.ID as id " .
        " from bdartes.wp_posts as a join bdartes.wp_postmeta inicio on inicio.post_id = a.ID " .
        " and inicio.meta_key = '_EventStartDate'";

    if(isset($_GET['titulo']) and $tipo = $_GET['tipo']){
        $query .= " AND a.post_title like '%" . sanitize_text_field($tipo) . "%'";
    }

    if(isset($_GET['created_ini']) and $data = DateTime::createFromFormat('d/m/Y',$_GET['created_ini'])){
        $query .= " AND a.post_date >= '".$data->format('Y-m-d 00:00:00')."'";
    }


    if(isset($_GET['created_fim']) and $datafim = DateTime::createFromFormat('d/m/Y',$_GET['created_fim'])){
        $query .= " AND a.post_date <= '".$datafim->format('Y-m-d 23:59:59')."'";
    }

    if(isset($_GET['evento_ini']) and $evento = DateTime::createFromFormat('d/m/Y',$_GET['evento_ini'])){
        $query .= " AND inicio.meta_value >= '".$evento->format('Y-m-d 00:00:00')."'";
    }


    if(isset($_GET['evento_fim']) and $eventoFim = DateTime::createFromFormat('d/m/Y',$_GET['evento_fim'])){
        $query .= " AND inicio.meta_value <= '".$eventoFim->format('Y-m-d 23:59:59')."'";
    }


    $query .= "AND post_parent = 0 ORDER BY id DESC LIMIT 500";


    foreach ($wpdbIntranet->get_results($query) as $item){
        $created = new DateTime($item->created);
        $rows[$created->format('Ymd'). str_pad($item->id,6,"0",STR_PAD_LEFT)] = [
            'id' => $item->id,
            'title' => $item->title,
            'chamada' => '',
            'type' => $item->type,
            'created' => $item->created,
            'data' => $item->data,
            'link' => get_permalink($item->id),
            'local' => '',
            'image' => null,
        ];
    }

}

function ia_content_page_query($query, &$rows) {
    global $wpdbIntranet;
    foreach ($wpdbIntranet->get_results($query) as $item){
        $created = new DateTime($item->created);
        $rows[$created->format('Ymd'). str_pad($item->id,6,"0",STR_PAD_LEFT)] = [
            'title' => $item->title,
            'chamada' => $item->evento_chamada,
            'type' => $item->type,
            'created' => $item->created,
            'data' => $item->evento_inicio ? new DateTime($item->evento_inicio) : null,
            'link' => 'content/'.$item->id,
            'local' => $item->evento_local,
            'image' => $item->evento_name ? WWW4_PUBLIC .'/media/cache/resolve/thumb/evento/'  . $item->evento_name : null,
        ];
    }

}

add_action('admin_head-post-new.php', 'pre_select_parent_wpse_56952');

function pre_select_parent_wpse_56952()
{
    // Check if adding new page
    if(!isset($_GET['post_type']) || 'page' != $_GET['post_type']) return;

    // Check for pre-selected parent
    if(!isset($_GET['the_id']) || empty( $_GET['the_id'] )) return;


    // There is a pre-selected value for the correct post_type, proceed with script
    $the_id = $_GET['the_id'];
    $the_title = get_the_title($the_id);

    ?>

    <script type="text/javascript">
        jQuery(document).ready( function($)
        {
            alert('Selecione como pai a página: <? echo $the_title ?>');
        });
    </script>
    <?php
}
