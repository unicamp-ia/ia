<?php

	if ((is_page(paginas_especiais())) || pagina_especial_filha(get_the_ID())) {
		$menu = "Pós-graduação em ";
		switch (get_page_parent_high_level(get_the_ID())) {
			case 99  : $menu = $menu . "Artes Visuais"; break;
			case 155 : $menu = $menu . "Multimeios";    break;
			case 88  : $menu = $menu . "Artes da Cena"; break;
			case 109 : $menu = $menu . "Música";        break;
		}
		echo("<small>Menu Específico do Programa</small>");
		wp_nav_menu( array("menu" => $menu , "container" => "nav" , "container_id" => "menu-especifico"));
	}
	echo("<small>Menu Comum à Pós-Graduação</small>");
	wp_nav_menu( array("menu" => "Pós-graduação" , "container" => "nav" , "container_id" => "menu-comum"));
	wp_nav_menu( array("menu" => "Viver em Barão Geraldo" , "container" => "nav" , "container_id" => "menu-comum"));

?>

<div class="contato">
	<p><strong>Coordenador Geral</strong></p>
	<?php echo(filtro_email_spam(get_field("coordenador-geral",168))); ?>
</div>
<?php if ((in_array(get_the_ID(),paginas_pos_graduacao_especificas())) || (pagina_pos_graduacao_especifica_filha(get_the_ID()))) { ?>
<div class="contato">
	<?php echo(filtro_email_spam(get_field("coordenador-da-area",get_page_parent_high_level(get_the_ID())))); ?>
</div>
<?php } ?>

<div class="contato">
	<?php echo(filtro_email_spam(get_field("secretaria",168))); ?>
</div>

<div class="contato">
	<p><strong>Telefones</strong></p>
	<?php echo(filtro_email_spam(get_field("telefones",168))); ?>
</div>
