<?php
/* Template Name: Home */


$tags = array(
    array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/nivo-slider.css"),
);

$tagsFooter = array(
    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/jquery.nivo.slider.pack.js"),
    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/home.js")
);


$contents = getContent();

$menu = null;
$menu['G'] = [
    4 => ['id' => 80, 'text' => ['pt' => 'Artes Cênicas','en' => 'Performing Arts','es' => 'Artes Escénicas', 'fr' => 'Arts de la Scène']],
    0 => ['id' => 97, 'text' => ['pt' => 'Artes Visuais','en' => 'Visual Arts','es' => 'Artes Visuales', 'fr' => 'Arts Visuels']],
    1 => ['id' => 105, 'text' => ['pt' => 'Comunicação Social - Midialogia','en' => 'Social Communication – Medialogy','es' => 'Comunicación Social: Mediología', 'fr' => 'Communication Sociale – Médialogie']],
    2 => ['id' => 68, 'text' => ['pt' => 'Dança','en' => 'Dance','es' => 'Danza', 'fr' => 'Danse']],
    3 => ['id' => 107, 'text' => ['pt' => 'Música','en' => 'Music','es' => 'Música', 'fr' => 'Musique']],
];
$menu['P'] = [
    4 => ['id' => 88, 'text' => ['pt' => 'Artes da Cena','en' => 'Performing Arts','es' => 'Artes de la Escena', 'fr' => 'Arts de la Scène']],
    0 => ['id' => 99, 'text' => ['pt' => 'Artes Visuais','en' => 'Visual Arts','es' => 'Artes Visuales', 'fr' => 'Arts visuels']],
    1 => ['id' => 155, 'text' => ['pt' => 'Multimeios','en' => 'Multimedia','es' => 'Multimedios', 'fr' => 'Multimédias']],
    2 => ['id' => 109, 'text' => ['pt' => 'Música','en' => 'Music','es' => 'Música', 'fr' => 'Musique']],
];


$lang = ia_get_lang();
if($lang == 'pt'){
    $main_menu = 'Principal';
} else {
    $main_menu = 'Principal-' . $lang;
}

Timber::render('twig/index.'.$lang.'.twig',[
    'contents' => $contents,
    'lang' => $lang,
    'main_menu' => $main_menu,
    'nome_area' => get_area_name(get_the_ID()),
    'menu' => $menu,
    'topbar' => getNewsflash(),
]);

function getImagemTopo($id){
    $imagem_topo = get_field("imagem_topo", get_page_parent_high_level($id));
    if($imagem_topo){
        return $imagem_topo['url'];
    }

    return null;
}