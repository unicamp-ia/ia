<?php
$lang = ia_get_lang();

$post_data = get_post($post->ID);

Timber::render('twig/page-nosidebar.twig',[
    'template_name' => 'page',
    //'main_menu' => getMainMenuName(get_the_ID(), $lang),
    //'contato' => filtro_email_spam(get_field("contato", get_page_parent_high_level(get_the_ID()))),
    'content' => the_post(),
]);

