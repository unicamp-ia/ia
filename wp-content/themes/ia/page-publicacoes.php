<?php
/*
Template Name: Pesquisa Extensão - Publicações
*/

$lang = ia_get_lang();

echo is_user_logged_in();


// Busca o grupo do programa na página de grupo de pesquisa
$a = get_field('anais_de_eventos', 842);
$e = get_field('e-livros', 842);
$p = get_field('periodicos_cientificos', 842);


Timber::render('twig/page-publicacoes.twig',[
    'template_name' => 'page',
    'main_menu' => getMainMenuName(get_the_ID(), $lang),
    'contato' => filtro_email_spam(get_field("contato", get_page_parent_high_level(get_the_ID()))),
    'a' => $a,
    'e' => $e,
    'p' => $p,
    'content' => the_post(),
]);

