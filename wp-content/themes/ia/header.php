﻿
<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
<head>
    <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php if (is_search()) { ?><meta name="robots" content="noindex, nofollow" /><?php } ?>

<?php
	$template_directory = get_bloginfo('template_directory');
	$stylesheet_url = get_bloginfo('stylesheet_url');
	$nome_area = get_area_name(get_the_ID());
    $template_directory = str_replace('http://', 'https://', $template_directory);
    $stylesheet_url = str_replace('http://', 'https://', $stylesheet_url);

?>

    <link rel="shortcut icon" href="<?php echo($template_directory); ?>/img/icons/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo($template_directory); ?>/css/variables.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo($template_directory); ?>/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo($template_directory); ?>/css/jquery.mosaic.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo($template_directory); ?>/css/home-new.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo($template_directory); ?>/css/carousel.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo($template_directory); ?>/css/news.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo($template_directory); ?>/css/content.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo($template_directory); ?>/css/header.css" media="screen" />


    <link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet" >

    <link rel="pingback" href="<?php bloginfo("pingback_url"); ?>" />



    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha256-YLGeXaapI0/5IgZopewRJcFXomhRMlYYjugPLSyNjTY=" crossorigin="anonymous" />

    <!-- slick -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha256-UK1EiopXIL+KVhfbFa8xrmAWPeBjMVdvYMYkTAEv/HI=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" integrity="sha256-4hqlsNP9KM6+2eA8VUT0kk4RsMRTeS7QGHIM+MZ5sLY=" crossorigin="anonymous" />


    <?php
	global $tags;
	if (isset($tags)) {
		foreach ($tags as $tag) {
			switch ($tag["tag"]) {
				case "link"   : { echo("<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $tag["href"] . "\" media=\"" . (isset($tag["media"]) ? $tag["media"] : "all") . "\" />\n"); break;}
				case "script" : { echo("<script type=\"text/javascript\" src=\"" . $tag["src"] . "\"></script>\n"); break;}
				case "meta"   : { echo("<meta name=\"" . $tag["name"] . "\" content=\"" . $tag["content"] . "\" />\n"); break;}
			}
		}
	}
?>

<?php wp_head(); ?>

<script>
    jQuery(function($){
        $('.menu-btn').click(function(){
           $('.responsive-menu').toggleClass('expand');
           $('#cssmenu ul ul:visible').slideUp('normal');
           $('#cssmenu ul ul ul:visible').slideUp('normal');
        })

        $('#cssmenu > ul > li > a').click(function() {
            var checkElement = $(this).next();

            if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                $(this).closest('li').removeClass('active');
                checkElement.slideUp('normal');
                $(this).closest('ul a').find('#haschildren').remove();
                $(this).closest('ul a').append('<span id="haschildren" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');
            }

            if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                $('#cssmenu ul ul:visible').slideUp('normal');
                checkElement.slideDown('normal');
                $(this).closest('ul a').find('#haschildren').remove();
                $(this).closest('ul a').append('<span id="haschildren" class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>');
            }


        });

        $('#cssmenu > ul > li > ul > li > a').click(function() {
            var checkElement = $(this).next();

            if((checkElement.is('ul ul')) && (checkElement.is(':visible'))) {
                $(this).closest('li ul').removeClass('active');
                checkElement.slideUp('normal');
                $(this).closest('li ul a').find('#haschildren').remove();
                $(this).closest('li ul a').append('<span id="haschildren" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');
            }

            if((checkElement.is('ul ul')) && (!checkElement.is(':visible'))) {
                $('#cssmenu ul ul ul:visible').slideUp('normal');
                checkElement.slideDown('normal');
                $(this).closest('li ul a').find('#haschildren').remove();
                $(this).closest('li ul a').append('<span id="haschildren" class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>');
            }
        });

        $(window).resize(function() {
            alteraMenu();
        });

    })

    function alteraMenu() {
        $('#cssmenu > ul').find('span').remove();
        var menu_area = '<?php echo $nome_area; ?>';
        menu_area = menu_area + '-menuprograma';
        if ($('#desktop').css('display') === 'none' ) {
            $('.menu-item').addClass('ajustamenu-mobile');
            $('#cssmenu > ul > li').addClass('corsite');
            $('#cssmenu > ul > li > ul > li').addClass('corsite');
            $('#cssmenu > ul').find('.menu-item-has-children > a').append('<span id="haschildren" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');

            $('#csssubmenu > ul > li').addClass(menu_area);
        } else {
            $('#cssmenu > ul').find('span').remove();
            $('#cssmenu > ul > li').removeClass('corsite');
            $('#cssmenu > ul > li > ul > li').removeClass('corsite');
            $('.menu-item').removeClass('ajustamenu-mobile');

            $('#csssubmenu > ul > li').removeClass(menu_area);
        }
    }



</script>

</head>

<body <?php body_class(body_slug()); ?>>

<header id="header" class="d-flex justify-content-start">
    <div id="logos" class="col col-lg-2 align-self-stretch text-center align-items-center">
        <div class="row align-items-center h-100">
            <div class="col mx-auto">
                <a class="navbar-brand" href="<?php bloginfo("url"); ?>">
                    <img src="<?php bloginfo("template_url"); ?>/img/logo-normal.png"/>
                </a>
                <a href="http://www.unicamp.br" class="navbar-brand">
                    <img src="<?php bloginfo("template_url"); ?>/img/logo-unicamp-t.png" />
                </a>
            </div>
        </div>

    </div>
    <div id="menus" class="col col-lg-10 align-self-stretch">
        <div class="row">
            <div class="col menu-servicos">
                <nav class="navbar navbar-expand-lg ">
                    <div class="container-fluid">
                        <?php wp_nav_menu( array("menu" => "Servicos" ,'container_id'=>'nav-servicos', 'menu_class' => 'navbar-nav ' )); ?>

                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <?php pll_the_languages(array('show_flags'=>1,'show_names'=>1, 'hide_if_no_translation'=>1, 'hide_current'=>1, 'force_home'=>0));?>
                            </li>
                            <li>
                                <form id="search" action="<?php bloginfo("url"); ?>" method="get" class="">
                                    <div class="input-group">
                                        <input type="text" class="form-control"  name="s" id="s" placeholder="Buscar"/>
                                        <span class="input-group-btn">
                                            <button type="submit" class="" aria-label="Pesquisar" title="Pesquisar">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12 menu-principal">
                <nav class="navbar navbar-expand-lg ">
                    <?php wp_nav_menu(array("menu" => "Principal" ,'container_id'=>'nav-principal', 'container_class' => 'navbar-nav ','menu_class' => 'navbar-nav ' )); ?>
                </nav>
            </div>
        </div>
    </div>
</header>

<div class="">
	<div id="content">