<?php

$content = [];

while (have_posts()):
  the_post();
  $content[] = [
    'title' => the_title('','',0),
    'link' => get_post_permalink(),
];
endwhile;

Timber::render('twig/category.twig',[
    'template_name' => 'category',
    'content' => $content,
]);


?>

