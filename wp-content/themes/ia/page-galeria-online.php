<?php
/*
Template Name: GaleriaOnline
*/

function my_theme_custom_mimes( $existing_mimes ) {
    $existing_mimes['json'] = 'application/json';
    $existing_mimes['unityweb'] = 'application/octet-stream';
    $existing_mimes['data'] = 'application/octet-stream';
    $existing_mimes['woff2'] = 'application/x-font-woff2';
    return $existing_mimes;
}
add_filter('mime_types', 'my_theme_custom_mimes');

$lang = ia_get_lang();

$tags = [
//    array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/bootstrap.css"),
//    array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/nivo-slider.css"),
//    array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/pagina-especial-com-slide.css")
];

$tagsFooter = array(
//    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/jquery.nivo.slider.pack.js"),
//    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/pagina-especial.js"),
//    array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/pagina-especial-com-slide.js")
);

$main_menu = getMainMenuName(get_the_ID(), $lang);

$avisos = get_field("avisos", get_the_ID());
$imagem_topo = get_field("imagem_topo", get_page_parent_high_level(get_the_ID()));
$post_data = get_post($post->ID);

$unity_files = html_entity_decode($post_data->post_content);
$unity_files = json_decode($unity_files, true);

$ehMobile="";
if(preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"])){
    $ehMobile="SIM";
}

Timber::render('twig/page-galeria-online.twig', [
    'template_name' => 'pos-especial',
    'content' => the_post(),
    'main_menu' => $main_menu,
    'ehMobile' => $ehMobile,
    'imagem_topo' => $imagem_topo['url'],
    'topbar' => getNewsflash(),
    'unity_files' => $unity_files,
    'wp_content' => content_url(),
    ]);



