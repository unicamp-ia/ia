<?php
/*
Template Name: Especial sem Sidebar
*/
?>
<?php

	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/nivo-slider.css"),
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/pagina-especial-com-slide.css"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/jquery.nivo.slider.pack.js"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/pagina-especial.js"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/pagina-especial-com-slide.js")
	);

	$menu = "Menu Superior ";
	switch (get_area_name(get_the_ID())) {
		case "artes-visuais" : $menu = $menu . "Artes Visuais"; break;
		case "comunicacao"   : $menu = $menu . "Comunicação"; break;
		case "danca"         : $menu = $menu . "Dança"; break;
		case "musica"        : $menu = $menu . "Música"; break;
		case "teatro"        : $menu = $menu . "Teatro"; break;
	}
	
	get_header();
?>


<?php
	if ((is_page(paginas_especiais())) || (pagina_especial_filha(get_the_ID()))) {
		
		include("slides.php");
		slider($imagens[get_area_name(get_the_ID())]);
	
	}
?>

</div>
<div class="row-fluid">

<div id="menu-lateral" class="span3 <?php echo(get_area_name(get_the_ID())); ?>">
    <?php
        
        $post_data = get_post($post->ID);
        
        switch (menu_lateral($post_data->post_name,$post->ID)) {
            case "graduacao"             : include("lateral-graduacao.php"); break;
            case "pos-graduacao"         : include("lateral-pos-graduacao.php"); break;
            case "departamento"          : include("lateral-departamento.php"); break;
            //case "pesquisas-publicacoes" : include("lateral-pesquisas-publicacoes.php"); break;
            default                      : break;
        }
        
        
        //get_post_nivel();
    
    ?>
</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="span9">
        <h1><?php the_title(); ?></h1>
        <?php the_breadcrumb(); ?>
        <?php
        /*
            switch (get_the_ID()) {
                case 88  : { echo apply_filters("the_content", get_post_field("post_content",92)); break; }
                case 124 : { echo apply_filters("the_content", get_post_field("post_content",105)); break; }
                default  : { the_content(); break; }
            }
			*/
			the_content();
        
        ?>
    </div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>