<?php
/*
Template Name: Eventos
*/
?>
<?php
	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/eventos.css")
	);

	get_header();
?>
<div class="tabbable tabs-left">
    <ul class="nav nav-tabs">
        <?php
            $categorias = get_terms(array("tribe_events_cat"),array("hide_empty" => 0));
            foreach ($categorias as $categoria) {
            	echo("<li");
				if (tribe_meta_event_category_name() == $categoria->name) { echo(" class=\"active\""); }
				echo("><a href=\"" . get_term_link($categoria) . "\">" . $categoria->name . "</a></li>\n");
            }
        ?>
    </ul>
    <div class="tab-content">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        <?php endwhile; else: ?>
            <p>Página não encontrada</p>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>