<?php

	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/nivo-slider.css"),
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/home.css"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/jquery.nivo.slider.pack.js"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/home.js")
	);
	
	get_header();
	
	switch (qual_site()) {
		case "danca" : {
			$imagens = array(
				array("src" => "slide/danca/1.jpg" , "title" => "" , "alt" => ""),
				array("src" => "slide/danca/2.jpg" , "title" => "" , "alt" => ""),
				array("src" => "slide/danca/3.jpg" , "title" => "" , "alt" => "")
			);
			break;
		}
		case "teatro" : {
			$imagens = array(
				array("src" => "slide/teatro/1.jpg" , "title" => "" , "alt" => "")
			);
			break;
		}
	}
?>

	<?php slider($imagens); ?>
    
    <br /><br />

    
<?php get_footer(); ?>