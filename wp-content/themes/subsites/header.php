<!doctype html>
<!--[if lt IE 7]> <html class="ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->

<head>
<meta charset="<?php bloginfo("charset"); ?>" />
<title><?php bloginfo("name");?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" <?php /*content="width=1000"*/?> />
<?php if (is_search()) { ?><meta name="robots" content="noindex, nofollow" /><?php } ?>

<link rel="apple-touch-icon" href="<?php bloginfo("template_directory"); ?>/img/icons/apple57.png"/>  
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo("template_directory"); ?>/img/icons/apple72.png"/>  
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo("template_directory"); ?>/img/icons/apple114.png"/> 
<link rel="shortcut icon" href="<?php bloginfo("template_directory"); ?>/img/icons/favicon.ico" />

<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/bootstrap.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo("stylesheet_url"); ?>" media="screen" />
<link rel="pingback" href="<?php bloginfo("pingback_url"); ?>" />

<!--[if lt IE 9]><script type="text/javascript" src="<?php bloginfo("template_directory"); ?>/js/ie8.js"></script><![endif]-->

<script type="text/javascript" src="<?php bloginfo("template_directory"); ?>/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo("template_directory"); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo("template_directory"); ?>/js/script.js"></script>

<?php
	global $tags;
	if (isset($tags)) {
		foreach ($tags as $tag) {
			switch ($tag["tag"]) {
				case "link"   : { echo("<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $tag["href"] . "\" media=\"screen\" />\n"); break;}
				case "script" : { echo("<script type=\"text/javascript\" src=\"" . $tag["src"] . "\"></script>\n"); break;}
				case "meta"   : { echo("<meta name=\"" . $tag["name"] . "\" content=\"" . $tag["content"] . "\" />\n"); break;}
			}
		}
	}
?>

<?php wp_head(); ?>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-3176901-1', 'unicamp.br');
	ga('send', 'pageview');
        
        function versaoDesktop() {
            var $viewport = $('head meta[name="viewport"]');    
            $viewport.attr('content', 'width=1000');
        }
        
        jQuery(function($){
            $('.menu-btn').click(function(){
               $('.responsive-menu').toggleClass('expand');
               $('#cssmenu ul ul:visible').slideUp('normal');
               $('#cssmenu ul ul ul:visible').slideUp('normal');
            })       
            
            $('#cssmenu > ul > li > a').click(function() {
                if ($('#desktop').css('display') === 'none' ) {
                    var checkElement = $(this).next();

                    if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                        $(this).closest('li').removeClass('active');
                        checkElement.slideUp('normal');
                        $(this).closest('ul a').find('#haschildren').remove();
                        $(this).closest('ul a').append('<span id="haschildren" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');
                    }

                    if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                        $('#cssmenu ul ul:visible').slideUp('normal');
                        checkElement.slideDown('normal');
                        $(this).closest('ul a').find('#haschildren').remove();
                        $(this).closest('ul a').append('<span id="haschildren" class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>');
                    }

                }
            });
            
            $('#cssmenu > ul > li > ul > li > a').click(function() {
                if ($('#desktop').css('display') === 'none' ) {
                    var checkElement = $(this).next();

                    if((checkElement.is('ul ul')) && (checkElement.is(':visible'))) {
                        $(this).closest('li ul').removeClass('active');
                        checkElement.slideUp('normal');
                        $(this).closest('li ul a').find('#haschildren').remove();
                        $(this).closest('li ul a').append('<span id="haschildren" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');
                    }

                    if((checkElement.is('ul ul')) && (!checkElement.is(':visible'))) {
                        $('#cssmenu ul ul ul:visible').slideUp('normal');
                        checkElement.slideDown('normal');
                        $(this).closest('li ul a').find('#haschildren').remove();
                        $(this).closest('li ul a').append('<span id="haschildren" class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>');
                    }

                }
            });
            
            $(window).resize(function() {
                alteraMenu();
            });
            
        })
        
        function alteraMenu() {
            $('#cssmenu > ul').find('span').remove();
            if ($('#desktop').css('display') === 'none' ) {
                $('.menu-item').addClass('ajustamenu-mobile');
                $('#cssmenu > ul > li').addClass('corsite');
                $('#cssmenu > ul > li > ul > li').addClass('corsite');
                $('#cssmenu > ul').find('.menu-item-has-children > a').append('<span id="haschildren" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');
            } else {
                $('#cssmenu > ul').find('span').remove();
                $('#cssmenu > ul > li').removeClass('corsite');
                $('#cssmenu > ul > li > ul > li').removeClass('corsite');
                $('.menu-item').removeClass('ajustamenu-mobile');
            }
        }
                
        jQuery(document).ready(function($) {
            if ($.browser.msie) {
                if(parseInt($.browser.version) <= 8){
                    $('body').find('#mobile').remove();
                }
            }

            $('.responsive-submenu').toggleClass('expand');
            alteraMenu();            
        });
</script>
</head>

<body <?php body_class(); ?>>
<header>
    <div id="desktop">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if (is_home()) { ?><h1 id="logo">Instituto de Artes</h1><?php }
                    else { ?><a id="logo" href="<?php bloginfo("url"); ?>">Instituto de Artes</a><?php } ?>

                    <?php
                        switch_to_blog(1);
                        wp_nav_menu( array("menu" => "Principal" , "container" => "nav" , "container_id" => "nav-main"));
                        restore_current_blog();
                    ?>

                    <div id="unicamp"><a href="http://www.unicamp.br" target="_blank"><img src="<?php bloginfo("template_url"); ?>/img/unicamp.png" /></a></div>

                </div> <!-- div.col-md-12 -->
            </div> <!-- div.row -->
        </div> <!-- div.container -->
    </div>
    
    <div id="mobile">
        <div class="container" >
            <div class="row config">
                <div class="col-md-12">
                    <div><a href="<?php bloginfo("url");?>"><img id="logo-ia-mobile" src="<?php bloginfo("template_url"); ?>/img/logo_mobile.png"/><img id="titulo-mobile" src="<?php bloginfo("template_url"); ?>/img/descricao_ia_mobile.png"/></a></div>
                      
                    <div id="unicamp-mobile"><a href="http://www.unicamp.br" target="_blank"><img src="<?php bloginfo("template_url"); ?>/img/unicamp-mobile.png" /></a></div>
                </div>
            </div>
            
            <div id="divpesq" class="row">
                <div class="col-md-12">
                    <div class="mobile-nav">
                        <div class="menu-btn" id="menu-btn">
                            <button id="btn-menu-principal-mobile">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    </div>
                    
                </div>
                <div id="divsobpesq" class="row"></div>
                <div class="responsive-menu container-fluid">
                    <nav>
                        <?php 
                            switch_to_blog(1);
                            wp_nav_menu(array("menu" => "Principal" , 'container' => 'div', "container_class" => "nav", "container_id" => "cssmenu" ) ); 
                            restore_current_blog();
                        ?>
                        <div id="cssmenu" class="nav">
                            <ul id="menu-principal-1" class="menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9999 ajustamenu-mobile corsite"><a href="#" onclick="versaoDesktop();"><?php echo utf8_encode("Vers�o Desktop")?></a></li>
                            </ul>
                        </div>                    
                    </nav>
                </div>    
            </div>
        </div>
    </div>    
</header>

<div id="titulo" class="<?php echo(qual_site()); ?>"><?php bloginfo("name"); ?></div>

<div class="container">
	<div id="content">
	    <div class="row">
            <div class="col-md-3">
				<?php
						add_filter("nav_menu_css_class" , "special_nav_class" , 10 , 2);
						function special_nav_class($classes, $item){
							 $classes[] = "list-group-item";

							 if ($item->object_id == get_the_ID()) { $classes[] = "active"; }
							 
							 return $classes;
						}
				
                		wp_nav_menu( array(
										"menu"            => "Principal",
										"container"       => "div",
										"container_id"    => "menu-principal",
										"container_class" => "panel panel-default",
										"menu_class"      => "list-group"
						)); ?>
            </div>
            
            <div class="col-md-9">