<?php
	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/categoria.css")
	);
	
	get_header();
?>

<h1>Categoria <em><?php single_cat_title(); ?></em></h1>

<?php the_breadcrumb(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<section>
		<?php if (has_post_thumbnail()) {?>
			<a class="imagem" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail("home"); ?></a>
		<?php } ?>
		<h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		<p><?php echo excerpt(80); ?></p>
	</section>
<?php endwhile; ?>

<?php the_pagination(); ?>

<?php else: ?>
	<p>Não há posts nesta categoria</p>
<?php endif; ?>

<?php get_footer(); ?>