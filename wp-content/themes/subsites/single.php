<?php
	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/post.css")
	);

	get_header();
?>

<?php if ( have_posts() ) :
		while ( have_posts() ) : the_post(); ?>
            <h1><?php the_title(); ?></h1>
            
            <?php the_breadcrumb(); ?>
            
            <?php if (has_post_thumbnail()) {?>
                <a id="imagem-destacada" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            <?php } ?>
            
            <?php
            the_content();
		endwhile;
	else: ?>
	    <p>Post não encontrado</p>
<?php endif; ?>

<?php get_footer(); ?>