<?php
		
	$imagens = array(
		"artes-visuais" => array(
			array("src" => "artes-visuais/1.jpg" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => ""),
			array("src" => "artes-visuais/2.jpg" , "title" => "Foto: Geraldo Porto"        , "alt" => ""),
			array("src" => "artes-visuais/3.jpg" , "title" => "Foto: Priscila Guerra"      , "alt" => "")
		),
		
		"comunicacao" => array(
			array("src" => "comunicacao/1.png" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => ""),
			array("src" => "comunicacao/2.png" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => ""),
			array("src" => "comunicacao/3.png" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => "")
		),
		
		"danca" => array(
			array("src" => "danca/1.jpg" , "title" => "Foto: Roberto Berton"  , "alt" => ""),
			array("src" => "danca/2.jpg" , "title" => "Foto: Roberto Berton"  , "alt" => ""),
			array("src" => "danca/3.jpg" , "title" => "Foto: Carlos Penteado" , "alt" => "")
		),
		
		"musica" => array(
			array("src" => "musica/1.png" , "title" => "Foto: Maria Lúcia Fagundes" , "alt" => "")
		),
		
		"teatro" => array(
			array("src" => "teatro/1.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
			array("src" => "teatro/2.jpg" , "title" => "Foto: Celso Palermo" , "alt" => "")
		)
	);
		
?>