			</div> <!-- div.col-md-9 -->
		</div> <!-- div.row -->
   	</div> <!-- div#content -->
</div> <!-- div.container -->

<footer class="footer-mobile">
    <div class="container">
    	<p class="endereco">
            Rua Elis Regina, 50<br />
            Cidade Universitária "Zeferino Vaz"<br />
            Barão Geraldo, Campinas - SP<br />
            CEP: 13083-854<br />
            Fax: +55 (19) 3521-7827
        </p>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
