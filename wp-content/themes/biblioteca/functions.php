<?php

remove_action("wp_head", "wp_generator"); // Remove a versão do Wordpress por questões de segurança
define("WP_DEBUG", false); // Desabilita o degub


add_action("admin_head", function  () {
   if (!current_user_can("manage_options")) {
	   echo '<style type="text/css">
			   td.mce_h1,
			   td.mce_h4,
			   td.mce_h5,
			   td.mce_h6,
			   
			   
			   li#wp-admin-bar-comments,
			   li#wp-admin-bar-new-media,
			   li#menu-media,
			   li#menu-comments,
			   li#menu-tools { display:none; }
			 </style>
			 ';
   }
});

function htaccess_notice () {
    global $pagenow;
    if ( $pagenow == "options-permalink.php" ) {
         echo '<div class="updated">
             <p><strong>ATENÇÃO!</strong> O arquivo <strong>.htaccess</strong> foi modificado manualmente. Qualquer alteração nesta página pode sobrescrever as configurações.</p>
         </div>';
    }
}
add_action("admin_notices", "htaccess_notice");

function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).' &#91;...&#93;';
	}
	else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

if ( function_exists("add_theme_support") ) {
	add_theme_support("post-thumbnails");
	set_post_thumbnail_size(144,189,true);
}

function the_breadcrumb () {
	
	$lista_negra = array();
	
    $separator = "&raquo;";
	global $post;
	
    echo "<ul id=\"breadcrumbs\" class=\"list-inline\">";
	
	echo "<li><a href=\"" . get_option("home") . "\">Home</a></li><li class=\"separator\">$separator</li>";
	
	if (is_category()) {
		global $wp_query;
		$category = $wp_query->get_queried_object();
		echo "<li>" . $category->name . "</li>";
	}
	elseif (is_single()) {
		$categories = get_the_category();
		foreach ($categories as $category) {
			if ($category->cat_ID != 66) { // Home
				echo "<li><a href=\"" . get_category_link($category->cat_ID) . "\" title=\"" . $category->name . "\">" . $category->name  . "</a></li><li class=\"separator\">$separator</li>";
			}
		}
		echo "<li>".get_the_title()."</li>";
	}
	elseif (is_page()) {
		if($post->post_parent){
			$anc = array_reverse(get_post_ancestors($post->ID));
			$title = get_the_title();
			foreach ( $anc as $ancestor ) {
				if (in_array($ancestor,$lista_negra)) {
					$output = $output . "<li>".get_the_title($ancestor)."</li>";						
				}
				else {
					$output = $output . "<li><a href=\"".get_permalink($ancestor)."\" title=\"".get_the_title($ancestor)."\">".get_the_title($ancestor)."</a></li>";
				}
				$output = $output . "<li class=\"separator\">$separator</li>";						
			}
			echo $output;
			echo "<li><strong title=\"$title\">$title</strong></li>";
		} else {
			echo "<li><strong>".get_the_title()."</strong></li>";
		}
	}
    echo "</ul>";
}

function the_pagination () {
	
	global $wp_query;
	
	$big = 999999999; // need an unlikely integer
	
	$paginas = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'type' => 'array'
	) );
	
	if(isset($paginas)) {
		echo("<nav><ul class=\"pagination\">");
		foreach($paginas as $pagina) {
			if (strpos($pagina, "current") === false) {
				echo("<li>$pagina</li>");
			} else {
				echo("<li class=\"active\">$pagina</li>");
			}
		}
		echo("</ul></nav>");
	}
}

add_filter( "the_content", "filtro_email_spam", 20 );
function filtro_email_spam( $content ) {
	
	$pattern="/(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";
	preg_match_all($pattern, $content, $matches);
	
	$matches = array_unique($matches[0]);
	
	foreach ($matches as $email) {
		$email_filtrado = str_replace("@", " (arroba) ", $email);
		$email_filtrado = str_replace(".", " (ponto) ", $email_filtrado);
		
		$content = str_replace($email,$email_filtrado,$content);
	}

    return $content;
}

add_theme_support("menus");

function include_shortcode( $atts, $content = null ) {
	include("/var/www/html/ia/wp-content/themes/biblioteca/".$content);
}
add_shortcode("include","include_shortcode");

function php_shortcode( $atts, $content = null ) {
	//eval(htmlentities(html_entity_decode($content)));
	//echo(html_entity_decode($content,ENT_QUOTES));
	//echo(html_entity_decode($content, ENT_QUOTES, "Windows-1252"));
	//echo(html_entity_decode("&amp;"));
	
    $search = array("&#8217;","&#8220;","&#8221;","&gt;","&lt;","&#8211;"); 
    $replace = array("'","\"","\"",">","<","--"); 
 
    return eval(html_entity_decode(str_replace($search, $replace, $content)));
	//echo(html_entity_decode(str_replace($search, $replace, $content)));
	
}
add_shortcode("php","php_shortcode");


function stop_echo() {
	ob_start();
}

function print_echo() {
	$saida = ob_get_contents();
	ob_end_clean();
	echo(utf8_encode($saida));
}




function slider ($imagens) {
	$path = get_bloginfo("template_url") . "/img/";
	echo("<div id=\"slider-wrapper\"><div id=\"slider\">");
	foreach ($imagens as $imagem) {			
		echo("<a href=\"javascript:void(0);\"><img src=\"" . $path . $imagem["src"] . "\" title=\"" . $imagem["title"] . "\" alt=\"" . $imagem["alt"] . "\" /></a>");
	}
	echo("</div></div>");
}





function permalink_shortcode($atts, $content = null) {
	return get_permalink($content);
}
add_shortcode("permalink","permalink_shortcode");

?>
