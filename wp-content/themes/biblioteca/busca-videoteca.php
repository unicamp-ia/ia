<?php

	//registro de acesso
	include("conexao.php");

        if (getenv(HTTP_X_FORWARDED_FOR)) $end = getenv(HTTP_X_FORWARDED_FOR);
        else $end = getenv(REMOTE_ADDR);
	//$end = $_SERVER['REMOTE_ADDR'];
  	//echo $end;
	$area = 2;
	$data = date("Y",time())."-".date("m",time())."-".date("d",time());

	// biblioteca IA
	if((($end>='143.106.55.129') && ($end<='143.106.55.158')) || ($end=='143.106.55.46')) {
	//if((($end>='143.106.55.129') && ($end<='143.106.55.158')) || $end=='::1') {
//		echo "biblioteca";
		$cli=1;
	}elseif(substr($end,0,10)=='143.106.55') {
//		echo "IA";
		$cli=2;
	}elseif(substr($end,0,7)=='143.106') {
//		echo "UNICAMP";
		$cli=3;
	}elseif(substr($end,0,7)!='143.106') {
//		echo "externo";
		$cli=4;
	}

	if((session_id()!=$id) || ($area!=$ar)) {
		$qAc = "insert into estat(cli,dt,ar,ender) values('$cli','$data','$area','$end')";
		$resAc = mysqli_query($link, $qAc);
	}
	$_SESSION['ar'] = $area;
	//

	// somente na biblioteca
	//if((($end>='143.106.55.129') && ($end<='143.106.55.154')) || $end=='::1') {
	if((($end>='143.106.55.129') && ($end<='143.106.55.158'))  || ($end=='143.106.55.46') || ($end=='143.106.55.32')) {

			$or = $_REQUEST['or'];
			$campo = $_REQUEST['campo'];
			$genero = $_REQUEST['genero'];

			$cpcond = $_REQUEST['cpcond'];
			$cond = $_REQUEST['cond'];
			$valor = $_REQUEST['valor'];
			$som = ($_REQUEST['som']?1:0);
			$ordenacao = $_REQUEST['ordenacao'];

                        // AntiXSS
                        $char_XSS=array("<",">","/","(",";",")","script","javascript:","alert","font","div","style","body");
                        include("AntiXSS.php");
                        $valor = AntiXSS::setEncoding($valor, "UTF-8");
                        $valor = AntiXSS::setFilter($valor, "black");
                        if ($valor=="XSS Detected!") die();
                        $valor=str_replace($char_XSS, "", $valor);

                        $valor = str_replace(" ", "%", $valor);
                        //$valor = ereg_replace ("^[osas]{1,2}%", "", $valor);
                        if ((substr($valor,0,2)=='o%') || (substr($valor,0,2)=='a%')) 
                            { $valor = substr($valor,2); }
                        elseif ((substr($valor,0,3)=='os%') || (substr($valor,0,3)=='as%')) 
                            { $valor = substr($valor,3); }
                        
			if($cpcond=='cadfita.classific') {
				$valor = str_pad($valor, 5, "0", STR_PAD_LEFT);
			}

			// paginacao
			$npag = $_REQUEST['npag'];
			$regpp = 5;
			$npag = (trim($npag)>1?$npag:1);
			$regin = ($regpp * $npag) - $regpp;
			$qtprev = 10;

			if($cpcond!='') {
				if($cpcond=='ult') {
					$qCt = "select
								count(*)
							from
								cadfita
							inner join
								cadfilmes on cadfita.chvfilme=cadfilmes.chvfilme
							left join
								cadgenero on cadfilmes.chvgenero=cadgenero.chvgenero
							left join
								tabsist on cadfita.chvsist=tabsist.chvsist
							left join
								tabmidia on cadfita.chvmidia=tabmidia.chvmidia
							left join
								tabacervo on cadfita.chvacervo=tabacervo.chvacervo
							left join
								tabcromia on cadfita.chvcromia=tabcromia.chvcromia";
				}else{
					$qCt = "select
								count(*)
							from
								cadfita
							inner join
								cadfilmes on cadfita.chvfilme=cadfilmes.chvfilme
							left join
								cadgenero on cadfilmes.chvgenero=cadgenero.chvgenero
							left join
								tabsist on cadfita.chvsist=tabsist.chvsist
							left join
								tabmidia on cadfita.chvmidia=tabmidia.chvmidia
							left join
								tabacervo on cadfita.chvacervo=tabacervo.chvacervo
							left join
								tabcromia on cadfita.chvcromia=tabcromia.chvcromia";

					if($cpcond!= '0') {
						if($som==1) {
							$qCt = $qCt." where soundex(".$cpcond.") ".substr($cond,0,4)." soundex('".(substr($cond,4,1)=='1'?'%':'').$valor.(substr($cond,4,1)>='1'?'%':'')."')";
						}else{
							$qCt = $qCt." where ".$cpcond." ".substr($cond,0,4)." '".(substr($cond,4,1)=='1'?'%':'').$valor.(substr($cond,4,1)>='1'?'%':'')."'";
						}
					}
				}
			}else{
				if(($genero == 0) || ($genero == '')) {
					if($campo=='T') {
						$qCt = "select
									count(*)
								from
									cadfilmes
								inner join cadfita on cadfilmes.chvfilme=cadfita.chvfilme
								left join cadgenero on cadfilmes.chvgenero = cadgenero.chvgenero
								left join tabmidia on cadfita.chvmidia=tabmidia.chvmidia
								left join tabcromia on cadfita.chvcromia=tabcromia.chvcromia
								left join tabsist on cadfita.chvsist=tabsist.chvsist
								left join tabacervo on cadfita.chvacervo=tabacervo.chvacervo
								where
									titorig like '%$valor%' or tittrad like '%$valor%' or
									direcao like '%$valor%' or producao like '%$valor%' or
									elenco like '%$valor%' or sinopse like '%$valor%' or
									coment like '%$valor%'";
					}elseif($campo=='classific') {
						$chvmidia = substr($valor,0,4);
						$classific = substr($valor,4,5);
						$qCt = "select
									count(*)
								from
									cadfilmes
								inner join cadfita on cadfilmes.chvfilme=cadfita.chvfilme
								left join cadgenero on cadfilmes.chvgenero = cadgenero.chvgenero
								left join tabmidia on cadfita.chvmidia=tabmidia.chvmidia
								left join tabcromia on cadfita.chvcromia=tabcromia.chvcromia
								left join tabsist on cadfita.chvsist=tabsist.chvsist
								left join tabacervo on cadfita.chvacervo=tabacervo.chvacervo
								where
									classific='$classific' and cadfita.chvmidia=$chvmidia";
					}else{
						$qCt = "select
									count(*)
								from
									cadfilmes
								inner join cadfita on cadfilmes.chvfilme=cadfita.chvfilme
								left join cadgenero on cadfilmes.chvgenero = cadgenero.chvgenero
								left join tabmidia on cadfita.chvmidia=tabmidia.chvmidia
								left join tabcromia on cadfita.chvcromia=tabcromia.chvcromia
								left join tabsist on cadfita.chvsist=tabsist.chvsist
								left join tabacervo on cadfita.chvacervo=tabacervo.chvacervo
								where ";
								if($campo == 'tit') {
									$cW = "titOrig like '%$valor%' or titTrad like '%$valor%'";
								}else{
									$cW = "$campo like '%$valor%'";
								}
						$qCt = $qCt.$cW;
					}
				}else{
					$qCt = "select
								count(*)
							from
								cadfilmes
							inner join cadfita on cadfilmes.chvfilme=cadfita.chvfilme
							left join cadgenero on cadfilmes.chvgenero = cadgenero.chvgenero
							left join tabmidia on cadfita.chvmidia=tabmidia.chvmidia
							left join tabcromia on cadfita.chvcromia=tabcromia.chvcromia
							left join tabsist on cadfita.chvsist=tabsist.chvsist
							left join tabacervo on cadfita.chvacervo=tabacervo.chvacervo
							where
								cadfilmes.chvgenero=$genero";
				}
			}
			$resCt = mysqli_query($link, $qCt);
			$rCt =  mysqli_fetch_array($resCt);
			$qtpag = $rCt[0]/$regpp;
			if((int)$qtpag < $qtpag) {
				$qtpag = (int)$qtpag +1;
			}
			$i = (int)($npag - ($qtprev / 2));
			if($i < 1)
				$i = 1;
			$fim = (int)($npag + ($qtprev / 2));
			if($fim > $qtpag)
				$fim = $qtpag;

			// selecao dos registros
			if($cpcond!='') {
				if($cpcond=='ult') {
					$query = "select
								cadfilmes.titorig,cadfilmes.tittrad,cadfita.classific,tabmidia.descr,
								cadfilmes.direcao,cadfilmes.producao,cadfita.disponivel,cadgenero.descr,
								cadfilmes.chvfilme,cadfita.chvfita,cadfita.peca,tabdisp.descr,cadfilmes.coment,
								cadfita.estcopia,cadfilmes.legenda
							from
								cadfilmes
							inner join cadfita on cadfilmes.chvfilme=cadfita.chvfilme
							left join cadgenero on cadfilmes.chvgenero = cadgenero.chvgenero
							left join tabmidia on cadfita.chvmidia=tabmidia.chvmidia
							left join tabcromia on cadfita.chvcromia=tabcromia.chvcromia
							left join tabsist on cadfita.chvsist=tabsist.chvsist
							left join tabacervo on cadfita.chvacervo=tabacervo.chvacervo
							left join tabdisp on cadfita.chvdisp=tabdisp.chvdisp";
                                                if($ordenacao != "") 
                                                    {  $query = $query . " order by ".$ordenacao; }
                                                else 
                                                    {  $query = $query . " order by dataAquis"; }
						$query = $query . " desc limit $regin,$regpp";
				}else{
					$query1 = "select
								cadfilmes.titorig,cadfilmes.tittrad,cadfita.classific,tabmidia.descr,
								cadfilmes.direcao,cadfilmes.producao,cadfita.disponivel,cadgenero.descr,
								cadfilmes.chvfilme,cadfita.chvfita,cadfita.peca,tabdisp.descr,cadfilmes.coment,
								cadfita.estcopia,cadfilmes.legenda
							from
								cadfilmes
							inner join cadfita on cadfilmes.chvfilme=cadfita.chvfilme
							left join cadgenero on cadfilmes.chvgenero = cadgenero.chvgenero
							left join tabmidia on cadfita.chvmidia=tabmidia.chvmidia
							left join tabcromia on cadfita.chvcromia=tabcromia.chvcromia
							left join tabsist on cadfita.chvsist=tabsist.chvsist
							left join tabacervo on cadfita.chvacervo=tabacervo.chvacervo
							left join tabdisp on cadfita.chvdisp=tabdisp.chvdisp";

					if($cpcond!= '0') {
						if($som==1) {
							$query1 = $query1." where soundex(".$cpcond.") ".substr($cond,0,4)." soundex('".(substr($cond,4,1)=='1'?'%':'').$valor.(substr($cond,4,1)>='1'?'%':'')."')";
						}else{
							$query1 = $query1." where ".$cpcond." ".substr($cond,0,4)." '".(substr($cond,4,1)=='1'?'%':'').$valor.(substr($cond,4,1)>='1'?'%':'')."'";
						}
					}

					if($ordenacao != "") 
                                                    {  $query = $query . " order by ".$ordenacao; }
                                                else 
                                                    {  $query = $query . " order by ".$campo; }
						$query = $query . " limit $regin,$regpp";

					$query = $query1.$query2;
				}
			}else{
				if($genero == 0) {
					if($campo=='T') {
						$query = "select
									cadfilmes.titorig,cadfilmes.tittrad,cadfita.classific,tabmidia.descr,
									cadfilmes.direcao,cadfilmes.producao,cadfita.disponivel,cadgenero.descr,
									cadfilmes.chvfilme,cadfita.chvfita,cadfita.peca,tabdisp.descr,cadfilmes.coment,
									cadfita.estcopia,cadfilmes.legenda
								from
									cadfilmes
								inner join cadfita on cadfilmes.chvfilme=cadfita.chvfilme
								left join cadgenero on cadfilmes.chvgenero = cadgenero.chvgenero
								left join tabmidia on cadfita.chvmidia=tabmidia.chvmidia
								left join tabcromia on cadfita.chvcromia=tabcromia.chvcromia
								left join tabsist on cadfita.chvsist=tabsist.chvsist
								left join tabacervo on cadfita.chvacervo=tabacervo.chvacervo
								left join tabdisp on cadfita.chvdisp=tabdisp.chvdisp
								where
									cadfilmes.titorig like '%$valor%' or cadfilmes.tittrad like '%$valor%' or
									cadfilmes.direcao like '%$valor%' or cadfilmes.producao like '%$valor%' or
									cadfilmes.elenco like '%$valor%' or sinopse like '%$valor%' or
									coment like '%$valor%'";
                                                if($ordenacao != "") 
                                                    {  $query = $query . " order by ".$ordenacao; }
                                                else 
                                                    {  $query = $query . " order by titorig"; }
						$query = $query . " limit $regin,$regpp";
					}elseif($campo=='classific') {
						$query = "select
									cadfilmes.titorig,cadfilmes.tittrad,cadfita.classific,tabmidia.descr,
									cadfilmes.direcao,cadfilmes.producao,cadfita.disponivel,cadgenero.descr,
									cadfilmes.chvfilme,cadfita.chvfita,cadfita.peca,tabdisp.descr,cadfilmes.coment,
									cadfita.estcopia,cadfilmes.legenda
								from
									cadfilmes
								inner join cadfita on cadfilmes.chvfilme=cadfita.chvfilme
								left join cadgenero on cadfilmes.chvgenero = cadgenero.chvgenero
								left join tabmidia on cadfita.chvmidia=tabmidia.chvmidia
								left join tabcromia on cadfita.chvcromia=tabcromia.chvcromia
								left join tabsist on cadfita.chvsist=tabsist.chvsist
								left join tabacervo on cadfita.chvacervo=tabacervo.chvacervo
								left join tabdisp on cadfita.chvdisp=tabdisp.chvdisp
								where
									classific='$classific' and cadfita.chvmidia=$chvmidia";
                                                if($ordenacao != "") 
                                                    {  $query = $query . " order by ".$ordenacao; }
                                                else 
                                                    {  $query = $query . " order by titorig"; }
						$query = $query . " limit $regin,$regpp";
					}else{
						if (substr($campo,0,3)=='tab') {
							$tab = explode(".",$campo);
							$qTab = "select * from $tab[0] where $campo=$valor";
							$resTab = mysqli_query($link, $qTab);
							$dGenero = mysqli_fetch_array($resTab);
						}
						//
						$query = "select
									cadfilmes.titorig,cadfilmes.tittrad,cadfita.classific,tabmidia.descr,
									cadfilmes.direcao,cadfilmes.producao,cadfita.disponivel,cadgenero.descr,
									cadfilmes.chvfilme,cadfita.chvfita,cadfita.peca,tabdisp.descr,cadfilmes.coment,
									cadfita.estcopia,cadfilmes.legenda
								from
									cadfilmes
								inner join cadfita on cadfilmes.chvfilme=cadfita.chvfilme
								left join cadgenero on cadfilmes.chvgenero = cadgenero.chvgenero
								left join tabmidia on cadfita.chvmidia=tabmidia.chvmidia
								left join tabcromia on cadfita.chvcromia=tabcromia.chvcromia
								left join tabsist on cadfita.chvsist=tabsist.chvsist
								left join tabacervo on cadfita.chvacervo=tabacervo.chvacervo
								left join tabdisp on cadfita.chvdisp=tabdisp.chvdisp
								where ";
								if($campo == 'tit') {
									$cW = "titOrig like '%$valor%' or titTrad like '%$valor%'";
								}else{
									$cW = "$campo like '%$valor%'";
								}
						$query = $query.$cW;
                                                if($ordenacao != "") 
                                                    {  $query = $query . " order by ".$ordenacao; }
                                                else 
                                                    {
                                                    if($campo=='cadfita.dataaquis')
                                                        $query = $query." order by $campo desc,cadfilmes.titorig";
                                                    elseif($campo=='tit')
							$query = $query." order by cadfilmes.titorig,cadfilmes.titTrad";
                                                    else
                                                    $query = $query." order by $campo,cadfilmes.titorig"; }
						$query = $query." limit $regin,$regpp";
					}
				}else{
					$qGen = "select * from cadgenero where chvgenero=$genero";
					$resGen = mysqli_query($link, $qGen);
					$dGenero = mysqli_fetch_array($resGen);
					$query = "select
									cadfilmes.titorig,cadfilmes.tittrad,cadfita.classific,tabmidia.descr,
									cadfilmes.direcao,cadfilmes.producao,cadfita.disponivel,cadgenero.descr,
									cadfilmes.chvfilme,cadfita.chvfita,cadfita.peca,tabdisp.descr,cadfilmes.coment,
									cadfita.estcopia,cadfilmes.legenda
							from
								cadfilmes
							inner join cadfita on cadfilmes.chvfilme=cadfita.chvfilme
							left join cadgenero on cadfilmes.chvgenero = cadgenero.chvgenero
							left join tabmidia on cadfita.chvmidia=tabmidia.chvmidia
							left join tabcromia on cadfita.chvcromia=tabcromia.chvcromia
							left join tabsist on cadfita.chvsist=tabsist.chvsist
							left join tabacervo on cadfita.chvacervo=tabacervo.chvacervo
							left join tabdisp on cadfita.chvdisp=tabdisp.chvdisp
							where
								cadfilmes.chvgenero=$genero";
                                        if($ordenacao == "") 
                                            { $query = $query . " order by titorig"; }
                                        else 
                                            { $query = $query . " order by ".$ordenacao; }
					$query = $query . " limit $regin,$regpp";
				}
			}
			$result = mysqli_query($link, $query);

			$qtdd = mysqli_num_rows($result);
                        $ordenacao."</BR>".$query;
		?>

    <h2>Pesquisa na Videoteca</h2>
    
    <div class="alert alert-info" role="alert">
        <?php
            echo "<h3>Você buscou por: <i>" . str_replace("%"," ",(trim($dGenero[1])!=''?$dGenero[1]:$valor)) . "</i></h3>";
            echo "<p>" . $rCt[0] . " registros localizados</p>";
        ?>
    </div>

    <p><a href='javascript:nova("regvideo.htm","p")' >Regras de utiliza&ccedil;&atilde;o</a>
    
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <p class="navbar-text">Classificar por:</p>
            </div>
            
            <div class="collapse navbar-collapse" >
            <?php
				$link = get_page_link(get_the_ID());
            
                $ordenacoes = array(
                    "cadfilmes.titorig"                   => "Título original",
                    "cadfilmes.tittrad"                   => "Título traduzido",
                    "cadfita.classific"                   => "Classificação",
                    "cadfilmes.direcao,cadfilmes.titorig" => "Direção",
                    "cadgenero.descr,cadfilmes.titorig"   => "Gênero"
                );
                
                foreach ($ordenacoes as $key => $value) {
                    echo("<a class=\"btn btn-default navbar-btn");
                    if ($ordenacao == $key) { echo(" active"); }
                    echo("\" href=\"$link?op=$op&npag=1&valor=$valor&campo=$campo&genero=$genero&or=$or&cpcond=$cpcond&cond=$cond&som=$som&ordenacao=$key\">$value</a> ");
                }
            
            ?>
            </div>
        </div>
    </nav>
 
    
	<?php
    if($qtdd > 0) {
        echo "<table class=\"table table-bordered table-striped\">";
        $ct = 0;
        while($row = mysqli_fetch_array($result)) {
			
			// codificação
			foreach ($row as $key => $value) { $row[$key] = utf8_encode($value); }

            $ln = $ct/2;
            $ln = (int)$ln;
            if(($ln*2)==$ct) {
                $cl = "jan2";
            }else{
                $cl = "jan2l";
            }
			
			echo("<tr><td><table class=\"table\">");

            echo "<tr><td width=\"60%\"><b>Titulo original:</b> $row[0]</td>";
            echo "<td width=\"40%\"><b>Classificação:</b> ".substr($row[3],0,1).$row[2]."</td></tr>";
            echo "<tr><td><b>Título traduzido:</b> $row[1]</td>";
            echo "<td><b>Mídia:</b> $row[3]</td></tr>";
            echo "<tr><td><b>Direção:</b> $row[4]</td>";
            echo "<td>";
            if($row[6]==1) {
				echo "<span class=\"disponivel glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> Disponível";
            }else{
                if($row[11]=='')
					echo "<span class=\"indisponivel glyphicon glyphicon-remove\" aria-hidden=\"true\"></span> Indisponível";
                else
                    echo $row[11];

            }
            echo "</td>";
            echo "</tr>";

//								echo "<tr><td class=".$cl.">&nbsp;</td>";
//								echo "<td class=".$cl." colspan=2>Produ&ccedil;&atilde;o: ";
//								echo $row[5]."</td>";
//								echo "<td class=".$cl.">&nbsp;</td></tr>";

//										if(trim($row[12])!='')
//											echo "<tr><td class=".$cl.">&nbsp;</td><td class=".$cl." colspan=3>".$row[12]."</td><td class=".$cl.">&nbsp;</td></tr>";
            echo "<tr><td><b>Gênero:</b> $row[7]</td>";
//								echo $row[7]."&nbsp;&nbsp;pe&ccedil;a:&nbsp;".$row[10]."</td>";
            if($or=='if')
                echo "<td><a href='default2.php?chvfilme=$row[8]&op=80'>incluir fita<img src='./imagens/lapisb.gif' border=0></a></td>";
            if($or=='ar')
                echo "<td><a href='default2.php?chvfilme=$row[8]&op=110'>editar<img src='./imagens/lapisb.gif' border=0></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='capa.php?chvfita=$row[9]' target='blank'>capa<img src='./imagens/print.gif' border=0></a></td>";
            if(($or!='if') &&($or!='ar'))
                echo "<td><a href=\"".get_page_link(20)."?chvfilme=$row[8]&op=60&cpcond=$cpcond&cond=$cond&valor=$valor&or=$or&campo=$campo&genero=$genero&npag=$npag\" class=\"detalhes btn btn-default btn-xs\"><span class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span> Detalhes</a></td>";
            
			echo "</tr>";

            if(trim($row[12])!='')
                echo "<tr><td colspan=\"2\">$row[12]</td></tr>";

            echo "<tr><td><b>Estado da Cópia:</b> <span class=\"$row[13] glyphicon glyphicon-certificate\" aria-hidden=\"true\"></span> $row[13]";
            /*
			if($row[13]=='Otimo')
                echo "<img src='./imagens/1.gif'>";
            if($row[13]=='Bom')
                echo "<img src='./imagens/2.gif'>";
            if($row[13]=='Razoavel')
                echo "<img src='./imagens/3.gif'>";
            if($row[13]=='Sofrivel')
                echo "<img src='./imagens/4.gif'>";
            echo "</td>";
			*/
			
			
            echo "<td><b>Legenda:</b> $row[14]</td></tr>";

			echo "</tr></td></tr></table>";

            $ct++;
        }
        echo "</table>";
    }else{
        echo "<br><br>Nenhum registro encontrado atendendo a forma de pesquisa.<br><br><br>";
    }
    ?>

    <nav>
        <ul class="pagination">
            <?php
                if(($or == 'if') || ($or == 'ar'))
                    $op = 100;
                else
                    $op = 50;
                $valor = str_replace("%", " ", $valor);
                
                $paginacao = "";
                
                if($npag>1) {
                    $paginacao .= "<li><a href=\"$link?op=$op&npag=1&valor=$valor&campo=$campo&genero=$genero&or=$or&cpcond=$cpcond&cond=$cond&som=$som&ordenacao=$ordenacao\" title=\"primeira página\"><span class=\"glyphicon glyphicon-step-backward\" aria-hidden=\"true\"></span></a></li>";
                    $paginacao .= "<li><a href=\"$link?op=$op&npag=".($npag-1)."&valor=$valor&campo=$campo&genero=$genero&or=$or&cpcond=$cpcond&cond=$cond&som=$som&ordenacao=$ordenacao\" title=\"página anterior\"><span class=\"glyphicon glyphicon-backward\" aria-hidden=\"true\"></span></a></li>";
                }
                for($inic=$i;$inic<=$fim;$inic++) {
                    if($inic != $npag)
                        $paginacao .= "<li><a href=\"$link?op=$op&npag=$inic&valor=$valor&campo=$campo&genero=$genero&or=$or&cpcond=$cpcond&cond=$cond&som=$som&ordenacao=$ordenacao\">$inic</a></li>";
                    else
                        $paginacao .= "<li class=\"active\"><a href=\"#\">$inic</a></li>";
                }
                if($npag<$qtpag) {
                    $paginacao .= "<li><a href=\"$link?op=$op&npag=".($npag+1)."&valor=$valor&campo=$campo&genero=$genero&or=$or&cpcond=$cpcond&cond=$cond&som=$som&ordenacao=$ordenacao\" title=\"próxima página\"><span class=\"glyphicon glyphicon-forward\" aria-hidden=\"true\"></span></a></li>";
                    $paginacao .= "<li><a href=\"$link?op=$op&npag=$qtpag&valor=$valor&campo=$campo&genero=$genero&or=$or&cpcond=$cpcond&cond=$cond&som=$som&ordenacao=$ordenacao\" title=\"última página\"><span class=\"glyphicon glyphicon-step-forward\" aria-hidden=\"true\"></span></a></li>";
                }

                echo $paginacao;
            ?>
        </ul>
    </nav>
        
        

	<?php
	} else {?>

		<table align=center width=100% CELLSPACING=0 CELLPADDING=0>
			<tr valign=top>
				<td height=21>
					<img src='./imagens/c9.gif' align=top>
				</td>
				<td height=21>
					<img src='./imagens/c1.gif' align=top>
				</td>
				<td width=50% class=janp2>
					<img src='./imagens/vid2.gif' align=top>
					Pesquisa na Videoteca
				</td>
				</td>
				<td>
					<img src='./imagens/c3.gif' align=top>
				</td>
				<td width=50% background='./imagens/c6.gif'>
				</td>
				<td>
					<img src='./imagens/c4.gif' align=top>
				</td>
				<td>
					<img src='./imagens/c10.gif' align=top>
				</td>
			</tr>
			<tr>
				<td background='./imagens/c7.gif'>
				</td>
				<td class=janp3 colspan=5>
					<table CELLSPACING=0 CELLPADDING=0 width=100%>
						<tr>
							<td class=jan1 width=70%>
								Pesquisa nao permitida
							</td>
							<td class=jan1>
									<span class=jan1e>

									</span>
							</td>
							<td class=jan1 align=right>
							</td>
							<td width=3 class=jan1>
							</td>
						</tr>
						<tr>
							<td class=jan2 colspan=4 align=center>
								<br><br>
								<img src='./imagens/aviso.jpg'>&nbsp;&nbsp;&nbsp;
								Consulta permitida apenas no ambito da Biblioteca Setorial do Instituto de Artes
								<br><br>
							</td>
						</tr>
					</table>
				</td>
				<td background='./imagens/c8.gif'>
				</td>
			</tr>
			<tr valign=top>
				<td>
					<img src='./imagens/c11.gif' align=top>
				</td>
				<td>
					<img src='./imagens/c12.gif' align=top>
				</td>
				<td colspan=3 background='./imagens/c13.gif'>
				</td>
				<td>
					<img src='./imagens/c14.gif' align=top>
				</td>
				<td>
					<img src='./imagens/c15.gif' align=top>
				</td>
			</tr>
		</table>
<?php } ?>
