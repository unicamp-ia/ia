<?php
	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/page.css")
	);

	get_header();
?>

<div class="row">
    <div class="col-md-6"><?php the_field("acervo-unicamp"); ?></div>
    <div class="col-md-6"><?php the_field("portais-de-pesquisa"); ?></div>
</div>

<?php include("busca.php"); ?>


<?php get_footer(); ?>