<?php

$campos = get_fields();
$aux = array();

//ksort($campos); 

foreach ($campos as $key => $value) { $aux[] = get_field_object($key); }

$campos = $aux;

// Compara se $a é maior que $b
function cmp($a, $b) {
	return $a['label'] > $b['label'];
}

// Ordena
usort($campos, 'cmp');

$primeiro = true;

?>

<div id="accordion" class="panel-group">
	<?php foreach ($campos as $campo) { ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"><a href="#<?php echo($campo["name"]); ?>" data-toggle="collapse" data-parent="#accordion"><?php echo($campo["label"]); ?></a></h4>
            </div>
            <div id="<?php echo($campo["name"]); ?>" class="panel-collapse collapse<?php if ($primeiro) { echo(" in"); $primeiro = false; } ?>">
                <div class="panel-body"><?php echo($campo["value"]); ?></div>
            </div>
        </div>
    <?php } ?>
</div>


