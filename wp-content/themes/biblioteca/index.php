<?php

	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/nivo-slider.css"),
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/home.css"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/jquery.nivo.slider.pack.js"),
		array("tag" => "script" , "src" => get_bloginfo("template_directory")."/js/home.js")
	);
	
	get_header();

	$imagens = array(
		array("src" => "slide/1.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
		array("src" => "slide/2.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
		array("src" => "slide/3.jpg" , "title" => "Foto: Celso Palermo" , "alt" => ""),
		array("src" => "slide/4.jpg" , "title" => "Foto: Celso Palermo" , "alt" => "")
	);
?>

	<?php slider($imagens); ?>
    
    <br /><br />
    
    <div id="ultimas-aquisicoes" class="panel panel-default">
        <div class="panel-heading">
			<p><a href="<?php echo(get_category_link(1)); ?>">Veja mais</a></p>
        	<h3 class="panel-title">Últimas Aquisições</h3>
		</div>
        <div class="panel-body">
        	<ul class="list-inline">
				<?php
                $the_query = new WP_Query( array("posts_per_page" => 4));
                
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <li>
                        <?php if (has_post_thumbnail()) { the_post_thumbnail("post-thumbnail",array("class" => "img-thumbnail")); } ?>
                        <h5><?php the_title(); ?></h5>
                        <h6><?php the_field("numero-de-chamada"); ?></h6>
                    </li>
                <?php endwhile; wp_reset_postdata(); ?>
            </ul>
            <br />
            <p>Caso não tenha localizado o material que você procura, <a href="http://www.iar.unicamp.br/biblioteca/sugestao-de-aquisicao/">clique aqui</a> e envie sua Sugestão de Aquisição.</p>
        </div>
	</div>

    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Busca</h3></div>
        <div class="panel-body">
        	<?php include("busca.php"); ?>
        </div> <!-- div.panel-body -->
    </div> <!-- div.panel -->
    
    
    
    <?php if (get_field("ativado",8)) { ?>
        <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-<?php the_field("tamanho",8); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                        <h2><?php the_field("titulo",8); ?></h2>
                    </div>
                    <div class="modal-body"><?php the_field("conteudo",8); ?></div>
                </div>
            </div>
        </div>
	<?php } ?>
    
<?php get_footer(); ?>