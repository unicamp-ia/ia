<?php
	$tags = array(
		array("tag" => "link" , "href" => get_bloginfo("template_directory")."/css/categoria.css")
	);
	
	get_header();
?>

<h1>Últimas Aquisições</h1>

<?php the_breadcrumb(); ?>

<p>Caso não tenha encontrado, envie aqui sua <a href="http://www.iar.unicamp.br/biblioteca/sugestao-de-aquisicao/">sugestão</a>.</p>
<br />
  <?php
	if ( have_posts() ) : global $wp_query;?>
    	<ul id="ultimas-aquisicoes" class="list-inline">
			<?php while ( have_posts() ) : the_post(); ?>
                <li<?php if (($wp_query->current_post+1)%4==0) { echo(" class=\"last\""); } ?>>
                    <?php if (has_post_thumbnail()) { the_post_thumbnail("post-thumbnail",array("class" => "img-thumbnail")); } ?>
                    <h5><?php the_title(); ?></h5>
                    <h6><?php the_field("numero-de-chamada"); ?></h6>
                </li>
            <?php endwhile; ?>

		<?php the_pagination();
	else: ?>
	    <p>Não há novas aquisições</p>
<?php endif; ?>

<?php get_footer(); ?>