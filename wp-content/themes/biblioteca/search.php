<?php
	$tags = array(
		array("tag" => "meta" , "name" => "robots" , "content" => "noindex, nofollow")
	);

	get_header();
?>

<div class="span9 listagem">
    <h3>Você buscou por: <em><?php echo($_GET["s"]); ?></em></h3>
    
    <?php if ( have_posts() ) :?>
        <h1>Resultados da busca</h1>
        <hr />
        <?php while ( have_posts() ) : the_post(); ?>
            <section>
                <?php if (has_post_thumbnail()) {?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
                <?php } ?>
                <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                <?php echo excerpt(80); ?>
            </section>
    <?php endwhile;
    
    the_pagination();
    
    else: ?>
        <p>Nada foi encontrado</p>
    <?php endif; ?>
</div>

<aside class="span3">
    <?php get_sidebar(); ?>
</aside>

<?php get_footer(); ?>