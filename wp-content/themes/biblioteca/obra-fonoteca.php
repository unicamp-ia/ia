<?php

	//include("conect.inc");
	include("conexao.php");
	
	$chvobra = $_REQUEST['chvobra'];

	$query = "select
				*
			from
				obra
			left join
				genero on obra.chvgenero=genero.chvgenero
			where obra.chvobra=$chvobra";
	$result = mysqli_query($link, $query);
	$row = mysqli_fetch_array($result);


?>

<?php

	foreach ($row as $key => $value) { $row[$key] = utf8_encode($value); } // codificação
	
	$linkar = get_permalink(12);
	
	echo "<table class=\"table table-bordered\">";
	if(trim($row[2])!='')
		echo "<tr><td colspan=\"4\">Titulo:&nbsp;<a href=\"$linkar?op=150&valor=".$row[2]."&campo=obra.titulo\">".$row[2]."</a></td></tr>";
	if(trim($row[4])!='')
		echo "<tr><td colspan=\"4\">Compositor:&nbsp;<a href=\"$linkar?op=150&valor=".$row[4]."&campo=compositor\">".$row[4]."</a></td></tr>";
	echo "<tr><td colspan=\"4\">Interprete:&nbsp;<a href=\"$linkar?op=150&valor=".$row[5]."&campo=obra.interprete\">".$row[5]."</a></td></tr>";
	
	if(trim($row[3])!='') {
		echo "<tr><td colspan=\"2\">Genero:&nbsp;<a href=\"$linkar?op=150&genero=".$row[12]."\">".$row[13]."</a></td>";	
		echo "<td colspan=\"2\">Serie:&nbsp;<a href=\"$linkar?op=150&valor=".$row[3]."&campo=obra.serie\">".$row[3]."</a></td></tr>";
	}
	else {
		echo "<tr><td colspan=\"4\">Genero:&nbsp;<a href=\"$linkar?op=150&genero=".$row[12]."\">".$row[13]."</a></td></tr>";
	}
	
	if ((trim($row[6])!='') && (trim($row[8])!='')) {
		echo "<tr><td colspan=\"2\">Local:&nbsp;<a href=\"$linkar?op=150&valor=".$row[6]."&campo=obra.local\">".$row[6]."</a></td>";
		echo "<td colspan=\"2\">Data:&nbsp;<a href=\"$linkar?op=150&valor=".$row[8]."&campo=obra.data\">".$row[8]."</a></td></tr>";
	} else {
		if (trim($row[6])!='') { echo "<tr><td colspan=\"4\">Local:&nbsp;<a href=\"$linkar?op=150&valor=".$row[6]."&campo=obra.local\">".$row[6]."</a></td></tr>"; }
		if (trim($row[8])!='') { echo "<tr><td colspan=\"4\">Data:&nbsp;<a href=\"$linkar?op=150&valor=".$row[8]."&campo=obra.data\">".$row[8]."</a></td></tr>"; }
	}
		
	if(trim($row[9])!='')
		echo "<td colspan=\"4\">Volume:&nbsp;<a href=\"$linkar?op=150&valor=".$row[9]."&campo=obra.volume\">".$row[9]."</a></td>";
	else
		echo "</td>";
	if(trim($row[10])!='')
		echo "<td colspan=\"4\">Minutos:&nbsp;<a href=\"$linkar?op=150&valor=".$row[10]."&campo=montagem\">".$row[10]."</a></td>";
	else
		echo "</td>";
	if(trim($row[7])!='')
		echo "<td colspan=\"4\">Gravadora:&nbsp;<a href=\"$linkar?op=150&valor=".$row[7]."&campo=obra.gravadora\">".$row[7]."</a></td></tr>";
	else
		echo "</tr>";
	if(trim($row[11])!='')
		echo "<tr><td colspan=\"4\">Notas:&nbsp;".$row[11]."</td></tr>";
	else
		echo "</tr>";
	//echo "</table>";
	
	//echo "<table class=\"table table-bordered\">";
	$qCopia = "select
				*
			from
				copia
			left join colecao on copia.chvcol=colecao.chvcol
			left join midia on copia.chvmidia=midia.chvmidia
			where
				copia.chvobra=$chvobra
			order by copia.tomboia,copia.dtreg desc";
	$resCopia = mysqli_query($link, $qCopia);
	
	$ct = 0;
	while($rCopia = mysqli_fetch_array($resCopia)) {
		
		foreach ($rCopia as $key => $value) { $rCopia[$key] = utf8_encode($value); } // codificação
	
		$ln = $ct/2;
		$ln = (int)$ln;
		if(($ln*2)==$ct) {
			$cl = "jan2";
		}else{
			$cl = "jan2l";
		}
	
		echo "<tr><td class=".$cl."><b>Tombo IA:&nbsp;".$rCopia[4].(trim($rCopia[5])!=''?'-':'').$rCopia[5]."</b></td>";
		echo "<td class=".$cl.">Tombo BC:&nbsp;".$rCopia[6]."</td>";
		echo "<td class=".$cl."><b>N&ordm; Chamada:&nbsp;".$rCopia[8]."</b></td>";
		echo "<td class=".$cl.">";
		if($rCopia[9]==1) {
			echo "<span class=\"disponivel glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> Disponível";
		}else{
			echo "<span class=\"indisponivel glyphicon glyphicon-remove\" aria-hidden=\"true\"></span> Indisponível";
		}
		echo "</td></tr>";
	
		echo "<tr><td class=".$cl." colspan=2>Cole&ccedil;&atilde;o:&nbsp;".$rCopia[11]."</td>";
		echo "<td class=".$cl.">M&iacute;dia:&nbsp;".$rCopia[13]."</td>";
		echo "<td class=".$cl.">Data Reg:&nbsp;".substr($rCopia[7],8,2)."/".substr($rCopia[7],5,2)."/".substr($rCopia[7],0,4)."</td></tr>";
		$ct++;
	}
	
	//echo "</table>";
	//echo "<table class=\"table table-bordered\">";
	
	$qMusica = "select
				*
			from
				musica
			where
				chvobra=$chvobra
			order by compositor";
	$resMusica = mysqli_query($link, $qMusica);
	$qtddM = mysqli_num_rows($resMusica);

	$ct = 0;
	
	echo "<tr><td colspan=\"4\">";
	if ($qtddM > 0) {
		echo("<p>".$qtddM." Música");
		
		if ($qtddM > 1) { echo("s"); } // plural
		
		echo(":</p><ul>");
		
		while($rMusica = mysqli_fetch_array($resMusica)) {
			
			foreach ($rMusica as $key => $value) { $rMusica[$key] = utf8_encode($value); } // codificação
	
			$ln = $ct/2;
			$ln = (int)$ln;
			if(($ln*2)==$ct) {
				$cl = "jan2";
			}else{
				$cl = "jan2l";
			}
	
			echo "<li><a href=\"$linkar?op=150&valor=".$rMusica[2]."&campo=musica.musica\">".$rMusica[2]."</a>";
			if(trim($rMusica[3])!='') {
				echo " &ndash; Compositor: <a href=\"$linkar?rop=150&valor=".$rMusica[3]."&campo=compositor\">".$rMusica[3]."</a>";
			}
			echo("</li>");
			$ct++;
	
		}
		
		echo("</ul>");
	}
	else {
		echo "Nenhuma Música";
	}
	
	echo "</td></tr></table>";

?>
