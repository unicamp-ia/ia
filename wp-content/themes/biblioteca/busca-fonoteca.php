<?php

	//registro de acesso
	//include("conect.inc");
	include("conexao.php");

	$end = $_SERVER['REMOTE_ADDR'];
//	echo $end;
	$area = 3;
	$data = date("Y",time())."-".date("m",time())."-".date("d",time());

	// biblioteca IA
	if(($end>='143.106.55.129') && ($end<='143.106.55.154')) {
//		echo "biblioteca";
		$cli=1;
	}elseif(substr($end,0,10)=='143.106.55') {
//		echo "IA";
		$cli=2;
	}elseif(substr($end,0,7)=='143.106') {
//		echo "UNICAMP";
		$cli=3;
	}elseif(substr($end,0,7)!='143.106') {
//		echo "externo";
		$cli=4;
	}

	if((session_id()!=$id) || ($area!=$ar)) {
		$qAc = "insert into estat(cli,dt,ar,ender) values('$cli','$data','$area','$end')";
		$resAc = mysqli_query($link, $qAc);
	}
	$_SESSION['ar'] = $area;
	//

	$or = $_REQUEST['or'];
	$campo = $_REQUEST['campo'];
	$valor = $_REQUEST['valor'];
	$genero = $_REQUEST['genero'];
	$variac = ($_REQUEST['variac']!=1?0:1);
	$campo2 = $_REQUEST['campo2'];
	$valor2 = $_REQUEST['valor2'];
	$variac2 = ($_REQUEST['variac2']!=1?0:1);
                        
        // AntiXSS
        $char_XSS=array("<",">","/","(",";",")","script","javascript:","alert","font","div","style","body");
        include("AntiXSS.php");
        $valor = AntiXSS::setEncoding($valor, "UTF-8");
        $valor = AntiXSS::setFilter($valor, "black");
        if ($valor=="XSS Detected!") die();
        $valor=str_replace($char_XSS, "", $valor);
        $valor2 = AntiXSS::setEncoding($valor2, "UTF-8");
        $valor2 = AntiXSS::setFilter($valor2, "black");
        if ($valor2=="XSS Detected!") die();
        $valor2=str_replace($char_XSS, "", $valor2);

	$valor = str_replace(" ", "%", $valor);
	if($campo=='copia.tomboia')
		$valor = str_pad($valor, 6, "0", STR_PAD_LEFT);
	$valor2 = str_replace(" ", "%", $valor2);

	// paginacao
	$npag = $_REQUEST['npag'];
	$regpp = 5;
	$npag = (trim($npag)>1?$npag:1);
	$regin = ($regpp * $npag) - $regpp;
	$qtprev = 10;
		if($campo=='T') {
			$qCt = "select
						count(*)
					from
						obra
					inner join
						copia on copia.chvobra=obra.chvobra
					left join
						genero on obra.chvgenero=genero.chvgenero
					left join
						musica on obra.chvobra=musica.chvobra";
			if($variac==0) {
				$qCt = $qCt." where
						(obra.titulo like '%$valor%' or obra.compositor like '%$valor%' or
						obra.interprete like '%$valor%' or musica.musica like '%$valor%'
						or musica.compositor like '%$valor%')";
			}else{
				$qCt = $qCt." where
						(obra.titulo like '$valor%' or obra.compositor like '$valor%' or
						obra.interprete like '$valor%' or musica.musica like '$valor%'
						or musica.compositor like '$valor%')";
			}
		}else{
			$qCt = "select
						count(*)
					from
						obra
					inner join
						copia on copia.chvobra=obra.chvobra
					left join
						genero on obra.chvgenero=genero.chvgenero
					left join
						musica on obra.chvobra=musica.chvobra";
			if($campo=='compositor') {
				if($variac==0) {
					$qCt = $qCt." where
							(obra.compositor like '%$valor%' or
							musica.compositor like '%$valor%')";
				}else{
					$qCt = $qCt." where
							(obra.compositor like '$valor%' or
							musica.compositor like '$valor%')";
				}
			}else{
				if($variac==0) {
					$qCt = $qCt." where
							$campo like '%$valor%'";
				}else{
					$qCt = $qCt." where
							$campo like '$valor%'";
				}
			}
		}

		// segunda selecao
		if($valor2!='') {
			if($campo2=='T') {
				if($variac2==0) {
					$qCt = $qCt." and
							(obra.titulo like '%$valor2%' or obra.compositor like '%$valor2%' or
							obra.interprete like '%$valor2%' or musica.musica like '%$valor2%'
							or musica.compositor like '%$valor2%')";
				}else{
					$qCt = $qCt." and
							(obra.titulo like '$valor2%' or obra.compositor like '$valor2%' or
							obra.interprete like '$valor2%' or musica.musica like '$valor2%'
							or musica.compositor like '$valor2%')";
				}
			}else{
				if($campo2=='compositor') {
					if($variac2==0) {
						$qCt = $qCt." and
								obra.compositor like '%$valor2%' or
								musica.compositor like '%$valor2%'";
					}else{
						$qCt = $qCt." and
								obra.compositor like '$valor2%' or
								musica.compositor like '$valor2%'";
					}
				}else{
					if($variac2==0) {
						$qCt = $qCt." and
								$campo2 like '%$valor2%'";
					}else{
						$qCt = $qCt." and
								$campo2 like '$valor2%'";
					}
				}
			}
		}

		// agrupamento pela obra
		$qCt = $qCt." group by obra.chvobra";

	
	$resCt = mysqli_query($link, $qCt);
	
	if ($resCt) {
		$rCt =  mysqli_fetch_array($resCt);
		$rCt[0] = mysqli_num_rows($resCt);
		$qtpag = $rCt[0]/$regpp;
		if((int)$qtpag < $qtpag) {
			$qtpag = (int)$qtpag +1;
		}
		$i = (int)($npag - ($qtprev / 2));
		if($i < 1)
			$i = 1;
		$fim = (int)($npag + ($qtprev / 2));
		if($fim > $qtpag)
			$fim = $qtpag;
	}	

	// selecao dos registros
		if($campo=='T') {
			$query = "select
						obra.titulo,obra.compositor,obra.interprete,
						genero.descr,musica.musica,obra.chvobra,
						copia.chamada,copia.tomboia,copia.compl,copia.disponivel
					from
						obra
					inner join
						copia on copia.chvobra=obra.chvobra
					left join
						genero on obra.chvgenero=genero.chvgenero
					left join
						musica on obra.chvobra=musica.chvobra";
			if($variac==0) {
				$query = $query." where
						(obra.titulo like '%$valor%' or obra.compositor like '%$valor%' or
						obra.interprete like '%$valor%' or musica.musica like '%$valor%'
						or musica.compositor like '%$valor%')";
			}else{
				$query = $query." where
						(obra.titulo like '$valor%' or obra.compositor like '$valor%' or
						obra.interprete like '$valor%' or musica.musica like '$valor%'
						or musica.compositor like '$valor%')";
			}
		}else{
			if($campo=='compositor') {
				$query = "select
							obra.titulo,obra.compositor,obra.interprete,
							genero.descr,musica.musica,obra.chvobra,
							copia.chamada,copia.tomboia,copia.compl,copia.disponivel
						from
							obra
						inner join
							copia on copia.chvobra=obra.chvobra
						left join
							genero on obra.chvgenero=genero.chvgenero
						left join
							musica on obra.chvobra=musica.chvobra";
				if($variac==0) {
					$query = $query." where
							(obra.compositor like '%$valor%' or
							musica.compositor like '%$valor%')";
				}else{
					$query = $query." where
							(obra.compositor like '$valor%' or
							musica.compositor like '$valor%')";
				}
			}else{
				$query = "select
							obra.titulo,obra.compositor,obra.interprete,
							genero.descr,musica.musica,obra.chvobra,
							copia.chamada,copia.tomboia,copia.compl,copia.disponivel
						from
							obra
						inner join
							copia on copia.chvobra=obra.chvobra
						left join
							genero on obra.chvgenero=genero.chvgenero
						left join
							musica on obra.chvobra=musica.chvobra";
				if($variac==0) {
					$query = $query." where
							$campo like '%$valor%'";
				}else{
					$query = $query." where
							$campo like '$valor%'";
				}
			}
		}

		// segunda selecao
		if($valor2!='') {
			if($campo2=='T') {
				if($variac2==0) {
					$query = $query." and
							(obra.titulo like '%$valor2%' or obra.compositor like '%$valor2%' or
							obra.interprete like '%$valor2%' or musica.musica like '%$valor2%'
							or musica.compositor like '%$valor2%')";
				}else{
					$query = $query." and
							(obra.titulo like '$valor2%' or obra.compositor like '$valor2%' or
							obra.interprete like '$valor2%' or musica.musica like '$valor2%'
							or musica.compositor like '$valor2%')";
				}
			}else{
				if($campo2=='compositor') {
					if($variac2==0) {
						$query = $query." and
								(obra.compositor like '%$valor2%' or
								musica.compositor like '%$valor2%')";
					}else{
						$query = $query." and
								(obra.compositor like '$valor2%' or
								musica.compositor like '$valor2%')";
					}
				}else{
					if($variac2==0) {
						$query = $query." and
								$campo2 like '%$valor2%'";
					}else{
						$query = $query." and
								$campo2 like '$valor2%'";
					}
				}
			}
		}

		// agrupamento, ordenacao, limites
		$query = $query."group by obra.chvobra
					order by titulo,musica
					limit $regin,$regpp";

	$result = mysqli_query($link, $query);
	
	if ($result) {
		$qtdd = mysqli_num_rows($result);	
	}
	else {
		$qtdd = 0;	
	}

	

?>

<h2>Pesquisa na Fonoteca</h2>
<div class="alert alert-info" role="alert">
    <?php
		echo "<h3>Você buscou por: <i>".str_replace("%"," ",$valor." ".$valor2)."</i></h3>";
		echo "<p>$rCt[0] registros localizados</p>";
    ?>
</div>
<?php if($qtdd > 0) { ?>
        <nav>
            <ul class="pagination">
                <?php
                    if (($or == 'if') || ($or == 'ar')) $op = 370;
                    else $op = 150;
                        
                    $link = get_page_link(get_the_ID());
                    
                    $paginacao = "";
                        
                    if($npag>1) {
                        $paginacao .= "<li><a href=\"$link?op=$op&npag=1&or=$or&valor=$valor&campo=$campo&variac=$variac&valor2=$valor2&campo2=$campo2&variac2=$variac2\" title=\"primeira página\"><span class=\"glyphicon glyphicon-step-backward\" aria-hidden=\"true\"></span></a></li>";
                        $paginacao .= "<li><a href=\"$link?op=$op&npag=".($npag-1)."&or=$or&valor=$valor&campo=$campo&variac=$variac&valor2=$valor2&campo2=$campo2&variac2=$variac2\" title=\"página anterior\"><span class=\"glyphicon glyphicon-backward\" aria-hidden=\"true\"></span></a></li>";
                    }
                    for($inic=$i;$inic<=$fim;$inic++) {
                        if($inic != $npag)
                            $paginacao .= "<li><a href=\"$link?op=$op&npag=$inic&or=$or&valor=$valor&campo=$campo&variac=$variac&valor2=$valor2&campo2=$campo2&variac2=$variac2\">$inic</a></li>";
                        else
                            $paginacao .= "<li class=\"active\"><a href=\"#\">$inic</a></li>";
                    }
                    if($npag<$qtpag) {
                        $paginacao .= "<li><a href=\"$link?op=$op&npag=".($npag+1)."&or=$or&valor=$valor&campo=$campo&variac=$variac&valor2=$valor2&campo2=$campo2&variac2=$variac2\" title=\"próxima página\"><span class=\"glyphicon glyphicon-forward\" aria-hidden=\"true\"></span></a></li>";
                        $paginacao .= "<li><a href=\"$link?op=$op&npag=$qtpag&or=$or&valor=$valor&campo=$campo&variac=$variac&valor2=$valor2&campo2=$campo2&variac2=$variac2\" title=\"última página\"><span class=\"glyphicon glyphicon-step-forward\" aria-hidden=\"true\"></span></a></li>";
                    }
                    
                    echo $paginacao;
                ?>
            </ul>
        </nav>
<?php
		echo "<table class=\"table table-bordered table-striped\">";
		$ct = 0;
		while($row = mysqli_fetch_array($result)) {

			// codificação
			foreach ($row as $key => $value) {
				$row[$key] = utf8_encode($value);
			}
			
			
			$ln = $ct/2;
			$ln = (int)$ln;
			if(($ln*2)==$ct) {
				$cl = "jan2";
			}else{
				$cl = "jan2l";
			}
			
			echo("<tr><td>");
			
			if($or=='if')
				echo "<p class=\"text-right\"><a href='default2.php?chvobra=$row[5]&op=350'>incluir fita</a></p>";
			if($or=='ar')
				echo "<p class=\"text-right\"><a href='default2.php?chvobra=$row[5]&op=380&npag=$npag&or=$or&valor=$valor&campo=$campo&variac=$variac&valor2=$valor2&campo2=$campo2&variac2=$variac2'>editar</a></p>";
			if(($or!='if') &&($or!='ar'))
				echo "<a href=\"".get_page_link(14)."?chvobra=$row[5]&op=160&npag=$npag&or=$or&valor=$valor&campo=$campo&variac=$variac&valor2=$valor2&campo2=$campo2&variac2=$variac2\" class=\"detalhes btn btn-default btn-xs\"><span class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span> Detalhes</a>";

			if(trim($row[0])!='') {
				echo "<p><strong>Titulo:</strong> $row[0]</p>";
			}else{
				if((($campo=='compositor') || ($campo2=='compositor')) &&
				  (($valor=='') || ($valor2==''))) {
				}else{
					echo "<p><strong>Musica:</strong> $row[4] [+ músicas]</p>";
				}
			}
			if(trim($row[1])!='') {
				echo "<p><strong>Compositor:</strong> $row[1]</p>";
			}
			echo "<p><strong>Intérprete:</strong> $row[2]</p>";
			echo "<p><strong>Gênero:</strong> $row[3]</p>";

			$ct++;
			
			echo("</td></tr>");
		}
		echo "</table>";
		
?>
        <nav>
            <ul class="pagination">
                <?php echo $paginacao; ?>
            </ul>
        </nav>
<?php
	}
	else {
		echo "<p>Nenhum registro encontrado atendendo a forma de pesquisa.</p>";
	}
?>
