<?php

	//registro de acesso
	//include("conect.inc");
	include("conexao.php");

	$end = $_SERVER['REMOTE_ADDR'];
//	echo $end;
	$area = 4;
	$data = date("Y",time())."-".date("m",time())."-".date("d",time());

	// biblioteca IA
	if(($end>='143.106.55.129') && ($end<='143.106.55.154')) {
//		echo "biblioteca";
		$cli=1;
	}elseif(substr($end,0,10)=='143.106.55') {
//		echo "IA";
		$cli=2;
	}elseif(substr($end,0,7)=='143.106') {
//		echo "UNICAMP";
		$cli=3;
	}elseif(substr($end,0,7)!='143.106') {
//		echo "externo";
		$cli=4;
	}

	if((session_id()!=$id) || ($area!=$ar)) {
		$qAc = "insert into estat(cli,dt,ar,ender) values('$cli','$data','$area','$end')";
		$resAc = mysqli_query($link, $qAc);
	}
	$_SESSION['ar'] = $area;
	//

	$or = $_REQUEST['or'];
	$valor = $_REQUEST['valor'];
	$campo = $_REQUEST['campo'];
	if (isset($_REQUEST['campo2'])) 
		$campo2 = $_REQUEST['campo2'];
	else
		$campo2 = 'compositor';


        // AntiXSS
        $char_XSS=array("<",">","/","(",";",")","script","javascript:","alert","font","div","style","body");
        include("AntiXSS.php");
        $valor = AntiXSS::setEncoding($valor, "UTF-8");
        $valor = AntiXSS::setFilter($valor, "black");
        if ($valor=="XSS Detected!") die();
        $valor=str_replace($char_XSS, "", $valor);

	// paginacao
	$npag = $_REQUEST['npag'];
	$regpp = 5;
	$npag = (trim($npag)>1?$npag:1);
	$regin = ($regpp * $npag) - $regpp;
	$qtprev = 10;
	// conta registros
	if($campo=='T') {
		$qCt = "select
					count(*)
				from
					partituras
				where
					titulo like '%$valor%' or
					compositor like '%$valor%' or
					nchamada like '%$valor%' or
					tomboia like '%$valor%' or
					tombobc like '%$valor%'";
	}else{
		$qCt = "select
					count(*)
				from
					partituras
				where
					$campo like '%$valor%'";
	}

	$resCt = mysqli_query($link, $qCt);
	$rCt =  mysqli_fetch_array($resCt);
	$qtpag = $rCt[0]/$regpp;
	if((int)$qtpag < $qtpag) {
		$qtpag = (int)$qtpag +1;
	}
	$i = (int)($npag - ($qtprev / 2));
	if($i < 1)
		$i = 1;
	$fim = (int)($npag + ($qtprev / 2));
	if($fim > $qtpag)
		$fim = $qtpag;

	// selecao dos registros
	if($campo=='T') {
		$query = "select
					chvpart, data, tomboia*1 AS tomboia, tombobc*1 as tombobc, nchamada, cutter, compositor, titulo, local, editora, ts, aquisicao
				from
					partituras
				where
					titulo like '%$valor%' or
					compositor like '%$valor%' or
					nchamada like '%$valor%' or
					tomboia like '%$valor%' or
					tombobc like '%$valor%'
				order by
					$campo2
				limit $regin,$regpp";
	}else{
		$query = "select
					chvpart, data, tomboia*1 AS tomboia, tombobc*1 as tombobc, nchamada, cutter, compositor, titulo, local, editora, ts, aquisicao
				from
					partituras
				where
					$campo like '%$valor%'
				order by
					$campo2
				limit $regin,$regpp";
	}

	// echo $query;

	$result = mysqli_query($link, $query);

	$qtdd = mysqli_num_rows($result);
?>
<h2>Pesquisa de Partituras</h2>
<div class="alert alert-info" role="alert">
<?php
	echo "<h3>Você buscou por: <i>" . $valor . "</i></h3>";
	echo "<p>" . $rCt[0] . " registros localizados</p>";
?>
</div>
        <nav>
            <ul class="pagination">
                <?php
					if(($or == 'if') || ($or == 'ar'))
						$op = 440;
					else
						$op = 170;
                        
                    $link = get_page_link(get_the_ID());
                    
                    $paginacao = "";
					
					if($npag>1) {
						$paginacao .= "<li><a href=\"$link?op=$op&npag=1&valor=$valor&campo=$campo&campo2=$campo2&or=$or\" title=\"primeira página\"><span class=\"glyphicon glyphicon-step-backward\" aria-hidden=\"true\"></span></a></li>";
						$paginacao .= "<li><a href=\"$link?op=$op&npag=".($npag-1)."&valor=$valor&campo=$campo&campo2=$campo2&or=$or\" title=\"página anterior\"><span class=\"glyphicon glyphicon-backward\" aria-hidden=\"true\"></span></a></li>";
					}
					for($inic=$i;$inic<=$fim;$inic++) {
						if($inic != $npag)
							$paginacao .= "<li><a href=\"$link?op=$op&npag=$inic&valor=$valor&campo=$campo&campo2=$campo2&or=$or\">$inic</a></li>";
						else
							$paginacao .= "<li class=\"active\"><a href=\"#\">$inic</a></li>";
					}
					if($npag<$qtpag) {
						$paginacao .= "<li><a href=\"$link?op=$op&npag=".($npag+1)."&valor=$valor&campo=$campo&campo2=$campo2&or=$or\" title=\"próxima página\"><span class=\"glyphicon glyphicon-forward\" aria-hidden=\"true\"></span></a></li>";
						$paginacao .= "<li><a href=\"$link?op=$op&npag=$qtpag&valor=$valor&campo=$campo&campo2=$campo2&or=$or\" title=\"última página\"><span class=\"glyphicon glyphicon-step-forward\" aria-hidden=\"true\"></span></a></li>";
					}

                    echo $paginacao;
                ?>
            </ul>
        </nav>

						<?php
						if($qtdd > 0) {
							echo "<table class=\"table table-bordered table-striped\">";
							$ct = 0;
							while($row = mysqli_fetch_array($result)) {
								
								// codificação
								foreach ($row as $key => $value) { $row[$key] = utf8_encode($value); }

								$ln = $ct/2;
								$ln = (int)$ln;
								if(($ln*2)==$ct) {
									$cl = "jan2";
								}else{
									$cl = "jan2l";
								}
								
								echo("<tr><td><table class=\"table\">");

								echo "<tr><td colspan=\"3\"><b>Titulo:</b> $row[7]</td></tr>";
								echo "<tr><td colspan=\"3\"><b>Compositor:</b> $row[6]</td></tr>";
								echo "<tr><td width=\"33%\"><b>Tombo IA:</b> $row[2]</td>";
								echo "<td width=\"33%\"><b>Tombo BC:</b> $row[3]</td>";
								echo "<td width=\"34%\"><b>N.Chamada:</b> $row[4]</td>";

								/*
								if($or=='ar') {
									echo "<td class=".$cl." align=right>";
									echo "<a href='default2.php?chvpart=$row[0]&op=450&valor=$valor&or=$or&campo=$campo&npag=$npag'>editar<img src='./imagens/lapisb.gif' border=0></a>";
									echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='exclpart.php?chvpart=$row[0]'>excluir<img src='./imagens/lapisa.gif' border=0></a>";
									echo "</td>";
								}else{
									echo "<td class=".$cl." align=right></td>";
								}
								*/
								echo "</tr></td></tr></table>";



								$ct++;
							}
							echo "</table>";
						}else{
							echo "<br><br>Nenhum registro encontrado atendendo a forma de pesquisa.<br><br><br>";
						}
						?>


        <nav>
            <ul class="pagination">
                <?php echo $paginacao; ?>
            </ul>
        </nav>
